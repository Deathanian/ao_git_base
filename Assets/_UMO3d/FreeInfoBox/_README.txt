﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
First, import this AddOn.

Next, move the InfoBoxUI prefab into your canvas. Make sure its inside your canvas
object and not inside any other object.

Finally, add a new component to all of your player prefabs: InfoBox

Thats it, no further modifications required!

Usage Information:

During play, you can show/hide the info box by pressing the B button.

You can add your own info text to the textbox by calling the following code client side:

var msg = "CHANGE THIS TEXT";
FindObjectOfType<UIInfoBox>().AddMessage(new InfoText(msg, Color.yellow));

If you want the server to send a message to one client, use this code inside the player
class:

var msg = "CHANGE THIS TEXT";
GetComponent<InfoBox>().TargetAddMessage(connectionToClient, msg);

Or prepend the player if the player class is available:

var msg = "CHANGE THIS TEXT";
player.GetComponent<InfoBox>().TargetAddMessage(connectionToClient, msg);

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Adds a simple information box to your game.

This box can hold info messages similar to the Chat, but separate from the chat messages.

Show/Hide the Info Box pressing the B button.

If a lot of chat messages are rolling through the chat window, players might miss a
important message. By adding this information to the info box instead, you create a better
understanding for your player and improve usability.

Most of my AddOns use the Info box. When the info box is not available, they use the
standard Chat instead.

======================================================================