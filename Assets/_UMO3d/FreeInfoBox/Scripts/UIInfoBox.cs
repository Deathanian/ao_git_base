﻿// =======================================================================================
// FREE INFO BOX - UI
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace UMO3d {

	// =======================================================================================
	// UIInfoBox
	// =======================================================================================
	public partial class UIInfoBox : MonoBehaviour {

		[SerializeField] KeyCode hotKey = KeyCode.B;
		[SerializeField] GameObject panel;
		[SerializeField] Transform content;
		[SerializeField] ScrollRect scrollRect;
		[SerializeField] GameObject textPrefab;
		[SerializeField] int keepHistory = 100; // only keep 'n' messages
	
		// -----------------------------------------------------------------------------------
		// Update
		// -----------------------------------------------------------------------------------
		void Update() {
			var player = Utils.ClientLocalPlayer();
			if (!player) panel.SetActive(false); // hide while not in the game world
			if (!player) return;
		
			// hotkey (not while typing in chat, etc.)
			if (Input.GetKeyDown(hotKey) && !UIUtils.AnyInputActive())
				panel.SetActive(!panel.activeSelf);
		
		}
	
		// -----------------------------------------------------------------------------------
		// AutoScroll
		// -----------------------------------------------------------------------------------
		void AutoScroll() {
			// update first so we don't ignore recently added messages, then scroll
			Canvas.ForceUpdateCanvases();
			scrollRect.verticalNormalizedPosition = 0;
		}
	
		// -----------------------------------------------------------------------------------
		// AddMessage
		// -----------------------------------------------------------------------------------
		public void AddMsg(InfoText info) {
				
			// delete an old message if we have too many
			if (content.childCount >= keepHistory)
				Destroy(content.GetChild(0).gameObject);

			// instantiate and initialize text prefab
			var go = Instantiate(textPrefab);
			go.transform.SetParent(content.transform, false);
			go.GetComponent<Text>().text = info.content;
			go.GetComponent<Text>().color = info.color;

			AutoScroll();
		}

	}

	// =======================================================================================

}