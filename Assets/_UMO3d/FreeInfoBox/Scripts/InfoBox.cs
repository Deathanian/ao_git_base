﻿// =======================================================================================
// FREE INFO BOX - UI
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace UMO3d {

	// =======================================================================================
	// InfoBox
	// =======================================================================================
	public partial class InfoBox : NetworkBehaviour {

		// -----------------------------------------------------------------------------------
		// TargetAddMessage
		// -----------------------------------------------------------------------------------
		[TargetRpc(channel=Channels.DefaultUnreliable)] // only send to one client
		public void TargetAddMessage(NetworkConnection target, string message) {
			AddMessage(new InfoText(message, Color.yellow));
		}

		// -----------------------------------------------------------------------------------
		// AddMessage
		// -----------------------------------------------------------------------------------
		[Client]
		public void AddMessage(InfoText info) {
			FindObjectOfType<UIInfoBox>().AddMsg(info);
		}

    }
    
	// =======================================================================================
	// InfoText
	// =======================================================================================
	[System.Serializable]
	public class InfoText {
		public string content; // the actual message
		public Color color;

		public InfoText(string _info, Color _color)
		{
			// construct the message (we don't really need to save all the parts,
			// also this will save future computations)
			content = _info;
			color = _color;
		}
	}

	// =======================================================================================

}