﻿// =======================================================================================
// NU CORE - ITEM
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// ITEM
// =======================================================================================
public partial struct Item {

	// -----------------------------------------------------------------------------------
	// equipBlockEffectBonus
	// -----------------------------------------------------------------------------------
	public float equipBlockEffectBonus {
		get {
#if _SimpleEquipmentSets
			return template.equipBlockEffectBonus + setBlockEffectBonus;
#else
			return template.equipBlockEffectBonus;
#endif
		}
	}
	
	// -----------------------------------------------------------------------------------
	// equipCriticalEffectBonus
	// -----------------------------------------------------------------------------------
	public float equipCriticalEffectBonus {
		get {
#if _SimpleEquipmentSets
			return template.equipCriticalEffectBonus + setCriticalEffectBonus;
#else
			return template.equipCriticalEffectBonus;
#endif
		}
	}
	
	// -----------------------------------------------------------------------------------
	// ToolTip_UMO3d_NuCoreItem
	// -----------------------------------------------------------------------------------
	public void ToolTip_UMO3d_NuCoreItem(StringBuilder tip) {
        if (category.StartsWith("Equipment")) {
        	tip.Append("\n");
        	if (equipBlockEffectBonus != 0) 	tip.Append( "Block Effect: " 	+ Mathf.RoundToInt(equipBlockEffectBonus*100).ToString() 	+ "%\n");
        	if (equipCriticalEffectBonus != 0) 	tip.Append( "Crit Effect: " 	+ Mathf.RoundToInt(equipCriticalEffectBonus*100).ToString() + "%\n");
        	
        }
    } 

}

// =======================================================================================