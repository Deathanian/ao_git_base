﻿// =======================================================================================
// NU CORE - SKILL
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// SKILL
// =======================================================================================
public partial struct Skill {
	
	// ---------- Level dependant Stats
	
	public float buffsBlockEffect {
		get { return template.levels[level-1].buffsBlockEffect; }
	}
	
	public float buffsCriticalEffect {
		get { return template.levels[level-1].buffsCriticalEffect; }
	}
	
	// ---------- ToolTip
	
	public void ToolTip_UMO3d_NuCoreSkill(StringBuilder tip) {
       	
        if (category.StartsWith("Buff")) {
        	tip.Append("\n");
        	if (buffsBlockEffect != 0) 		tip.Append( "Block Effect: "	+ Mathf.RoundToInt(buffsBlockEffect*100).ToString() 	+ "%\n");
        	if (buffsCriticalEffect != 0) 	tip.Append( "Crit Effect: "		+ Mathf.RoundToInt(buffsCriticalEffect*100).ToString() 	+ "%\n");
        	
        }
    } 
}

// =======================================================================================