﻿// =======================================================================================
// NU CORE - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {

	private bool UMO3d_myCheckAllowance;
	
	public SyncListNuCoreAttribute UMO3d_Attributes = new SyncListNuCoreAttribute();

	// -----------------------------------------------------------------------------------
	// OnLevelUp_UMO3d_NuCore
	// -----------------------------------------------------------------------------------
	private void OnLevelUp_UMO3d_NuCore() {
		attributePoints += levels[level-1].attributePoints;
	}
	
	// ===================================================================================
	// ===================================================================================
	
	// -----------------------------------------------------------------------------------
	// UMO3d_CheckItemUsage
	// -----------------------------------------------------------------------------------
	private bool UMO3d_CheckItemUsage(Item item) {
		
		UMO3d_myCheckAllowance = true;
		
		// addon system hooks
        Utils.InvokeMany(typeof(Player), this, "OnCheckItemUsage_", item);
	
		return UMO3d_myCheckAllowance;
		
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_CheckSkillUsage
	// -----------------------------------------------------------------------------------
	private bool UMO3d_CheckSkillUsage(Skill skill) {
		
		UMO3d_myCheckAllowance = true;
		
		// addon system hooks
        Utils.InvokeMany(typeof(Player), this, "OnCheckSkillUsage_", skill);
	
		return UMO3d_myCheckAllowance;
		
	}

	// ===================================================================================
	// ===================================================================================

	// -----------------------------------------------------------------------------------
	// blockEffect
	// -----------------------------------------------------------------------------------
    public float baseBlockEffect { get { return levels[level-1].baseBlockEffect; } }
    public float blockEffect {
    	get {

            float equipmentBonus = (from item in equipment
                                  where item.valid
                                  select item.equipBlockEffectBonus).Sum();

            float buffBonus = (from skill in skills
                             where skill.BuffTimeRemaining() > 0
                             select skill.buffsBlockEffect).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = UMO3d_getSetBonusBlockEffect();
#endif

           var attributeBonus = 0;
#if _SimpleAttributes
#pragma warning disable
			if (Constants.UMO3d_ATRIB_ACTIVE5) {
				if (Constants.UMO3d_SCALE_BLOCKEFFECT) {
					attributeBonus = Convert.ToInt32(levels[level-1].manaMax * (attribute5 * Constants.UMO3d_BONUS_BLOCKEFFECT));
				} else {
					attributeBonus = Convert.ToInt32(attribute5 * Constants.UMO3d_BONUS_BLOCKEFFECT);
				}
			}
#pragma warning restore
#endif
     
            return levels[level-1].baseBlockEffect + equipmentBonus + buffBonus + setBonus + attributeBonus;
    	}
    }
    
   	// -----------------------------------------------------------------------------------
	// criticalEffect
	// -----------------------------------------------------------------------------------
    public float baseCriticalEffect { get { return levels[level-1].baseCriticalEffect; } }
    public float criticalEffect {
    	get {

            float equipmentBonus = (from item in equipment
                                  where item.valid
                                  select item.equipCriticalEffectBonus).Sum();

            float buffBonus = (from skill in skills
                             where skill.BuffTimeRemaining() > 0
                             select skill.buffsCriticalEffect).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = UMO3d_getSetBonusCriticalEffect();
#endif

			var attributeBonus = 0;
 #if _SimpleAttributes
 #pragma warning disable
 			if (Constants.UMO3d_ATRIB_ACTIVE6) {
				if (Constants.UMO3d_SCALE_CRITEFFECT) {
					attributeBonus = Convert.ToInt32(levels[level-1].manaMax * (attribute6 * Constants.UMO3d_BONUS_CRITEFFECT));
				} else {
					attributeBonus = Convert.ToInt32(attribute6 * Constants.UMO3d_BONUS_CRITEFFECT);
				}
			}
#pragma warning restore
#endif
          
        	return levels[level-1].baseCriticalEffect + equipmentBonus + setBonus + buffBonus + attributeBonus;
    	}
    }

	// ===================================================================================
	// ===================================================================================

	// -----------------------------------------------------------------------------------
	// UMO3d_AttributeSet
	// -----------------------------------------------------------------------------------
	public void UMO3d_AttributeSet(string attribname, int attribvalue) {
		int idx = UMO3d_Attributes.FindIndex(att => att.name == attribname);
		if (idx != -1) {
			var attrib = UMO3d_Attributes[idx];
			attrib.value = attribvalue;
			UMO3d_Attributes[idx] = attrib;
		} else {
			var attrib = new NuCoreAttribute();
			attrib.name = attribname;
			attrib.value = attribvalue;
			UMO3d_Attributes.Add(attrib);
		}
	}
	
    // -----------------------------------------------------------------------------------
	// UMO3d_AttributeGet
	// -----------------------------------------------------------------------------------
	public int UMO3d_AttributeGet(string attribname) {
		int idx = UMO3d_Attributes.FindIndex(att => att.name == attribname);
		if (idx != -1) return UMO3d_Attributes[idx].value;
		return 0;
	}	

	// ===================================================================================
	// ===================================================================================
   

}

// =======================================================================================