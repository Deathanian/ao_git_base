﻿// =======================================================================================
// NU CORE - NETWORKMANAGER MMO
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// NETWORKMANAGER MMO
// =======================================================================================
public partial class NetworkManagerMMO {

	// -----------------------------------------------------------------------------------
	// OnServerCharacterCreate_UMO3d_NuCore
	// -----------------------------------------------------------------------------------
	private void OnServerCharacterCreate_UMO3d_NuCore(CharacterCreateMsg message, Player player) {
		
		player.strength 	= player._strength;
		player.intelligence = player._intelligence;
		
#if _SimpleAttributes
		player.attribute1 	= player._attribute1;
		player.attribute2 	= player._attribute2;
		player.attribute3 	= player._attribute3;
		player.attribute4 	= player._attribute4;
		player.attribute5 	= player._attribute5;
		player.attribute6 	= player._attribute6;
		
		player.attribute7 	= player._attribute7;
		player.attribute8 	= player._attribute8;

#if _SimpleStamina
		player.attribute9 	= player._attribute9;
		player.stamina 		= player.staminaMax;
#endif
		
#endif
		
	}
	
	// ===================================================================================
	
}

// =======================================================================================