﻿// =======================================================================================
// NU CORE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace UMO3d {

	// ===================================================================================
	// NU CORE
	// ===================================================================================
	public partial class NuCore {
		
		/*
			placeholder for future functionality
		*/
		
	}

}

// =======================================================================================