﻿// =======================================================================================
// NU CORE - ITEM TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// ITEM TEMPLATE
// =======================================================================================
public partial class ItemTemplate : ScriptableObject {
   	
	[Header("UMO3d NuCore - Modifiers")]
	[Range(-Constants.UMO3d_MODIFIER_BLOCK_MAX,Constants.UMO3d_MODIFIER_BLOCK_MAX)] public float equipBlockEffectBonus;
	[Range(-Constants.UMO3d_MODIFIER_CRIT_MAX,	Constants.UMO3d_MODIFIER_CRIT_MAX)] public float equipCriticalEffectBonus;

}

// =======================================================================================