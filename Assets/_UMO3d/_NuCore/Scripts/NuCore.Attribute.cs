﻿// =======================================================================================
// SIMPLE ATTRIBUTE - CLASS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
 
 namespace UMO3d {
 
	[System.Serializable]
	public struct NuCoreAttribute {
		public string name;
		public int value;
 
		public NuCoreAttribute(string _name, int _value) {
			name = _name;
			value = _value;
		}
	}
 
	public class SyncListNuCoreAttribute : SyncListStruct<NuCoreAttribute> { }

}

// =======================================================================================