﻿// =======================================================================================
// NU CORE - ENTITY
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;

// =======================================================================================
// ENTITY
// =======================================================================================
public partial class Entity {

	/*
		placeholder for future functionality
	*/
	
}

// =======================================================================================