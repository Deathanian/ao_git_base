﻿// =======================================================================================
// NU CORE - MONSTER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// MONSTER
// =======================================================================================
public partial class Monster {

	// -----------------------------------------------------------------------------------
	// blockEffect
	// -----------------------------------------------------------------------------------
    [Header("Block Effect")]
    [SerializeField, Range(0, 1)] float _blockEffect = 0;
    
    public float blockEffect {
    	get {
    		
            // calculate buff bonus
            float buffBonus = (from skill in skills
                             where skill.BuffTimeRemaining() > 0
                             select skill.buffsBlockEffect).Sum();

            // return base + buffs
            return _blockEffect + buffBonus;
    	}
    }
    
   	// -----------------------------------------------------------------------------------
	// criticalEffect
	// -----------------------------------------------------------------------------------
    [Header("Critical Effect")]
    [SerializeField, Range(0, 1)] float _criticalEffect = 0;
    
    public float criticalEffect {
    	get {
    		
            // calculate buff bonus
            float buffBonus = (from skill in skills
                             where skill.BuffTimeRemaining() > 0
                             select skill.buffsCriticalEffect).Sum();

            // return base + buffs
            return _criticalEffect + buffBonus;
    	}
    }
    
}

// =======================================================================================