﻿// =======================================================================================
// NU CORE - SKILL TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// SKILL TEMPLATE
// =======================================================================================
public partial class SkillTemplate : ScriptableObject {
   
    public partial struct SkillLevel {
       
        [Header("NuCore - Modifiers")]
   		[Range(-Constants.UMO3d_MODIFIER_BLOCK_MAX,Constants.UMO3d_MODIFIER_BLOCK_MAX)] public float buffsBlockEffect;
 	  	[Range(-Constants.UMO3d_MODIFIER_CRIT_MAX,	Constants.UMO3d_MODIFIER_CRIT_MAX)] public float buffsCriticalEffect;
	   	
    }
   
}

// =======================================================================================