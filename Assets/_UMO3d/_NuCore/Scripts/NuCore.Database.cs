﻿// =======================================================================================
// NU CORE - DATABASE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using Mono.Data.Sqlite; // copied from Unity/Mono/lib/mono/2.0 to Plugins
using System;
using System.Collections;

// =======================================================================================
// DATABASE
// =======================================================================================
public partial class Database {
	
	// -----------------------------------------------------------------------------------
	// Initialize_UMO3d_NuCoreDatabase
	// -----------------------------------------------------------------------------------
	static void Initialize_UMO3d_NuCoreDatabase() {
		ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS UMO3d_character_attributes ( character TEXT NOT NULL, attribute TEXT NOT NULL, value INTEGER NOT NULL)");
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterLoad_UMO3d_NuCoreDatabase
	// -----------------------------------------------------------------------------------
	static void CharacterLoad_UMO3d_NuCoreDatabase(Player player) {
		var table = ExecuteReader("SELECT attribute, value FROM UMO3d_character_attributes WHERE character=@name", new SqliteParameter("@name", player.name));
		foreach (var row in table) {
			player.UMO3d_AttributeSet( (string)row[0], Convert.ToInt32((long)row[1]) );
		}
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterSave_UMO3d_NuCoreDatabase
	// -----------------------------------------------------------------------------------
	static void CharacterSave_UMO3d_NuCoreDatabase(Player player) {
		ExecuteNonQuery("DELETE FROM UMO3d_character_attributes WHERE character=@character", new SqliteParameter("@character", player.name));
		for (int i = 0; i < player.UMO3d_Attributes.Count; ++i) {
			ExecuteNonQuery("INSERT INTO UMO3d_character_attributes VALUES (@character, @attribute, @value)",
 				new SqliteParameter("@character", player.name),
 				new SqliteParameter("@attribute", player.UMO3d_Attributes[i].name),
 				new SqliteParameter("@value", player.UMO3d_Attributes[i].value)
 				);
 		}		
	}	
	
}

// =======================================================================================