﻿// =======================================================================================
// UMO3d CONSTANTS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Constants {
		
  		// -----------------------------------------------------------------------------------
		// Attribute Names
		// -----------------------------------------------------------------------------------
		public const string UMO3d_ATTRIB_STR 				= "Strength";
		public const string	UMO3d_ATTRIB_INT			 	= "Intelligence";


  		// -----------------------------------------------------------------------------------
		// Extra Stat Names
		// -----------------------------------------------------------------------------------
		public const string	UMO3d_STAT_HEALTH				= "Health";
		public const string	UMO3d_STAT_MANA					= "Mana";
		public const string	UMO3d_STAT_DAMAGE				= "Damage";
		public const string	UMO3d_STAT_DEFENSE				= "Defense";
		public const string	UMO3d_STAT_BLOCK_CHANCE			= "Block Chance";
		public const string	UMO3d_STAT_CRIT_CHANCE			= "Critical Chance";
		public const string	UMO3d_STAT_HEALTH_REC			= "Health Recovery";
		public const string	UMO3d_STAT_MANA_REC				= "Mana Recovery";
		public const string	UMO3d_STAT_SPEED				= "Speed";
		public const string	UMO3d_STAT_BLOCK_EFFECT			= "Block Effect";
		public const string	UMO3d_STAT_CRIT_EFFECT			= "Critical Effect";
		
		public const string	UMO3d_TOOLTIP_BONUS				= " Bonus: ";
		
		// -----------------------------------------------------------------------------------
		// Attribute Bonus
		// 0.01f = each attribute point adds 1%
		// -----------------------------------------------------------------------------------	
		public const float	UMO3d_BONUS_HEALTHMAX			= 0.01f; 			// applied via Strength
		public const float 	UMO3d_BONUS_MANAMAX				= 0.01f; 			// applied via Intelligence
		
  		// -----------------------------------------------------------------------------------
		// Combat Modifiers
		// -----------------------------------------------------------------------------------
		public const float	UMO3d_MODIFIER_CRIT_MAX			= 1.0f;				// Crit Effect/Chance maximum a skill/item can modify (1.0f = 100%)
		public const float	UMO3d_MODIFIER_BLOCK_MAX		= 1.0f;				// Block Effect/Chance maximum a skill/item can modify (1.0f = 100%)
		public const float	UMO3d_MODIFIER_CRIT 			= 2.0f;				// Max crit damage = base damage * 2.0f
		public const float	UMO3d_MODIFIER_BLOCK 			= 0.5f;				// Max block damage = base damage * 0.5f
		public const float	UMO3d_MODIFIER_RANDOM			= 1.5f;				// Max damage randomization
																				// minimum = base damage
																				// maximum = base damage * 1.5f (default)
		
		
		
		
	}

}

// ===================================================================================