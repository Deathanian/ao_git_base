======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

AddOn Installation Guide
----------------------------------------------------------------------
AddOns are for uMMORPG3d Our AddOns are concipied for uMMORPG3d, they are useless without.
The installation process assumes that you are using a blank Unity project with the most
recent uMMORPG3d version already imported. We also highly recommend that you keep a backup
copy of your project every time you add a new AddOn.

Finally, always import AddOns one-by-one and do not try to import several AddOns at once.

AddOn Requirements
----------------------------------------------------------------------
All of our AddOns require our free NuTools and most of them require the free NuCore as well.

	1	The free NuTools are put into your shopping cart with each purchase, the NuTools
		are also available for direct download in our store.
		
	2	The free NuCore is only available for verified buyers of uMMORPG.
		You can download it using our NuCore Download Butler.

AddOn Import
----------------------------------------------------------------------
After download, import the Unity Package into your uMMORPG project. The import should
automatically be placed at the right location, you do not have to move around files unless
an AddOn says so. If you are using our other AddOns as well, all files will be kept inside
one folder in the root directory of your project.

AddOn Installation
----------------------------------------------------------------------
Follow the instructions as stated in the _Readme that comes with each downloaded Unity Package.

AddOn Installation Errors
----------------------------------------------------------------------
If you get any errors, here are the most common mistakes:

	1	Does the AddOn require our free NuCore? Is NuCore properly installed?
	
	2	Does the AddOn require our free NuTools? Are NuTools properly installed?
	
	3	Is the AddOn, NuCore and your uMMORPG version up to date?
	
	4	Did you follow exactly the installation process as stated in each of the AddOns _Readme?
	
	5	Did you clear your Scripting Define Symbols? 
	
	Go to: Edit->Project Settings->Player->Scripting Define Symbols (In the Inspector)
	and delete the Scripting Define Symbols, they will be re-generated automatically.

AddOn Upgrading
----------------------------------------------------------------------
We provide regular, free updates for our AddOns. You are encouraged to re-download the
Addons you acquired in one of our stores (either this one or the Unity Asset Store) to stay
up to date. To install, simply paste the new version over the old one. Most often thats all
you have to do. In very rare situations you need to drag the UI prefabs again onto your
Canvas. No need to replace the prefabs you already have in your scene! If you acquired the
AddOn via this store, here is how to get the free upgrades:
	
	1	Go to the “MyAccount” section above.
	
	2	Enter eMail and Password.
	
	3	If you forgot your password, the shop can resend one.
	
	4	On your account page, click “Downloads” to get the new version.

AddOn Compatibilty
----------------------------------------------------------------------
We do our best to ensure compatibility of our AddOns. But no guarantee can be given when
used in conjunction with your own modifications or 3rd party AddOns or Assets.

AddOn Legacy Versions
----------------------------------------------------------------------
We provide Legacy versions of most of our AddOns, these are suited for older versions of
uMMORPG. Please note that this installation process is aimed at users of the most recent
version though. Plus: Older versions are always less developed and prone to bugs and
glitches. As a rule of thumb: The higher the version, the better it is developed/debugged.

AddOn Support 
----------------------------------------------------------------------
If you still get errors or are stuck, head over to our Discord Support Server (recommended)
or drop us a line via eMail. Please note that we can only help verified buyers who are
using a blank project. Modded projects already contain so many changes that it is almost
impossible for us to be of any help!