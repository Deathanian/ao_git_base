﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class QuestDialogue
{
    [SerializeField] List<string> dialogues = new List<string>();
    int i = 0;

    public string[] dialogue
    {
        get
        {
            return dialogues.ToArray();
        }
    }

    public QuestDialogue(string[] dial)
    {
        this.dialogues = dial.ToList();
    }

    public string GetNextLine()
    {
        return dialogues[i++];
    }

    public bool hasNext
    { get { return i < dialogues.Count; } }

    public int Length
    {
        get
        {
            return dialogues.Count;
        }
    }

    internal void Restart()
    {
        i = 0;
    }
}