//#define UMMORPG_PROFESSIONS
//#define SIMPLE_WEATHER
//#define UMMORPG_SEASONS_SUN

// Saves the quest info in a ScriptableObject that can be used ingame by
// referencing it from a MonoBehaviour. It only stores an quest's static data.
//
// We also add each one to a dictionary automatically, so that all of them can
// be found by name without having to put them all in a database. Note that we
// have to put them all into the Resources folder and use Resources.LoadAll to
// load them. This is important because some quests may not be referenced by any
// entity ingame (e.g. after a special event). But all quests should still be
// loadable from the database, even if they are not referenced by anyone
// anymore. So we have to use Resources.Load. (before we added them to the dict
// in OnEnable, but that's only called for those that are referenced in the
// game. All others will be ignored be Unity.)
//
// A Quest can be created by right clicking the Resources folder and selecting
// Create -> Toon World Quest. Existing quests can be found in the Resources folder
using UMO3d;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace UMO3d {

	[CreateAssetMenu(fileName = "New Quest", menuName = "Create New Quest")]
	public partial class NewQuestTemplate : ScriptableObject
	{
		#region Fields

		/*****************************************************/
		[Header("General")]
		[TextArea(1, 30)]
		public string toolTip;

		public string visitNpcTooltip;

		public string visitPlaceTooltip;

		public string killTooltip;

		public string gatherTooltip;

	#if UMMORPG_PROFESSIONS

		public string harvestTooltip;

		public string professionsTooltip;

	#endif

		/******************************************************/
		[Header("Starting Requirement")]
	#if UMMORPG_SEASONS_SUN
		public bool canBeStartedDuringDay = true;
		public bool canBeStartedDuringNight = true;
	#endif

	#if SIMPLE_WEATHER
		public bool canBeStartedDuringRain = true;
		public bool canBeStartedDuringSun = true;
	#endif

		public int requiredLevel; // player.level
		public NewQuestTemplate predecessor; // this quest has to be completed first

		/*****************************************************/
		[Header("Rewards")]
		public long rewardGold;
		public long rewardExperience;
		public ItemTemplate rewardItem;

		/*****************************************************/
		[Header("Fulfillment")]
		public NewQuestTask[] tasksToComplete;

		/********************************************************/
		[Header("Dialogues")]
		public QuestDialogue startDialogue;

		#endregion

		#region private fields

	#if SIMPLE_WEATHER
		ObjectFinder<WeatherManager> weather = new ObjectFinder<WeatherManager>();
	#endif

	#if UMMORPG_SEASONS_SUN
		ObjectFinder<SunController> sun = new ObjectFinder<SunController>();
	#endif

		#endregion

		#region getter

		public bool CanBeStarted
		{
			get
			{
				bool b = true;
	#if SIMPLE_WEATHER
				b = b ? ((weather.val.GetWeather() == Weather.RAIN && canBeStartedDuringRain) ||
					(weather.val.GetWeather() == Weather.SUN && canBeStartedDuringSun)) : false;
	#endif

	#if UMMORPG_SEASONS_SUN
				b = b ? ((sun.val.isDay && canBeStartedDuringDay) ||
					(!sun.val.isDay && canBeStartedDuringNight)) : false;
	#endif
				return b;
			}
		}

	#endregion

		/************************STATIC*****************************/

		// caching /////////////////////////////////////////////////////////////////
		// we can only use Resources.Load in the main thread. we can't use it when
		// declaring static variables. so we have to use it as soon as 'dict' is
		// accessed for the first time from the main thread.
		static Dictionary<string, NewQuestTemplate> cache = null;
		public static Dictionary<string, NewQuestTemplate> Dict
		{
			get
			{
				// load if not loaded yet
				return cache ?? (cache = Resources.LoadAll<NewQuestTemplate>("").ToDictionary(
					quest => quest.name, quest => quest)
				);
			}
		}
	}

}