﻿[System.Serializable]
public partial struct NewQuestGather
{
    public ItemTemplate toGather;

    public int amount;

    public bool removeOnceGathered;

    public string name
    {
        get
        {
            return toGather.name;
        }
    }
}