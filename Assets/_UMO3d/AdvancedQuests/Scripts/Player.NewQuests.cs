//#define DIRECTION_POINTER
//#define UMMORPG_PROFESSIONS
//#define UMMORPG_MSG_FRAME
//#define NEW_QUESTS_USE_MSG_FRAME

using UMO3d;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public partial class Player
{
    public SyncListNewQuest newQuests = new SyncListNewQuest();

#if DIRECTION_POINTER

    Counter actualizePointerCounter = new Counter(10);

    GameObject ArrowPointer
    {
        get
        {
            return Resources.Load<GameObject>("Pointer");
        }
    }

    GameObject localPointer;

#endif

    [Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdCompleteNewQuestTask(int newQuestIndex)
    {
        NewQuest quest = newQuests[newQuestIndex];
        if (!quest.completed && quest.IsTaskFulfilled(Utils.ClientLocalPlayer()))
        {
            if (quest.ActualTask.onTaskFinished.Length > 0)
                TargetStartClientDialogue(connectionToClient, quest.ActualTask.onTaskFinished.dialogue);
            if (quest.IsLastTask)
            {
                for (int i = 0; i < quest.ActualTask.toGather.Length; i++)
                    if (quest.ActualTask.toGather[i].removeOnceGathered)
                        InventoryRemoveAmount(quest.ActualTask.toGather[i].name, quest.ActualTask.toGather[i].amount);

                // gain rewards
                gold += quest.RewardGold;
                experience += quest.RewardExperience;
                if (quest.RewardItem != null)
                    InventoryAddAmount(quest.RewardItem, 1);

                // complete quest
                quest.completed = true;
                quest.isTracked = false;
                newQuests[newQuestIndex] = quest;
#if DIRECTION_POINTER
                TargetSetNextTarget(connectionToClient);
#endif
            }
            else
            {
                for (int i = 0; i < quest.ActualTask.toGather.Length; i++)
                    if (quest.ActualTask.toGather[i].removeOnceGathered)
                        InventoryRemoveAmount(quest.ActualTask.toGather[i].name, quest.ActualTask.toGather[i].amount);
                quest.NextTask();
                newQuests[newQuestIndex] = quest;
            }
        }
    }

    [TargetRpc]
    private void TargetStartClientDialogue(NetworkConnection target, string[] dialogue)
    {
        QuestDialogue dial = new QuestDialogue(dialogue);
        StartClientDialogue(dial);
    }

    [Client]
    private void StartClientDialogue(QuestDialogue dialogue)
    {
        UIQuestDialogue.instance.StartDialogue(dialogue);
    }

    [Command(channel = Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdAcceptNewQuest(int npcQuestIndex)
    {
        // validate
        // use collider point(s) to also work with big entities
        if (state == "IDLE" &&
            target != null &&
            target.health > 0 &&
            target is Npc &&
            0 <= npcQuestIndex && npcQuestIndex < ((Npc)target).newQuests.Length &&
            Utils.ClosestDistance(collider, target.collider) <= interactionRange &&
            CanStartNewQuest(((Npc)target).newQuests[npcQuestIndex]))
        {
            var npcQuest = ((Npc)target).newQuests[npcQuestIndex];
            var q = new NewQuest(npcQuest)
            {
                isTracked = !newQuests.Any(a => !a.completed && a.isTracked),
                npcName = target.name
            };
            newQuests.Add(q);
            TargetStartClientDialogue(connectionToClient, newQuests.Last().Template.startDialogue.dialogue);
#if DIRECTION_POINTER
            TargetSetNextTarget(connectionToClient);
#endif
        }
    }

    public int GetNewQuestIndexByName(string questName)
    {
        return newQuests.FindIndex(quest => quest.name == questName);
    }

    public void IncreaseNewQuestKillCounterFor(string monsterName)
    {
        if (isServer && !isClient)//no double increase on server & play
            TargetIncreaseNewQuestKillCounterFor(connectionToClient, monsterName);
        for (int i = 0; i < newQuests.Count; ++i)
        {
            // active quest and not completed yet?
            if (!newQuests[i].completed)
                if (newQuests[i].killed.Any(ni => ni.name == monsterName))
                {
                    var quest = newQuests[i];
                    quest.killed.First(q => q.name == monsterName).value++;
                    newQuests[i] = quest;
                }
        }
    }

    [TargetRpc]
    private void TargetIncreaseNewQuestKillCounterFor(NetworkConnection connectionToClient, string monsterName)
    {
        IncreaseNewQuestKillCounterFor(monsterName);
    }

    // helper function to check if the player has completed a quest before
    public bool HasCompletedNewQuest(string questName)
    {
        return newQuests.Any(q => q.name == questName && q.completed);
    }

    // helper function to check if a player has an active (not completed) quest
    public bool HasActiveNewQuest(string questName)
    {
        return newQuests.Any(q => q.name == questName && !q.completed);
    }

    public bool CanStartNewQuest(NewQuestTemplate quest)
    {
        // not too many quests yet?
        // has required level?
        // not accepted yet?
        // has finished predecessor quest (if any)?
        return level >= quest.requiredLevel &&          // has required level?
               GetNewQuestIndexByName(quest.name) == -1 && // not accepted yet?
               (quest.predecessor == null || HasCompletedNewQuest(quest.predecessor.name));
    }

    public bool CanCompleteNewQuestTask(string questName)
    {
        // has the quest and not completed yet?
        int index = GetNewQuestIndexByName(questName);
        if (index != -1 && !newQuests[index].completed)
        {
            // fulfilled?
            var quest = newQuests[index];
            if (!quest.completed && quest.IsTaskFulfilled(this)) return true;
        }
        return false;
    }

    public bool NeedToVisitNpc(string n)
    {
        for (int i = 0; i < newQuests.Count; i++)
        {
            var q = newQuests[i];
            if (q.visitedNpcs.Any(b => b.name.Equals(n) && !b.value))
                return true;
        }
        return false;
    }

    public void DealDamageAt_NewQuests(HashSet<Entity> entities, int amount)
    {
        foreach (var e in entities)
            if (e is Monster && e.health == 0)
            {
                IncreaseNewQuestKillCounterFor(e.name);
            }
    }

    private void TryCompleteNewQuest(int newQuestId)
    {
        if (newQuests[newQuestId].ActualTask.taskAutoComplete && newQuests[newQuestId].IsTaskFulfilled(this) && (!newQuests[newQuestId].IsLastTask ||
            newQuests[newQuestId].RewardItem == null || InventoryCanAddAmount(newQuests[newQuestId].RewardItem, 1)))
        {
            CmdCompleteNewQuestTask(newQuestId);
        }
    }

#if DIRECTION_POINTER

    bool updateTarget = false;

    Counter updateTargetFrame = new Counter(20);

#endif

    public void LateUpdate_NewQuests()
    {
        for (int i = 0; i < newQuests.Count(); i++)
        {
            bool b = false;
            //the distance to the place to visit is less than the choosen distance
            if (!newQuests[i].completed && !newQuests[i].VisitedAllPlaces)
            {
                foreach (NamedVector3 nv in newQuests[i].ActualTask.placesToVisit)
                {
                    if (Vector3.Distance(transform.position, nv.vector) < newQuests[i].ActualTask.distanceToBeVisited)
                    {
                        NewQuest q = newQuests[i];
                        q.VisitPlace(nv.name);
                        newQuests[i] = q;
                        b = true;
#if DIRECTION_POINTER
                        SetNextTarget();
#endif
                    }
                }
            }
            if (b)
                TryCompleteNewQuest(i);
        }
#if DIRECTION_POINTER
        if (isClient && (localPointer == null || (updateTarget && updateTargetFrame.ready) || (!localPointer.activeSelf && actualizePointerCounter.ready && newQuests.Any(q => q.isTracked && !q.completed))))
            SetNextTarget();
#endif
    }

    void OnSelect_NewQuests(Entity entity)
    {
        if (entity is Npc)
        {
            bool anyVisit = false;
            for (int i = 0; i < newQuests.Count; ++i)
            {
                if (newQuests[i].completed) continue;
                bool b = false;
                var quest = newQuests[i];
                for (int j = 0; j < quest.ActualTask.npcsToVisit.Length; j++)
                {
                    if (entity != null &&
                        entity.health > 0 &&
                        Utils.ClosestDistance(collider, entity.collider) <= interactionRange &&
                        entity.name.Equals(quest.ActualTask.npcsToVisit[j].name) &&
                        !quest.visitedNpcs.Where(p => p.name.Equals(entity.name)).First().value)
                    {
#if UMMORPG_MSG_FRAME && NEW_QUESTS_USE_MSG_FRAME
                        ((Npc)entity).ShowDialogue(quest.ActualTask.npcsToVisit[j].OnVisitDialogue); 
#else
                        StartClientDialogue(quest.ActualTask.npcsToVisit[j].OnVisitDialogue);
#endif
                        quest.VisitNpc(quest.ActualTask.npcsToVisit[j].name);
                        if (isClient && !isServer)
                            CmdVisitNpc(i, quest.ActualTask.npcsToVisit[j].name);
                        newQuests[i] = quest;
                        b = true;
                        anyVisit = true;
#if DIRECTION_POINTER
                        localPointer.SetActive(false);
                        SetNextTarget();
#endif
                    }
                }
                if (b)
                    TryCompleteNewQuest(i);
            }
            if (anyVisit)
                FindObjectOfType<UINpcDialogue>().panel.SetActive(false);
        }
    }

    [Command]
    private void CmdVisitNpc(int qId, string name)
    {
        var q = newQuests[qId];
        q.VisitNpc(name);
        newQuests[qId] = q;
    }

#if DIRECTION_POINTER

    [TargetRpc]
    public void TargetSetNextTarget(NetworkConnection conn)
    {
        SetNextTarget();
    }

    public void SetNextTarget()
    {
        if (isServer && !isClient)
        {
            TargetSetNextTarget(connectionToClient);
            return;
        }
#if DIRECTION_POINTER
		if (localPointer == null)
        {
            GameObject go = Instantiate(ArrowPointer);
            go.SetActive(false);
			go.GetComponent<DirectionPointer>().toFollow = transform;
            localPointer = go;
        }
#endif
        else
            localPointer.SetActive(false);
        if (!newQuests.Any(qu => !qu.completed && qu.isTracked)) return;

        var q = newQuests.First(qu => !qu.completed && qu.isTracked);
        VorT b;
        object t = GetNextTarget(q, out b);
        if (b == VorT.VECTOR)
        {
#if DIRECTION_POINTER
            localPointer.GetComponent<DirectionPointer>().SetTarget((Vector3)t);
            localPointer.SetActive(true);
#endif
        }
        else if (b == VorT.TRANSFORM)
        {
#if DIRECTION_POINTER
            localPointer.GetComponent<DirectionPointer>().SetTarget((Transform)t);
            localPointer.SetActive(true);
#endif
        }
    }

    private object GetNextTarget(NewQuest q, out VorT b)
    {
        if (!q.VisitedAllPlaces)
        {
            updateTarget = false;
            b = VorT.VECTOR;
            return q.ActualTask.placesToVisit.First(nv => nv.name == q.visitedPlaces.First(nb => !nb.value).name).vector;
        }
        else if (!q.VisitedAllNpcs)
        {
            updateTarget = false;
            var v = GetObjectOfName<Npc>(q.visitedNpcs.First(nb => !nb.value).name);
            b = v != null ? VorT.TRANSFORM : VorT.NULL;
            return v ? v : null;
        }
        else if (!q.KilledAll)
        {
            updateTarget = true;
            Transform v = GetClosestMonster();
            b = v != null ? VorT.TRANSFORM : VorT.NULL;
            return v;
        }
#if UMMORPG_PROFESSIONS
        else if (!q.HarvestedAll)
        {
            updateTarget = true;
            Transform v = GetClosestCollectible();
            b = v != null ? VorT.TRANSFORM : VorT.NULL;
            return v;
        }
#endif
        var t = GetObjectOfName<Npc>(q.npcName);
        b = t == null ? VorT.NULL : VorT.TRANSFORM;
        return t;
    }

    private Transform GetObjectOfName<T>(string name) where T : Component
    {
        T[] t = FindObjectsOfType<T>();
        return t.Any(o => o.name == name) ? t.First(o => o.name == name).transform : null;
    }

    private Transform GetClosestMonster()
    {
        Monster[] t = FindObjectsOfType<Monster>();
        Transform v = null;
        float minDist = float.MaxValue;
        for (int i = 0; i < t.Length; i++)
        {
            if (t[i].health <= 0) continue;
            NewQuest nq = newQuests.First(q => !q.completed && q.isTracked);
            if (nq.ActualTask.targets.Any(tar => tar.name.Equals(t[i].name) && nq.killed.First(a => a.name == tar.name).value < tar.amount))
            {
                float f = Vector3.Distance(t[i].transform.position, transform.position);
                if (v == null || f < minDist)
                {
                    minDist = f;
                    v = t[i].transform;
                }
            }
        }
        return v;
    }

#if UMMORPG_PROFESSIONS
 
    private Transform GetClosestCollectible()
    {
        ResourceNode[] t = FindObjectsOfType<ResourceNode>();
        Transform v = null;
        float minDist = float.MaxValue;
        for (int i = 0; i < t.Length; i++)
        {
            if (t[i].health <= 0) continue;
            NewQuest nq = newQuests.First(q => !q.completed && q.isTracked);
            if (nq.ActualTask.toHarvest.Any(tar => tar.Name.Equals(t[i].name) && nq.harvested.First(a => a.name == tar.Name).value < tar.amount))
            {
                float f = Vector3.Distance(t[i].transform.position, transform.position);
                if (v == null || f < minDist)
                {
                    minDist = f;
                    v = t[i].transform;
                }
            }
        }
        return v;
    }

#endif

    private enum VorT
    {
        VECTOR,
        TRANSFORM,
        NULL
    }

#endif
        }
