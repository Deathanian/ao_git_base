//#define UMMORPG_PROFESSIONS

using UMO3d;

#if UMMORPG_PROFESSIONS

namespace UMO3d {

	[System.Serializable]
	public class ProfessionLevelTarget
	{
		public ProfessionTemplate profession;

		public int targetLevel;

		public string Name
		{
			get
			{
				return profession.name;
			}
		}
	}

}

#endif
