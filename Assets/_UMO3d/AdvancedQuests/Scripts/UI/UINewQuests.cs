﻿// Note: this script has to be on an always-active UI parent, so that we can
// always react to the hotkey.
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public partial class UINewQuests : MonoBehaviour
{
    public KeyCode hotKey = KeyCode.Q;
    public GameObject panel;
    public Transform content;
    public UINpcQuestSlot npcSlotPrefab;

    void Update()
    {
        var player = Utils.ClientLocalPlayer();
        if (!player) return;

        // hotkey (not while typing in chat, etc.)
        if (Input.GetKeyDown(hotKey) && !UIUtils.AnyInputActive())
            panel.SetActive(!panel.activeSelf);

        // only update the panel if it's active
        if (panel.activeSelf)
        {
            // only show active quests, no completed ones
            var activeQuests = player.newQuests.Where(q => !q.completed).ToList();

            // instantiate/destroy enough slots
            UIUtils.BalancePrefabs(npcSlotPrefab.gameObject, activeQuests.Count, content);

            // refresh all
            for (int i = 0; i < activeQuests.Count; ++i)
            {
                var slot = content.GetChild(i).GetComponent<UINpcQuestSlot>();
                var quest = activeQuests[i];
                slot.descriptionText.text = quest.ToolTip(player);
#if DIRECTION_POINTER
                slot.actionButton.GetComponentInChildren<Text>().text = quest.isTracked ? "Tracked" : "Track";
                slot.actionButton.interactable = !quest.isTracked;
                string qName = quest.name;
                slot.actionButton.onClick.SetListener(() =>
                {
                    for (int j = 0; j < player.newQuests.Count; j++)
                    {
                        var q = player.newQuests[j];
                        q.isTracked = j == player.GetNewQuestIndexByName(qName);
                        player.newQuests[j] = q;
                    }
                    player.SetNextTarget();
                });
#else
                slot.actionButton.gameObject.SetActive(false);
#endif
            }
        }

        // addon system hooks
        Utils.InvokeMany(typeof(UIQuests), this, "Update_");
    }

    public void InvertShow()
    {
        panel.SetActive(!panel.activeSelf);

        Utils.InvokeMany(typeof(UINewQuests), this, "InvertShow_");
    }
}
