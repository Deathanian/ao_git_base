﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class UIQuestDialogue : MonoBehaviour
{
    public GameObject panel;
    QuestDialogue dialogue;
    public Text text;
    public Button button;
    public static UIQuestDialogue instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        instance = this;

        Utils.InvokeMany(typeof(UIQuestDialogue), this, "Awake_");
    }

    private void Start()
    {
        button.onClick.AddListener(ButtonClicked);

        Utils.InvokeMany(typeof(UIQuestDialogue), this, "Start_");
    }

    private void ButtonClicked()
    {
        if(dialogue.hasNext)
            text.text = dialogue.GetNextLine();
        else
            panel.SetActive(false);

        Utils.InvokeMany(typeof(UIQuestDialogue), this, "ButtonClicked_");
    }

    public void StartDialogue(QuestDialogue d)
    {
        panel.SetActive(true);
        dialogue = d;
        dialogue.Restart();
        ButtonClicked();//simulate button click to load first text

        Utils.InvokeMany(typeof(UIQuestDialogue), this, "StartDialogue_");
    }

    public bool hasDialogue
    {
        get { return dialogue != null && dialogue.hasNext; }
    }
}
