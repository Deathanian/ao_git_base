﻿using UMO3d;
using UnityEngine;
using UnityEngine.UI;

public partial class UINpcDialogue
{
    [Header("New Quests")]
    public Button newQuestsButton;
    public UINpcNewQuests newQuestsPanel;

    private void Update_NewQuests()
    {
        var player = Utils.ClientLocalPlayer();
        if (!player) return;

        if (panel.activeSelf &&
            player.target != null && player.target is Npc &&
            Utils.ClosestDistance(player.collider, player.target.collider) <= player.interactionRange)
        {
            var npc = (Npc)player.target;

            newQuestsButton.gameObject.SetActive(npc.newQuests.Length > 0);
            if (npc.newQuests.Length > 0)
            {
                newQuestsButton.onClick.SetListener(() => {
                    newQuestsPanel.panel.SetActive(true);
                    panel.SetActive(false);
                });
            }
        }
    }
}