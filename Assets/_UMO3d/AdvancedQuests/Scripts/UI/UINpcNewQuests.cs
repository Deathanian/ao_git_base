﻿using UMO3d;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace UMO3d {

	public partial class UINpcNewQuests : MonoBehaviour
	{
		public GameObject panel;
		public UINpcQuestSlot slotPrefab;
		public Transform content;

		void Update()
		{
			var player = Utils.ClientLocalPlayer();
			if (!player) return;

			// npc quest
			// use collider point(s) to also work with big entities
			if (player.target != null && player.target is Npc &&
				Utils.ClosestDistance(player.collider, player.target.collider) <= player.interactionRange)
			{
				var npc = (Npc)player.target;

				// instantiate/destroy enough slots
				var questsAvailable = npc.NewQuestsVisibleFor(player);
				UIUtils.BalancePrefabs(slotPrefab.gameObject, questsAvailable.Count, content);

				// refresh all
				for (int i = 0; i < questsAvailable.Count; ++i)
				{
					var slot = content.GetChild(i).GetComponent<UINpcQuestSlot>();

					// find quest index in original npc quest list (unfiltered)
					int npcIndex = Array.FindIndex(npc.newQuests, q => q.name == questsAvailable[i].name);

					// find quest index in player quest list
					int questIndex = player.GetNewQuestIndexByName(npc.newQuests[npcIndex].name);
					if (questIndex != -1)
					{
						// running quest: shows description with current progress
						// instead of static one
						var quest = player.newQuests[questIndex];
						var reward = npc.newQuests[npcIndex].rewardItem;
						bool hasSpace = reward == null || player.InventoryCanAddAmount(reward, 1);//While single reward

						slot.descriptionText.text = quest.ToolTip(player);
						if (!hasSpace)
							slot.descriptionText.text += "\n<color=red>Not enough inventory space!</color>";

						slot.actionButton.interactable = quest.IsTaskFulfilled(player) && hasSpace;
						slot.actionButton.GetComponentInChildren<Text>().text = "Complete";
						int i2 = questIndex;
						slot.actionButton.onClick.SetListener(() => {
							player.CmdCompleteNewQuestTask(i2);
							panel.SetActive(false);
						});
					}
					else
					{
						// new quest
						slot.descriptionText.text = new NewQuest(npc.newQuests[npcIndex]).ToolTip();
						slot.actionButton.interactable = true;
						slot.actionButton.GetComponentInChildren<Text>().text = "Accept";
						slot.actionButton.onClick.SetListener(() => {
							player.CmdAcceptNewQuest(npcIndex);
							panel.SetActive(false);
						});
					}
				}
			}
			else panel.SetActive(false); // hide

			// addon system hooks
			Utils.InvokeMany(typeof(UINpcQuests), this, "Update_");
		}
	}

}