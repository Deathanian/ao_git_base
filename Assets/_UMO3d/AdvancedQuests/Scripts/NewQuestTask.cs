//#define UMMORPG_PROFESSIONS
using UMO3d;
using UnityEngine;

namespace UMO3d {

	[System.Serializable]
	public partial struct NewQuestTask
	{
		[Tooltip("If the task doesn't auto complete, the player will have to complete it at the original NPC")]
		public bool taskAutoComplete;

		public QuestDialogue onTaskFinished;

		public VisitableNPC[] npcsToVisit;

		public NamedVector3[] placesToVisit;
		public float distanceToBeVisited;

		public NewQuestTarget[] targets;
		public NewQuestGather[] toGather;

	#if UMMORPG_PROFESSIONS

		public NewQuestHarvest[] toHarvest;

		public ProfessionLevelTarget[] professionsLevelToReach;

	#endif
	}

}