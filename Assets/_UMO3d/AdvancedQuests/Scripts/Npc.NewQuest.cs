//#define UMMORPG_MSG_FRAME
//#define NEW_QUESTS_USE_MSG_FRAME

using UMO3d;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class Npc
{
    [Header("New Quests")]
    public NewQuestTemplate[] newQuests;

    public List<NewQuestTemplate> NewQuestsVisibleFor(Player player)
    {
        return newQuests.Where(q => (player.CanStartNewQuest(q) && q.CanBeStarted) ||
                                 player.HasActiveNewQuest(q.name)).ToList();
    }

    protected void UpdateClient_NewQuest()
    {
        if (questOverlay != null)
        {
            var player = Utils.ClientLocalPlayer();

            if (newQuests.Any(q => player.CanCompleteNewQuestTask(q.name)))
                questOverlay.text = "!";
            else if (newQuests.Any(q => player.CanStartNewQuest(q)))
                questOverlay.text = "?";
            else
                questOverlay.text = "";
        }
    }
#if UMMORPG_MSG_FRAME && NEW_QUESTS_USE_MSG_FRAME
    QuestDialogue actualDialogue;

    public void ShowDialogue(QuestDialogue dialogue)
    {
        actualDialogue = dialogue;
        actualDialogue.Restart();
        ShowNext();
    }

    private void ShowNext()
    {
        if (actualDialogue.hasNext)
            msgFrame.ShowMessage(actualDialogue.GetNextLine(), ShowNext);
    }
#endif
}
