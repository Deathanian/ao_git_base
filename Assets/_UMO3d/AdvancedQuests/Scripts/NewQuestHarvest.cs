//#define UMMORPG_PROFESSIONS

using UMO3d;

#if UMMORPG_PROFESSIONS

namespace UMO3d {

	[System.Serializable]
	public partial struct NewQuestHarvest
	{
		public ResourceNode target;

		public int amount;

		public string Name
		{
			get
			{
				return target.name;
			}
		}
	}

}

#endif
