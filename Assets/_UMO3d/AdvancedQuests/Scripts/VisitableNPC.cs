﻿using UnityEngine;

[System.Serializable]
public struct VisitableNPC
{
    public GameObject Npc;
    //public GameObject GoalLocation;//temp, I don't like using this
    public QuestDialogue OnVisitDialogue;

    public string name
    {
        get
        {
            return Npc != null?Npc.name:"";
        }
    }
}
