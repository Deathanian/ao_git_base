﻿[System.Serializable]
public partial struct NewQuestTarget
{
    public Monster target;

    public int amount;

    public string name
    {
        get
        {
            return target.name;
        }
    }
}