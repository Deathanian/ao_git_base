//#define UMMORPG_PROFESSIONS
using UMO3d;
using Mono.Data.Sqlite;
using System;

public partial class Database
{
    static void Initialize_NewQuests()
    {
        ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS character_new_quests (
                            character TEXT NOT NULL,
                            name TEXT NOT NULL,
                            taskIndex INTEGER,
                            completed INTEGER NOT NULL,
                            killed TEXT NOT NULL,
                            visitedPlaces TEXT NOT NULL,
                            visitedNPCs TEXT NOT NULL,
                            isTracked INTEGER NOT NULL,
                            npcName TEXT NOT NULL)");

#if UMMORPG_PROFESSIONS

        ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS character_new_quests_harvesting (
                            character TEXT NOT NULL,
                            name TEXT NOT NULL,
                            harvested TEXT NOT NULL)");

#endif
    }

    public static void CharacterLoad_NewQuests(Player player)
    {
        var table = ExecuteReader(
            "SELECT name, taskIndex, completed, killed, visitedPlaces, visitedNPCs, isTracked, npcName FROM character_new_quests WHERE character=@character",
            new SqliteParameter("@character", player.name));

        foreach (var row in table)
        {
            var quest = new NewQuest()
            {
                name = (string)row[0]
            };
            try
            {
                quest.taskIndex = Convert.ToInt32((long)row[1]);
            }
            catch
            {
                quest.taskIndex = 0;
            }
            quest.completed = ((long)row[2]) != 0; // sqlite has no bool
            quest.LoadKilled((string)row[3]);
            quest.LoadVisitedPlaces((string)row[4]);
            quest.LoadVisitedNPCs((string)row[5]);
            quest.isTracked = ((long)row[6]) != 0;
            quest.npcName = ((string)row[7]);

#if UMMORPG_PROFESSIONS

            var table2 = ExecuteReader(
                "SELECT harvested FROM character_new_quests_harvesting WHERE character=@character AND name=@name",
                new SqliteParameter("@character", player.name),
                new SqliteParameter("@name", quest.name));
            var row2 = table2[0];
            quest.LoadHarvested((string)row2[0]);

#endif

            player.newQuests.Add(quest.TemplateExists() ? quest : new NewQuest());
        }
    }

    public static void CharacterSave_NewQuests(Player player)
    {
        // quests: remove old entries first, then add all new ones
        ExecuteNonQuery("DELETE FROM character_new_quests WHERE character=@character", new SqliteParameter("@character", player.name));
#if UMMORPG_PROFESSIONS
        ExecuteNonQuery("DELETE FROM character_new_quests_harvesting WHERE character=@character", new SqliteParameter("@character", player.name));
#endif
        foreach (var quest in player.newQuests)
        {
            ExecuteNonQuery("INSERT INTO character_new_quests VALUES (@character, @name, @taskIndex, @completed, @killed, @visitedPlaces, @visitedNPCs, @isTracked, @npcName)",
                            new SqliteParameter("@character", player.name),
                            new SqliteParameter("@name", quest.name),
                            new SqliteParameter("@taskIndex", quest.taskIndex),
                            new SqliteParameter("@completed", Convert.ToInt32(quest.completed)),
                            new SqliteParameter("@killed", quest.GetKilledAsString()),
                            new SqliteParameter("@visitedPlaces", quest.GetVisitedPlacesAsString()),
                            new SqliteParameter("@visitedNPCs", quest.GetVisitedNPCsAsString()),
                            new SqliteParameter("@isTracked", Convert.ToInt32(quest.isTracked)),
                            new SqliteParameter("@npcName", quest.npcName));

#if UMMORPG_PROFESSIONS

            ExecuteNonQuery("INSERT INTO character_new_quests_harvesting VALUES (@character, @name, @harvested)",
                            new SqliteParameter("@character", player.name),
                            new SqliteParameter("@name", quest.name),
                            new SqliteParameter("@harvested", quest.GetHarvestedAsString()));

#endif

        }
    }
}
