//#define UMMORPG_PROFESSIONS

// The Quest struct only contains the dynamic quest properties and a name, so
// that the static properties can be read from the scriptable object. The
// benefits are low bandwidth and easy Player database saving (saves always
// refer to the scriptable quest, so we can change that any time).
//
// Quests have to be structs in order to work with SyncLists.

using UMO3d;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace UMO3d {

	[System.Serializable]
	public partial struct NewQuest
	{
		// name used to reference the database entry (cant save template directly
		// because synclist only support simple types)
		public string name;

		// dynamic stats
		public bool completed; // after finishing it at the npc and getting rewards

		public bool isTracked;

		public string npcName;

		public NamedInt[] killed;

		public NamedBool[] visitedPlaces;

		public NamedBool[] visitedNpcs;

	#if UMMORPG_PROFESSIONS

		public NamedInt[] harvested;

	#endif

		public int taskIndex;

		// constructors
		public NewQuest(NewQuestTemplate template)
		{
			name = template.name;
			completed = false;
			taskIndex = 0;
			visitedNpcs = null;
			visitedPlaces = null;
			killed = null;

	#if UMMORPG_PROFESSIONS
			harvested = null;
	#endif

			isTracked = true;
			npcName = "";
			visitedNpcs = new NamedBool[ActualTask.npcsToVisit.Length];
			visitedPlaces = new NamedBool[ActualTask.placesToVisit.Length];
			killed = new NamedInt[ActualTask.targets.Length];

	#if UMMORPG_PROFESSIONS
			harvested = new NamedInt[ActualTask.toHarvest.Length];
	#endif

			for (int i = 0; i < ActualTask.targets.Length; i++)
			{
				killed[i] = new NamedInt(ActualTask.targets[i].name, 0);
			}
			for (int i = 0; i < ActualTask.placesToVisit.Length; i++)
			{
				visitedPlaces[i] = new NamedBool(ActualTask.placesToVisit[i].name, false);
			}
			for (int i = 0; i < ActualTask.npcsToVisit.Length; i++)
			{
				visitedNpcs[i] = new NamedBool(ActualTask.npcsToVisit[i].name, false);
			}

	#if UMMORPG_PROFESSIONS
			for (int i = 0; i < ActualTask.toHarvest.Length; i++)
			{
				harvested[i] = new NamedInt(ActualTask.toHarvest[i].Name, 0);
			}
	#endif
		}

		// does the template still exist?
		public bool TemplateExists()
		{
			return NewQuestTemplate.Dict.ContainsKey(name);
		}

		// database quest property access
		public NewQuestTemplate Template
		{
			get { return NewQuestTemplate.Dict[name]; }
		}
		public int RequiredLevel
		{
			get { return Template.requiredLevel; }
		}
		public string Predecessor
		{
			get { return Template.predecessor != null ? Template.predecessor.name : ""; }
		}
		public long RewardGold
		{
			get { return Template.rewardGold; }
		}
		public long RewardExperience
		{
			get { return Template.rewardExperience; }
		}
		public ItemTemplate RewardItem
		{
			get { return Template.rewardItem; }
		}
		public NewQuestTask ActualTask
		{
			get
			{
				return Template.tasksToComplete[taskIndex];
			}
		}
		public bool VisitedAllPlaces
		{
			get
			{
				return !visitedPlaces.Any(p => !p.value);
			}
		}
		public bool VisitedAllNpcs
		{
			get
			{
				return !visitedNpcs.Any(p => !p.value);
			}
		}
		public bool KilledAll
		{
			get
			{
				bool allKilled = true;
				foreach (var m in ActualTask.targets)
				{
					if (killed.Where(a => a.name == m.name).First().value < m.amount)
						allKilled = false;
				}
				return allKilled;
			}
		}
		public bool GatheredAll(Player p)
		{
			bool allGathered = p != null;
			if (allGathered)
				foreach (var m in ActualTask.toGather)
				{
					if (p.InventoryCountAmount(m.name) < m.amount)
						return false;
				}
			return true;
		}

	#if UMMORPG_PROFESSIONS

		public bool HarvestedAll
		{
			get
			{
				bool allHarvested = true;
				foreach (var m in ActualTask.toHarvest)
				{
					if (harvested.Where(a => a.name == m.Name).First().value < m.amount)
						allHarvested = false;
				}
				return allHarvested;
			}
		}

		public bool ReachAllProfessionsLevels(Player p)
		{
			foreach (var prof in ActualTask.professionsLevelToReach)
			{
				if (!p.professions.Any(pr => pr.templateName == prof.Name) || p.GetProfession(prof.Name).Level < prof.targetLevel)
					return false;
			}
			return true;
		}

	#endif

		#region Tooltip

		// fill in all variables into the tooltip
		// this saves us lots of ugly string concatenation code. we can't do it in
		// QuestTemplate because some variables can only be replaced here, hence we
		// would end up with some variables not replaced in the string when calling
		// Tooltip() from the template.
		// -> note: each tooltip can have any variables, or none if needed
		// -> example usage:
		/*
		<b>{NAME}</b>
		Description here...

		Tasks:
		{VISITPLACE}
		{VISITNPC}
		{KILL}
		{GATHER}
		{TOHARVEST}
		{PROFESSIONS}

		Rewards:
		* {REWARDGOLD} Gold
		* {REWARDEXPERIENCE} Experience
		* {REWARDITEM}

		{STATUS}
		*/
		public string ToolTip(Player p = null)
		{
			// we use a StringBuilder so that addons can modify tooltips later too
			// ('string' itself can't be passed as a mutable object)
			var tip = new StringBuilder(Template.toolTip);
			tip.Replace("{NAME}", name);
			tip.Replace("{REWARDGOLD}", RewardGold.ToString());
			tip.Replace("{REWARDEXPERIENCE}", RewardExperience.ToString());
			tip.Replace("{REWARDITEM}", RewardItem != null ? RewardItem.name : "");
			try
			{
				tip.Replace("{STATUS}", p == null ? "" : IsTaskFulfilled(p) ? "<i>Completed!</i>" : "");
			}
			catch
			{
				tip.Replace("{STATUS}", "");
			}

			// addon system hooks
			Utils.InvokeMany(typeof(NewQuest), this, "ToolTip_", tip, p);

			return tip.ToString();
		}

		/*
		 * Visit Place {PLACENAME} : {VISITDONE}
		 */
		public void ToolTip_Places(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.placesToVisit != null)
				for (int i = 0; i < ActualTask.placesToVisit.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.visitPlaceTooltip);
					placeTip.Replace("{PLACENAME}", ActualTask.placesToVisit[i].name);
					var a = ActualTask;
					placeTip.Replace("{VISITDONE}", visitedPlaces.Where(vn => vn.name == a.placesToVisit[i].name).First().value ? "Done" : "");

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{VISITPLACE}", s);
		}

		/*
		 * Visit Npc {NPCNAME} : {VISITDONE}
		 */
		public void ToolTip_Npcs(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.npcsToVisit != null)
				for (int i = 0; i < ActualTask.npcsToVisit.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.visitNpcTooltip);
					placeTip.Replace("{NPCNAME}", ActualTask.npcsToVisit[i].name);
					var a = ActualTask;
					placeTip.Replace("{VISITDONE}", visitedNpcs.Where(vn => vn.name == a.npcsToVisit[i].name).First().value ? "Done" : "");

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{VISITNPC}", s);
		}

		/*
		 * Kill {TARGETNAME} : {KILL}/{AMOUNT}
		 */
		public void ToolTip_Kills(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.targets != null)
				for (int i = 0; i < ActualTask.targets.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.killTooltip);
					placeTip.Replace("{TARGETNAME}", ActualTask.targets[i].name);
					var a = ActualTask;
					placeTip.Replace("{KILL}", killed.Where(m => m.name == a.targets[i].name).First().value.ToString());
					placeTip.Replace("{AMOUNT}", ActualTask.targets[i].amount.ToString());

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{KILL}", s);
		}

		/*
		 * Gather {GATHERNAME} : {GATHERED}/{TOGATHER}
		 */
		public void ToolTip_Gather(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.toGather != null)
				for (int i = 0; i < ActualTask.toGather.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.gatherTooltip);
					placeTip.Replace("{GATHERNAME}", ActualTask.toGather[i].name);
					var a = ActualTask;
					if (p != null)
						placeTip.Replace("{GATHERED}", p.InventoryCountAmount(ActualTask.toGather[i].name).ToString());
					else
						placeTip.Replace("{GATHERED}", "0");
					placeTip.Replace("{TOGATHER}", ActualTask.toGather[i].amount.ToString());

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{GATHER}", s);
		}

	#if UMMORPG_PROFESSIONS

		/*
		 * Harvest {TOHARVESTNAME} : {HARVESTED}/{AMOUNT}
		 */
		public void ToolTip_Harvest(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.toHarvest != null)
				for (int i = 0; i < ActualTask.toHarvest.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.harvestTooltip);
					placeTip.Replace("{TOHARVESTNAME}", ActualTask.toHarvest[i].Name);
					var a = ActualTask;
					placeTip.Replace("{HARVESTED}", harvested.First(m => m.name == a.toHarvest[i].Name).value.ToString());
					placeTip.Replace("{AMOUNT}", ActualTask.toHarvest[i].amount.ToString());

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{TOHARVEST}", s);
		}

		/*
		 * Reach level {LEVEL} of profession {PROFESSION} - {DONE}
		 */
		public void ToolTip_Professions(StringBuilder tip, Player p)
		{
			string s = "";
			if (ActualTask.professionsLevelToReach != null)
				for (int i = 0; i < ActualTask.professionsLevelToReach.Length; i++)
				{
					StringBuilder placeTip = new StringBuilder(Template.professionsTooltip);
					placeTip.Replace("{LEVEL}", ActualTask.professionsLevelToReach[i].targetLevel.ToString());
					placeTip.Replace("{PROFESSION}", ActualTask.professionsLevelToReach[i].Name);
					placeTip.Replace("{DONE}", 
						p != null &&
						p.HasProfession(ActualTask.professionsLevelToReach[i].Name) && 
						p.GetProfession(ActualTask.professionsLevelToReach[i].Name).Level >= ActualTask.professionsLevelToReach[i].targetLevel ? 
						"Done" : "");

					s += (s == "" ? "" : "\n") + placeTip.ToString();
				}
			tip.Replace("{PROFESSIONS}", s);
		}

	#endif

		#endregion

		public bool IsTaskFulfilled(Player p)
		{
			bool allKilled = KilledAll;
			bool allVisitedPlace = VisitedAllPlaces;
			bool allVisitedNpc = VisitedAllNpcs;
			bool allGathered = GatheredAll(p);

			bool b = allVisitedNpc && allVisitedPlace && allKilled && allGathered;

	#if UMMORPG_PROFESSIONS
			b = b ? HarvestedAll && ReachAllProfessionsLevels(p) : false;
	#endif

			return b;
		}

		public bool IsLastTask
		{
			get
			{
				return taskIndex == Template.tasksToComplete.Length - 1;
			}
		}

		public void NextTask()
		{
			if (!IsLastTask)
			{
				taskIndex++;
				visitedNpcs = new NamedBool[ActualTask.npcsToVisit.Length];
				visitedPlaces = new NamedBool[ActualTask.placesToVisit.Length];
				killed = new NamedInt[ActualTask.targets.Length];
				for (int i = 0; i < ActualTask.targets.Length; i++)
				{
					killed[i] = new NamedInt(ActualTask.targets[i].name, 0);
				}
				for (int i = 0; i < ActualTask.placesToVisit.Length; i++)
				{
					visitedPlaces[i] = new NamedBool(ActualTask.placesToVisit[i].name, false);
				}
				for (int i = 0; i < ActualTask.npcsToVisit.Length; i++)
				{
					visitedNpcs[i] = new NamedBool(ActualTask.npcsToVisit[i].name, false);
				}
	#if UMMORPG_PROFESSIONS
				harvested = new NamedInt[ActualTask.toHarvest.Length];
				for (int i = 0; i < ActualTask.toHarvest.Length; i++)
				{
					harvested[i] = new NamedInt(ActualTask.toHarvest[i].Name, 0);
				}
	#endif
			}
		}

		public void VisitPlace(string placeName)
		{
			if (visitedPlaces.Any(p => p.name == placeName))
				visitedPlaces.Where(p => p.name == placeName).First().value = true;
		}

		public void VisitNpc(string npcName)
		{
			if (visitedNpcs.Any(p => p.name == npcName))
				visitedNpcs.Where(p => p.name == npcName).First().value = true;
		}

		#region Database

		public string GetKilledAsString()
		{
			string s = "";
			if (killed != null)
				foreach (var m in killed)
				{
					s += (s == "" ? "" : ";") + m.name + ":" + m.value;
				}
			return s;
		}

		public void LoadKilled(string k)
		{
			if (k == "") return;
			string[] killedString = k.Split(';');
			killed = new NamedInt[killedString.Length];
			for (int i = 0; i < killedString.Length; i++)
			{
				string[] ni = killedString[i].Split(':');
				killed[i] = new NamedInt(ni[0], int.Parse(ni[1]));
			}
		}

		public string GetVisitedPlacesAsString()
		{
			string s = "";
			foreach (var m in visitedPlaces)
			{
				if (m.value)
					s += (s == "" ? "" : ";") + m.name;
			}
			return s;
		}

		public void LoadVisitedPlaces(string v)
		{
			string[] visited = v.Split(';');
			visitedPlaces = new NamedBool[ActualTask.placesToVisit.Length];
			int j = 0;
			for (int i = 0; i < visitedPlaces.Length; i++)
			{
				visitedPlaces[i] = new NamedBool(ActualTask.placesToVisit[i].name, j >= visited.Length ? false : visited[j] == ActualTask.placesToVisit[i].name);
				if (j < visited.Length && visited[j] == ActualTask.placesToVisit[i].name)
					j++;
			}
		}

		public string GetVisitedNPCsAsString()
		{
			string s = "";
			foreach (var m in visitedNpcs)
			{
				if (m.value)
					s += (s == "" ? "" : ";") + m.name;
			}
			return s;
		}

		public void LoadVisitedNPCs(string v)
		{
			string[] visited = v.Split(';');
			visitedNpcs = new NamedBool[ActualTask.npcsToVisit.Length];
			int j = 0;
			for (int i = 0; i < visitedNpcs.Length; i++)
			{
				visitedNpcs[i] = new NamedBool(ActualTask.npcsToVisit[i].name, j >= visited.Length ? false : visited[j] == ActualTask.npcsToVisit[i].name);
				if (j < visited.Length && visited[j] == ActualTask.npcsToVisit[i].name)
					j++;
			}
		}

	#if UMMORPG_PROFESSIONS

		public string GetHarvestedAsString()
		{
			string s = "";
			if (harvested != null)
				foreach (var m in harvested)
				{
					s += (s == "" ? "" : ";") + m.name + ":" + m.value;
				}
			return s;
		}

		public void LoadHarvested(string h)
		{
			if (h == "") return;
			string[] harvestedString = h.Split(';');
			harvested = new NamedInt[harvestedString.Length];
			for (int i = 0; i < harvested.Length; i++)
			{
				string[] ni = harvestedString[i].Split(':');
				harvested[i] = new NamedInt(ni[0], int.Parse(ni[1]));
			}
		}

	#endif

		#endregion
	}

	public class SyncListNewQuest : SyncListStruct<NewQuest> { }

}