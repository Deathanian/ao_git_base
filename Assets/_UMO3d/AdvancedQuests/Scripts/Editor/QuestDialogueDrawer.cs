﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(QuestDialogue))]
public class QuestDialogueDrawer : PropertyDrawer
{
    public Dictionary<string, bool> states = new Dictionary<string, bool>();

    float fieldHeight = 18;
    float spaceHeight = 2;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (!states.ContainsKey(property.name))
            states.Add(property.name, false);
        bool actualState = states[property.name];
        if (actualState)
            return fieldHeight + (property.FindPropertyRelative("dialogues").arraySize + 1) * (fieldHeight + spaceHeight);
        else
            return fieldHeight;
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (!states.ContainsKey(property.name))
            states.Add(property.name, false);
        bool actualState = states[property.name];
        Rect foldoutPos = new Rect(position.x, position.y, position.width, fieldHeight);
        states[property.name] = EditorGUI.Foldout(foldoutPos, actualState, property.displayName,true);
        if(actualState)
        {
            SerializedProperty arr = property.FindPropertyRelative("dialogues");
            int arraySize = arr.arraySize;
            Rect nextRect = new Rect(position.x, position.position.y + fieldHeight + spaceHeight, position.width, fieldHeight);
            EditorGUI.indentLevel++;
            nextRect = EditorGUI.IndentedRect(nextRect);

            Rect nextButton = new Rect(position.x, position.y + fieldHeight + spaceHeight, 25, fieldHeight);
            List<int> toRemove = new List<int>();
            for (int i = 0; i < arraySize; i++)
            {
                if (GUI.Button(nextButton, "X"))
                    toRemove.Add(i);
                EditorGUI.PropertyField(nextRect, arr.GetArrayElementAtIndex(i), GUIContent.none);
                nextRect.position += new Vector2(0, fieldHeight + spaceHeight);
                nextButton.position += new Vector2(0, fieldHeight + spaceHeight);
            }
            for (int i = 0; i < toRemove.Count; i++)
                arr.DeleteArrayElementAtIndex(toRemove[i]);
            nextRect.x = position.x;
            nextRect.width = position.width;
            if (GUI.Button(nextRect, "Add line")) 
            {
                arr.arraySize++;
                arr.InsertArrayElementAtIndex(arr.arraySize - 1);
            }
            /*if(toRemove.Count > 0)
                property.up*///need to set dirty?
            EditorGUI.indentLevel--;
        }
    }
}