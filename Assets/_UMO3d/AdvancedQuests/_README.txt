﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Install this AddOn.

Find the prefabs folder in this AddONs folder and select all three "NewQuestsUI",
"NpcNewQuestsUI" and "QuestDialogueUI" and put it into your scenes canvas.

Add new quests by duplicating and editing the two example quests found in the resources
folder of the AddOn

See this installation video by Laffy for more information:

https://www.youtube.com/watch?v=9C51V0UlRck


DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn will greatly expand the quest system in your project.

Quest Dialogue System
Add NPC(s) to visit as a quest requirement
Add place(s) to go to as a quest requirement
Weather/Day/Night conditions to spawn a quest
Multi-chain tasks into one quests
Ability to autocomplete quests
No script modifications required

======================================================================