// =======================================================================================
// SIMPLE STAMINA - CONSTANTS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Constants {
		
	  	// -----------------------------------------------------------------------------------
		// Constants
		// -----------------------------------------------------------------------------------
		public const bool 	UMO3d_EXHAUSTED_BLOCK_RECOVER_HEALTH = true;
		public const bool 	UMO3d_EXHAUSTED_BLOCK_RECOVERY_MANA	= true;
  		public const float 	UMO3d_EXHAUSTED_EXP_PENALTY			= 0.5f;
		public const int 	UMO3d_STAMINA_PENALTY					= 2;
		
		// -----------------------------------------------------------------------------------

	}

}	
// ===================================================================================