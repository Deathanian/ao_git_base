﻿// =======================================================================================
// SIMPLE STAMINA - SKILL TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// SKILL TEMPLATE
// =======================================================================================
public partial class SkillTemplate : ScriptableObject {
   
    public partial struct SkillLevel {
       
        [Header("SimpleStamina - Modifiers")]
#pragma warning disable
		public int healsStamina;
		public int buffsStaminaMax;
		public float buffsStaminaPercentPerSecond; // 0.1=10%; can be negative too
#pragma warning restore

    }
   
}

// =======================================================================================