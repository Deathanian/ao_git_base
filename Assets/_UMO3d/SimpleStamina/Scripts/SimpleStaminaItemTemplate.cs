﻿// =======================================================================================
// SIMPLE STAMINA - ITEM TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// ITEM TEMPLATE
// =======================================================================================
public partial class ItemTemplate : ScriptableObject {
   	
        [Header("UMO3d SimpleStamina - Modifiers")]
		public int usageStamina;
		public int equipStaminaBonus;
		public float equipStaminaRecoveryBonus;
   	
}

// =======================================================================================