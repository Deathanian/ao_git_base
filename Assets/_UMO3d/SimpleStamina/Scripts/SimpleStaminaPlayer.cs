﻿// =======================================================================================
// SIMPLE STAMINA - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	[HideInInspector] public bool UMO3d_exhausted = false;
	
	protected const string UMO3d_MSG_EXHAUSTED_ON 		= "You are exhausted!";	
	protected const string UMO3d_MSG_EXHAUSTED_OFF 	= "You feel refreshed.";	
	
	// -----------------------------------------------------------------------------------
	// PlayerLevel
    // -----------------------------------------------------------------------------------
    public partial class PlayerLevel {
        public int staminaMax = 100;														// NuCore
    }
    
	// -----------------------------------------------------------------------------------
	// Stamina
    // -----------------------------------------------------------------------------------
	[Header("SimpleStamina")]
    [SyncVar] int _stamina = 1;
    public int stamina {
        get { return Mathf.Min(_stamina, staminaMax); } 									// min in case hp>hpmax after buff ends etc.
        set { 
        
        	_stamina = Mathf.Clamp(value, 0, staminaMax);
        	
        	if (_stamina <= 0) {
        		if (!UMO3d_exhausted) {
        			if (healthRecovery && Constants.UMO3d_EXHAUSTED_BLOCK_RECOVER_HEALTH) healthRecovery = false;
        			if (manaRecovery && Constants.UMO3d_EXHAUSTED_BLOCK_RECOVERY_MANA) manaRecovery = false;
        			UMO3d_exhausted = true;
#if _FreeInfoBox
					GetComponent<InfoBox>().TargetAddMessage(connectionToClient, UMO3d_MSG_EXHAUSTED_ON);
#else
					GetComponent<Chat>().TargetMsgInfo(connectionToClient, UMO3d_MSG_EXHAUSTED_ON);
#endif	
        		}
        	} else if (_stamina > 0) {
        		if (UMO3d_exhausted) {
        			if (!healthRecovery && Constants.UMO3d_EXHAUSTED_BLOCK_RECOVER_HEALTH) healthRecovery = true;
        			if (!manaRecovery && Constants.UMO3d_EXHAUSTED_BLOCK_RECOVERY_MANA) manaRecovery = true;
        			UMO3d_exhausted = false;
#if _FreeInfoBox
					GetComponent<InfoBox>().TargetAddMessage(connectionToClient, UMO3d_MSG_EXHAUSTED_OFF);
#else
					GetComponent<Chat>().TargetMsgInfo(connectionToClient, UMO3d_MSG_EXHAUSTED_OFF);
#endif
        		}

        	}
        
        }
    }
    
    public bool staminaRecovery = true; 													// can be disabled in combat etc.
    public int baseStaminaRecoveryRate = 0;
    public int staminaRecoveryRate {
        get {
            // base + buffs
            float equipmentBonus = 0.0f;
            
            equipmentBonus = (from item in equipment
                                 where item.valid
                                 select item.equipStaminaRecoveryBonus).Sum();
           
            float buffPercent = 0.0f;
            
            buffPercent = (from skill in skills
                                 where skill.BuffTimeRemaining() > 0
                                 select skill.buffsStaminaPercentPerSecond).Sum();

            return baseStaminaRecoveryRate + Convert.ToInt32(equipmentBonus * staminaMax) + Convert.ToInt32(buffPercent * staminaMax);
        }
    }
    
	// -----------------------------------------------------------------------------------
	// staminaMax
    // -----------------------------------------------------------------------------------
    public int staminaMax {																	// NuCore
        get {
        
        	int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                  where item.valid
                                  select item.equipStaminaBonus).Sum();
#endif

			int buffBonus = 0;

            buffBonus = (from skill in skills
                             where skill.BuffTimeRemaining() > 0
                             select skill.buffsStaminaMax).Sum();


			int attributeBonus = 0;
#if _SimpleAttributes
#pragma warning disable
			if (Constants.UMO3d_ATRIB_ACTIVE9) {
				if (Constants.UMO3d_SCALE_STAMINA) {
					attributeBonus = Convert.ToInt32(levels[level-1].staminaMax * (attribute9 * Constants.UMO3d_BONUS_STAMINA));
				} else {
					attributeBonus = Convert.ToInt32(attribute9 * Constants.UMO3d_BONUS_STAMINA);
				}
			}
#pragma warning restore
#endif
           
            return levels[level-1].staminaMax + equipmentBonus + buffBonus + attributeBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// StaminaPercent
    // -----------------------------------------------------------------------------------
    public float StaminaPercent() {
        return (stamina != 0 && staminaMax != 0) ? (float)stamina / (float)staminaMax : 0;
    }

	// =======================================================================================

}

// =======================================================================================