﻿// =======================================================================================
// SIMPLE STAMINA - SKILL
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// SKILL
// =======================================================================================
public partial struct Skill {
	
	// ---------- Level dependant Stats
	
	public int healsStamina {
    	get { return template.levels[level-1].healsStamina; }
    }
	
	public int buffsStaminaMax {
    	get { return template.levels[level-1].buffsStaminaMax; }
    }

    public float buffsStaminaPercentPerSecond {
        get { return template.levels[level-1].buffsStaminaPercentPerSecond; }
    }
	
	// ---------- ToolTip
	
	public void ToolTip_UMO3d_SimpleStaminaSkill(StringBuilder tip) {
       	
        if (category.StartsWith("Buff")) {
        	tip.Append("\n");
        	if (buffsStaminaMax != 0) 					tip.Append( "Buffs max Stamina: "			+ buffsStaminaMax.ToString() 	+ "%\n");
        	if (buffsStaminaPercentPerSecond != 0) 		tip.Append( "Buffs Stamina % per Second:  "	+ Mathf.RoundToInt(buffsStaminaPercentPerSecond*100).ToString() 	+ "%\n");
        	
        } else {
        
        	if (healsStamina != 0) 						tip.Append( "Recovers Stamina: "			+ healsStamina.ToString() 	+ "%\n");
        
        }
    } 
}

// =======================================================================================