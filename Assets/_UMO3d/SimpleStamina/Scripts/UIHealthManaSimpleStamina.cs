﻿// =======================================================================================
// SIMPLE STAMINA - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using UnityEngine.UI;

public partial class UIHealthMana : MonoBehaviour {
    
    public Slider staminaSlider;
    public Text staminaStatus;
   
    void Update_UIHealthManaSimpleStamina() {
        var player = Utils.ClientLocalPlayer();
        panel.SetActive(player != null); // hide while not in the game world
        if (!player) return;

        staminaSlider.value = player.StaminaPercent();
        staminaStatus.text = player.stamina + " / " + player.staminaMax;

    }
    
}

// =======================================================================================