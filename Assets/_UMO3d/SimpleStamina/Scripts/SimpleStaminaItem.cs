﻿// =======================================================================================
// SIMPLE STAMINA - ITEM
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// ITEM
// =======================================================================================
public partial struct Item {

	public int usageStamina {
		get { return template.usageStamina; }
	}

	public int equipStaminaBonus {
		get { return template.equipStaminaBonus; }
	}

	public float equipStaminaRecoveryBonus {
		get { return template.equipStaminaRecoveryBonus; }
	}
	
	
	// ---------- ToolTip
	
	public void ToolTip_UMO3d_SimpleStaminaItem(StringBuilder tip) {
       	
        if (category.StartsWith("Equipment")) {
        	tip.Append("\n");
        	if (equipStaminaBonus != 0) 			tip.Append( "Stamina Bonus: " + equipStaminaBonus.ToString() 	+ "\n");
        	if (equipStaminaRecoveryBonus != 0) 	tip.Append( "Stamina Recovery Bonus: " + Mathf.RoundToInt(equipStaminaRecoveryBonus * 100).ToString() + "%\n");        	
        } else {
        	if (usageStamina !=0) tip.Replace("Restores ", usageStamina.ToString() + " Stamina on use");
        }
    } 

	
}

// =======================================================================================