﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires the NuCore: http://www.unity-mmo.com/butler

First, Install the most recent version of NuCore.

2. Next, install this AddOn.

3. Go to your Canvas in the scene and de-activate the "HealthMana" window. You dont have to
delete it (so you can still receive official updates), de-activation is enough.

4. Drag-n-Drop the new HealthManaUI from this AddOns prefabs directory into your canvas.
Make sure its placed inside your canvas but not inside any other object.

5. You can adjust the stamina settings by editing "SimpleStaminaConstants" in this
AddOns prefabs directory.

Thats all, no core modifications!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn adds a energy system to your game!

- All actions (except IDLE) use stamina (at 1 stamina per second)
- Add (food) items to replenish player stamina
- Once a player runs out of stamina:
	- Health recovery is blocked (otpional)
	- Mana recovery is blocked (optional)
	- Experience gain is reduced (adjustable)
	- Certain items cannot be used/equipped anymore (adjustable per item)
	- Certain skills cannot be activated anymore (adjustable per skill)

Use in combination with SimpleSanctuary to let your players recover Stamina while offline.

This AddOn unlocks new features on my SimpleSanctuary and SimpleDamageFloor as well as
SimpleAttributes and SimpleUsageLimits AddOns.

======================================================================