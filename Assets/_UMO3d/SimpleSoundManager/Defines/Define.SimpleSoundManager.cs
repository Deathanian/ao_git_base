﻿// =======================================================================================
// DEFINES
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class Define_UMO3d_SimpleSoundManager
{
    const string define = "_SimpleSoundManager";
    
	const string nucore = "_NuCore";
	const bool required = true;
	const int version = 113;
	
    static Define_UMO3d_SimpleSoundManager(){ AddLibrayDefineIfNeeded(); }

    static void AddLibrayDefineIfNeeded() {
        // Get defines.
        BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

        // Append only if not defined already.
        #pragma warning disable
        if (required && !defines.Contains(nucore)) {
            Debug.LogWarning("WARNING! This AddOn requires <b>NuCore v"+version+"</b> - free download: http://www.unity-mmo.com/butler - If installed already, please restart Unity.");
            //return;
        }
        #pragma warning restore

        // Append.
        if (defines.Contains(define)){ return; }

        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, (defines + ";" + define));
        //Debug.LogWarning("<b>" + define + "</b> added to <i>Scripting Define Symbols</i> for selected build target (" + EditorUserBuildSettings.activeBuildTarget.ToString() + ").");
    }
}

#endif

// =======================================================================================