﻿﻿using UnityEngine;

public class SoundNode {

    private AudioSource audioSource;
    private AudioClip[] audioClips;
    private float vol;
    private float soundDelay;

    private float transitionOffset {
        get {
            return 0.01f;
        }
    }

    public SoundNode(AudioSource audioSource, AudioClip[] audioClips, float vol, float soundDelay) {
        this.audioClips = audioClips;
        this.audioSource = audioSource;
        this.vol = vol;
        this.soundDelay = soundDelay;
    }

    public void Play(int i) {
        audioSource.PlayOneShot(audioClips[i], vol);
    }

	public bool canPlay(int i, float counter)
	{
        return audioClips.Length != 0 && counter > Mathf.Clamp(audioClips[i].length + soundDelay, transitionOffset, audioClips[i].length + soundDelay - transitionOffset);
	}

    public void switchAudio(int index1, int index2) {
        AudioClip temp = audioClips[index1];
        audioClips[index1] = audioClips[index2];
        audioClips[index2] = temp;
    }

    public AudioClip[] AudioClips(){
        return audioClips;
    }
}
