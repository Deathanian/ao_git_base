//#define UMMORPG_SEASONS_SUN
using UnityEngine;

public partial class Player
{
    [Header("Player Sound Manager")]
    [SerializeField]
    AudioClip[] footsteps;
    [SerializeField, Range(0, 1)]
    float footstepsVol;
    [SerializeField]
    float footstepsDelay;
    [SerializeField]
    AudioClip[] death;
    [SerializeField, Range(0, 1)]
    float deathVol;
    [SerializeField]
    float deathDelay;
    [SerializeField]
    AudioClip[] trading;
    [SerializeField, Range(0, 1)]
    float tradingVol;
    [SerializeField]
    float tradingDelay;
    [SerializeField]
    AudioClip[] casting;
    [SerializeField, Range(0, 1)]
    float castingVol;
    [SerializeField]
    float castingDelay;

    private SoundNode bgmNode;
    private SoundNode footStepsNode;
    private SoundNode deathNode;
    private SoundNode tradingNode;
    private SoundNode castingNode;
#if UMMORPG_SEASONS_SUN
    private SoundNode nightNode;
    private SoundNode dayNode;
#endif
#if UMMORPG_SEASONS_SUN
	private ObjectFinder<SunController> sunp = new ObjectFinder<SunController>();
#endif
	private float bgmCounter;
    private float stepCounter;
    private float deathCounter;
    private float tradingCounter;
    private float castingCounter;
#if UMMORPG_SEASONS_SUN
    private float nightCounter;
    private float dayCounter;
#endif

	static int randIndex;

    void OnStartLocalPlayer_BackgroundMusic()
    {
        bgmNode = MusicManager.BgmNode();
        footStepsNode = new SoundNode(SoundManager.AudioSource(), footsteps, footstepsVol, footstepsDelay);
        deathNode = new SoundNode(SoundManager.AudioSource(), death, deathVol, footstepsDelay);
        tradingNode = new SoundNode(SoundManager.AudioSource(), trading, tradingVol, tradingDelay);
        castingNode = new SoundNode(SoundManager.AudioSource(), casting, castingVol, castingDelay);
        bgmCounter = float.MaxValue;
#if UMMORPG_SEASONS_SUN
        nightNode = MusicManager.NightNode();
        dayNode = MusicManager.DayNode();
        nightCounter = 1000.0f;
        dayCounter = 1000.0f;
#endif
        FindObjectOfType<Introduction>().GetComponent<Introduction>().enabled = false;
        MusicManager.inIntro = false;
        SoundManager.AudioSource().Stop();
    }

    void UpdateClient_SoundManager()
    {
        if (!isLocalPlayer) return;
        stepCounter += Time.deltaTime;
        deathCounter += Time.deltaTime;
        tradingCounter += Time.deltaTime;
        castingCounter += Time.deltaTime;
        bgmCounter += Time.deltaTime;
#if UMMORPG_SEASONS_SUN
        nightCounter += Time.deltaTime;
        dayCounter += Time.deltaTime;
#endif

        if (state == "MOVING" && SoundManager.ShouldPlay(footStepsNode, stepCounter))
        {
            SoundManager.playNode(footStepsNode, SoundManager.NextIndex());
            stepCounter = 0.0f;
        }


        if (state == "CASTING" && EventSkillFinished() && SoundManager.ShouldPlay(castingNode, castingCounter))
        {
            SoundManager.playNode(castingNode, SoundManager.NextIndex());
            castingCounter = 0.0f;
        }

        if (state == "TRADING" && EventTradeStarted() && SoundManager.ShouldPlay(tradingNode, tradingCounter))
        {
            SoundManager.playNode(tradingNode, SoundManager.NextIndex());
            tradingCounter = 0.0f;
        }

        if (state == "DEAD" && EventDied() && SoundManager.ShouldPlay(deathNode, deathCounter))
        {
            SoundManager.playNode(deathNode, SoundManager.NextIndex());
            deathCounter = 0.0f;

        }
        if (SoundManager.ShouldPlay(bgmNode, bgmCounter))
        {
            SoundManager.playNode(bgmNode, SoundManager.NextIndex());
            bgmCounter = 0.0f;
        }
#if UMMORPG_SEASONS_SUN
        if (sunp.val.isNight && SoundManager.ShouldPlay(nightNode, nightCounter)) {
            SoundManager.playNode(nightNode, SoundManager.NextIndex());
            nightCounter = 0.0f;
        }
        if (sunp.val.isDay && SoundManager.ShouldPlay(dayNode, dayCounter))
		{
			SoundManager.playNode(dayNode, SoundManager.NextIndex());
			dayCounter = 0.0f;
		}
#endif
    }

}
