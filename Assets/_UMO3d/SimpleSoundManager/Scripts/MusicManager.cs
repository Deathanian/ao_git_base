#define UMMORPG_SEASONS_SUN
using UnityEngine;


[RequireComponent(typeof(Introduction))]
public class MusicManager : SoundManager
{

    [System.NonSerialized]
    public static bool inIntro;

    [SerializeField]
    AudioClip[] intro;
    [SerializeField, Range(0, 1)]
    float introVol;
    [SerializeField]
    float introDelay;
    [SerializeField]
    AudioClip[] bgm;
    [SerializeField, Range(0, 1)]
    float bgmVol;
    [SerializeField]
    float bgmDelay;

#if UMMORPG_SEASONS_SUN
    [SerializeField]
    AudioClip[] Night;
    [SerializeField, Range(0, 1)]
    float nightVol;
    [SerializeField]
    float nightDelay;
    [SerializeField]
    AudioClip[] Day;
    [SerializeField, Range(0, 1)]
    float dayVol;
    [SerializeField]
    float dayDelay;
#endif

    private static SoundNode introNode;
    private static SoundNode bgmNode;
#if UMMORPG_SEASONS_SUN
    private static SoundNode nightNode;
    private static SoundNode dayNode;
#endif

    // Use this for initialization
    void OnEnable()
    {
        introNode = new SoundNode(AudioSource(), intro, introVol, introDelay);
        bgmNode = new SoundNode(AudioSource(), bgm, bgmVol, bgmDelay);
#if UMMORPG_SEASONS_SUN
        nightNode = new SoundNode(AudioSource(), Night, nightVol, nightDelay);
        dayNode = new SoundNode(AudioSource(), Day, dayVol, dayDelay);
#endif
    }

	public static SoundNode IntroNode()
	{
		return introNode;
	}

	public static SoundNode BgmNode()
	{
		return bgmNode;
	}

#if UMMORPG_SEASONS_SUN
    public static SoundNode NightNode() {
        return nightNode;
    }

    public static SoundNode DayNode() {
        return dayNode;
    }
#endif
}
