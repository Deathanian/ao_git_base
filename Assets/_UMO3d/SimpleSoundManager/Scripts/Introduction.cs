﻿﻿using UnityEngine;

[RequireComponent(typeof(MusicManager))]
public class Introduction : MonoBehaviour  {

    private SoundNode introNode;

	private float introCounter;

    void Start() {
        MusicManager.inIntro = true;
        introCounter = float.MaxValue;
        introNode =  MusicManager.IntroNode();
    }

    void Update() {
        introCounter += Time.deltaTime;
		if (SoundManager.ShouldPlay(introNode, introCounter))
		{
            SoundManager.playNode(introNode, SoundManager.NextIndex());
			introCounter = 0.0f;
		}
	}
}
