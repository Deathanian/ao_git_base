﻿using UnityEngine;
using UnityEngine.Networking;
public class SoundManager : NetworkBehaviour {

	private static AudioSource audioSource;

    private static int nextIndex;

    protected virtual void Awake()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera.gameObject.GetComponent<AudioListener>() == null)
        {
            mainCamera.gameObject.AddComponent(typeof(AudioListener));
        }
		if (mainCamera.gameObject.GetComponent<AudioSource>() == null)
		{
			mainCamera.gameObject.AddComponent(typeof(AudioSource));
		}
        audioSource = mainCamera.GetComponent<AudioSource>();
    }

    public static void playNode(SoundNode node, int index)
	{
		int audioClipLength = node.AudioClips().Length;
		if (audioClipLength == 1)
		{
			node.Play(0);
		} 
		else
		{
			node.Play(index);
			node.switchAudio(index, 0);
		}
	}

	public static bool ShouldPlay(SoundNode node, float counter)
	{


        int audioClipLength = node.AudioClips().Length;
        nextIndex = (audioClipLength != 1) ? Random.Range(1, audioClipLength) : 0;
        bool shouldPlay = (node.canPlay(0, counter)) ? true : false;
        return shouldPlay;
    

	}

	public static AudioSource AudioSource()
	{
		return audioSource;
	}

    public static int NextIndex() {
        return nextIndex;
    }

}
