﻿// =======================================================================================
// FREE ITEM STATS - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using System.Collections;
using System;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	public bool[] equipmentRefreshed = { false, false, false, false, false, false, false, false };

	// -----------------------------------------------------------------------------------
	// SwapInventoryEquip_UMO3d_FreeItemStats
	// -----------------------------------------------------------------------------------
	public void SwapInventoryEquip_UMO3d_FreeItemStats(int inventoryIndex, int equipIndex) {
		
		// ---------- Unequip Item
		if (equipment[equipIndex].valid && CanEquip(equipmentInfo[equipIndex].requiredCategory, equipment[equipIndex]) && equipmentRefreshed[equipIndex]) {
			UMO3d_RefreshEquipmentStats(equipIndex, false);
			equipmentRefreshed[equipIndex] = false;
		}
	}
	
	// -----------------------------------------------------------------------------------
	// RefreshLocation_UMO3d_FreeItemStats
	// -----------------------------------------------------------------------------------
	public void RefreshLocation_UMO3d_FreeItemStats(int equipIndex, Item item) {
	
		// ---------- Equip Item
		if (item.valid && !equipmentRefreshed[equipIndex]) {
			UMO3d_RefreshEquipmentStats(equipIndex, true);
			equipmentRefreshed[equipIndex] = true;
		}
		
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_RefreshEquipmentStats
	// -----------------------------------------------------------------------------------
	private void UMO3d_RefreshEquipmentStats(int equipIndex, bool additive) {
	
		var tmp_HealthRecoveryBonus		= equipment[equipIndex].equipHealthRecoveryBonus;
		var tmp_ManaRecoveryBonus		= equipment[equipIndex].equipManaRecoveryBonus;
		var tmp_SpeedBonus				= equipment[equipIndex].equipSpeedBonus;
			
#if _SimpleEquipmentSets
		UMO3d_RefreshEquipmentSets();
		tmp_HealthRecoveryBonus		+= equipment[equipIndex].setHealthRecoveryBonus + equipment[equipIndex].setPartialHealthRecoveryBonus + equipment[equipIndex].setCompleteHealthRecoveryBonus;
		tmp_ManaRecoveryBonus		+= equipment[equipIndex].setManaRecoveryBonus + equipment[equipIndex].setPartialManaRecoveryBonus + equipment[equipIndex].setCompleteManaRecoveryBonus;
		tmp_SpeedBonus				+= equipment[equipIndex].setSpeedBonus + equipment[equipIndex].setPartialSpeedBonus + equipment[equipIndex].setCompleteSpeedBonus;
#endif

		if (additive) {
			baseHealthRecoveryRate 	+= tmp_HealthRecoveryBonus;
			baseManaRecoveryRate 	+= tmp_ManaRecoveryBonus;
			agent.speed 			+= tmp_SpeedBonus;
		} else {
			baseHealthRecoveryRate 	-= tmp_HealthRecoveryBonus;
			baseManaRecoveryRate 	-= tmp_ManaRecoveryBonus;
			agent.speed 			-= tmp_SpeedBonus;
		}
		
	}
	
}

// =======================================================================================