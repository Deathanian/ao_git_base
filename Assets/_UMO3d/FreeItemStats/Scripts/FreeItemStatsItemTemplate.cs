﻿// =======================================================================================
// FREE ITEM STATS - ITEM TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// ITEM TEMPLATE
// =======================================================================================
public partial class ItemTemplate : ScriptableObject {
   
	[Header("UMO3d FreeItemStats - Equipment")]
	public int equipHealthRecoveryBonus;
    public int equipManaRecoveryBonus;
    public float equipSpeedBonus;
	
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_STR)]
	public int equipStrengthBonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_INT)]
	public int equipIntelligenceBonus;
	
#if _SimpleAttributes

	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR1)]
	public int equipAttribute1Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR2)]
	public int equipAttribute2Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR3)]
	public int equipAttribute3Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR4)]
	public int equipAttribute4Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR5)]
	public int equipAttribute5Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR6)]
	public int equipAttribute6Bonus;
	
	//--7
	//--8
	
#if _SimpleStamina
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR9)]
	public int equipAttribute9Bonus;
#endif
	
#endif

#if _SimpleBoosters

	[Tooltip(Constants.UMO3d_MSG_BOOST_GOLD)]
	[Range(0, 1)] public float equipGoldBonus;
	[Tooltip(Constants.UMO3d_MSG_BOOST_EXP)]
	[Range(0, 1)] public float equipExpBonus;
	[Tooltip(Constants.UMO3d_MSG_BOOST_COINS)]
	[Range(0, 1)] public float equipCoinsBonus;


#endif
	  
}

// =======================================================================================