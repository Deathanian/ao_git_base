﻿// =======================================================================================
// FREE ITEM STATS - ITEM
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using System.Text;

// =======================================================================================
// ITEM
// =======================================================================================
public partial struct Item {
	
	// -----------------------------------------------------------------------------------
	// Getters
	// -----------------------------------------------------------------------------------

	// ---------------------------------------------------------------------------------- Basic Item Stats
	
	// -- 
   	public int equipHealthRecoveryBonus {
        get { return template.equipHealthRecoveryBonus; }
    }
    
    // -- 
    public int equipManaRecoveryBonus {
        get { return template.equipManaRecoveryBonus; }
    }
    
    // -- 
    public float equipSpeedBonus {
        get { return template.equipSpeedBonus; }
    }
    
    // -- 
	public int equipStrengthBonus {
		get { return template.equipStrengthBonus; }
	}
	
	// -- 
	public int equipIntelligenceBonus {
		get { return template.equipIntelligenceBonus; }
	}
	
	// ---------------------------------------------------------------------------------- SimpleAttributes Item Stats
#if _SimpleAttributes

	public int equipAttribute1Bonus {
		get { return template.equipAttribute1Bonus; }
	}
	
	public int equipAttribute2Bonus {
		get { return template.equipAttribute2Bonus; }
	}
	
	public int equipAttribute3Bonus {
		get { return template.equipAttribute3Bonus; }
	}
	
	public int equipAttribute4Bonus {
		get { return template.equipAttribute4Bonus; }
	}
	
	public int equipAttribute5Bonus {
		get { return template.equipAttribute5Bonus; }
	}

	public int equipAttribute6Bonus {
		get { return template.equipAttribute6Bonus; }
	}

	//--7
	//--8
	
#if _SimpleStamina
	public int equipAttribute9Bonus {
		get { return template.equipAttribute9Bonus; }
	}
#endif
	
#endif

// ---------------------------------------------------------------------------------- SimpleBoosters Item Stats
#if _SimpleBoosters

	public float equipGoldBonus {
		get { return template.equipGoldBonus; }
	}

	public float equipExpBonus {
		get { return template.equipExpBonus; }
	}

	public float equipCoinsBonus {
		get { return template.equipCoinsBonus; }
	}

#endif

	// -----------------------------------------------------------------------------------
	// ToolTip_UMO3d_FreeItemStatsItem
	// -----------------------------------------------------------------------------------
	public void ToolTip_UMO3d_FreeItemStatsItem(StringBuilder tip) {
       	
        if (category.StartsWith("Equipment")) {
        	tip.Append("\n");
        	
        	if (equipHealthRecoveryBonus != 0) 	tip.Append( Constants.UMO3d_STAT_HEALTH_REC 	+ Constants.UMO3d_TOOLTIP_BONUS + equipHealthRecoveryBonus.ToString() 	+ "\n");
        	if (equipManaRecoveryBonus != 0) 	tip.Append( Constants.UMO3d_STAT_MANA_REC  		+ Constants.UMO3d_TOOLTIP_BONUS + equipManaRecoveryBonus.ToString() 	+ "\n");
        	if (equipSpeedBonus != 0) 			tip.Append( Constants.UMO3d_STAT_SPEED 			+ Constants.UMO3d_TOOLTIP_BONUS + equipSpeedBonus.ToString() 			+ "\n");
        	if (equipStrengthBonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_STR 			+ Constants.UMO3d_TOOLTIP_BONUS + equipStrengthBonus.ToString() 		+ "\n");
        	if (equipIntelligenceBonus != 0) 	tip.Append( Constants.UMO3d_ATTRIB_INT 			+ Constants.UMO3d_TOOLTIP_BONUS + equipIntelligenceBonus.ToString() 	+ "\n");
#if _SimpleAttributes
        	if (equipAttribute1Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR1 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute1Bonus.ToString()		+ "\n");
        	if (equipAttribute2Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR2 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute2Bonus.ToString()	 	+ "\n");
        	if (equipAttribute3Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR3 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute3Bonus.ToString() 		+ "\n");
        	if (equipAttribute4Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR4 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute4Bonus.ToString()	 	+ "\n");
        	if (equipAttribute5Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR5 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute5Bonus.ToString()	 	+ "\n");
        	if (equipAttribute6Bonus != 0) 		tip.Append( Constants.UMO3d_ATTRIB_ATR6 		+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute6Bonus.ToString()	 	+ "\n");
			//--7
			//--8
#if _SimpleStamina
			if (equipAttribute9Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR9 			+ Constants.UMO3d_TOOLTIP_BONUS + equipAttribute9Bonus.ToString() 		+ "\n");
#endif
#endif
#if _SimpleBoosters
			if (equipGoldBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_GOLD 	+ Mathf.RoundToInt(equipGoldBonus * 100).ToString() 					+ "%\n");
			if (equipExpBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_EXP 	+ Mathf.RoundToInt(equipExpBonus * 100).ToString() 						+ "%\n");
			if (equipCoinsBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_COINS 	+ Mathf.RoundToInt(equipCoinsBonus * 100).ToString() 					+ "%\n");
#endif

        }
    }  
    
}

// =======================================================================================