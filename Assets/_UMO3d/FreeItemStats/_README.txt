﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires the NuCore: http://www.unity-mmo.com/butler

First, Install the most recent version of NuCore.

Next, locate your player prefabs in the inspector:

1. Search for the equipmentRefreshed entry

2. Set it to the same amount of indexes as you have equipment locations in your game
(detault is 8 to cover eight equipment slots numbered 0 to 7);

Finally, edit your items to your liking - you will see new options in the inspector.

Thats it, no further modifications required!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires NuCore!

This little AddOn will add three new stats to your equipable items!

* Equip Health Recovery Rate (can also be negative!)
* Equip Mana Recovery Rate (can also be negative!)
* Equip Speed Bonus (can also be negative!)
* Equip Strength Bonus (can also be negative)
* Equip Intelligence Bonus (can also be negative)

======================================================================