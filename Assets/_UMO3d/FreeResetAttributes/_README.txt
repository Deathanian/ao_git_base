﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.indie-mmo.com
* NuCore Download (required for most AddOns): http://www.indie-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

This AddOn requires the NuCore!

First, Install the most recent version of NuCore.

Next, import this AddOn.

Finally, move the items in this AddOns "Prefabs" directory into your Resources -> Items
directory. Assign those items to a Monster Drop or NPC Shop so you can test them in your
game.

Thats it, no further modifications required!

Usage Information:

To setup a Reset Item, simply edit the item. There are two different types available for
Attributes Reset and for Skill Reset.

Using a Reset Attributes item, will reset ALL attribute values to 0 and grant the player
that many attribute points.

Using a Reset Skill item, will reset ALL skill levels to 0 and grant the player that
many skill experience.

Note: The first skill is the basic attack, it is never reset.

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Allow your players to reset their Attributes/Skills with special items!

- Reset all attribute points back to 0 and gain attribute points to spend.
- Reset all skill levels back to 0 and gain skill experience to spend.

======================================================================