﻿// =======================================================================================
// FREE RESET ATTRIBUTES - ITEM TEMPLATE
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class ItemTemplate {
    
    public bool resetAttributes;
    public bool resetSkills;
    
}

// =======================================================================================