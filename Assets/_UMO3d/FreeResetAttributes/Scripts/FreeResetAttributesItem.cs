﻿// =======================================================================================
// FREE RESET ATTRIBUTES - ITEM
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public partial struct Item {
    
    public bool resetAttributes {
        get { return template.resetAttributes; }
    }
    
    public bool resetSkills {
        get { return template.resetSkills; }
    }
    
}

// =======================================================================================