﻿// =======================================================================================
// FREE RESET ATTRIBUTES - PLAYER
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	protected const string UMO3d_MSG_RESET_ATTRIB 		= "Your attribute points have been reset!";	
	protected const string UMO3d_MSG_RESET_SKILL 		= "Your skill points have been reset!";	

	// -----------------------------------------------------------------------------------
	// OnUseInventoryItem_CHFreeResetAttributes
	// -----------------------------------------------------------------------------------
	public void OnUseInventoryItem_CHFreeResetAttributes(int index) {
		var item = inventory[index];
		if (item.valid && (item.resetAttributes || item.resetSkills) ) {
		
            // ---------- attribute point reset
            if (item.resetAttributes) {
				var myAttributePoints = 0;
			
				myAttributePoints += strength;
				myAttributePoints += intelligence;
			
#if _SimpleAttributes
				myAttributePoints += attribute1;
				myAttributePoints += attribute2;
				myAttributePoints += attribute3;
				myAttributePoints += attribute4;
				myAttributePoints += attribute5;
				myAttributePoints += attribute6;
#endif          
				if (myAttributePoints > 0) {
					attributePoints += myAttributePoints;
					strength 		= 0;
					intelligence 	= 0;
#if _SimpleAttributes
					attribute1 		= 0;
					attribute2 		= 0;
					attribute3 		= 0;
					attribute4 		= 0;
					attribute5 		= 0;
					attribute6 		= 0;
#endif
	 
#if _FreeInfoBox
					GetComponent<InfoBox>().TargetAddMessage(connectionToClient, UMO3d_MSG_RESET_ATTRIB);
#else
					GetComponent<Chat>().TargetMsgInfo(connectionToClient, UMO3d_MSG_RESET_ATTRIB);
#endif   
				}
            }
            
            // ---------- skill point reset
            if (item.resetSkills) {
				long mySkillPoints = 0;
			
				for (int skillIndex = 1; skillIndex < skills.Count; skillIndex++) {
					var skill = skills[skillIndex];
					if (skill.learned && !skill.category.StartsWith("Status") && skill.level > 0) {
						for (int j = skill.level; j > 0; j--) {
							mySkillPoints += skill.requiredSkillExperience;
						}
						skill.level = 1;
						skill.learned = false;
						skills[skillIndex] = skill;
					}
				}
			
				if (mySkillPoints > 0) {
					skillExperience += mySkillPoints;
#if _FreeInfoBox
					GetComponent<InfoBox>().TargetAddMessage(connectionToClient, UMO3d_MSG_RESET_SKILL);
#else
					GetComponent<Chat>().TargetMsgInfo(connectionToClient, UMO3d_MSG_RESET_SKILL);
#endif   
				}
            }
            
            // decrease amount or destroy
            if (item.usageDestroy) {
            	--item.amount;
            	if (item.amount == 0) item.valid = false;
            	inventory[index] = item; // put new values in there
            }
			
		}
	}
	
}

// =======================================================================================