﻿// =======================================================================================
// VERSION
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Version {
		
		public const int UMO3d_VERSION_FREE_RESETATTRIBUTES = 104;

	}

}

// ===================================================================================