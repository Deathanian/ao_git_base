﻿// =======================================================================================
// DESTROY ON SERVER - CLASS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

namespace UMO3d {

	public class FreeDestroyOnServer : MonoBehaviour {
	
	    [SerializeField] Entity source;
	    
		// -------------------------------------------------------------------------------
		// Start
	    // -------------------------------------------------------------------------------
    	void Start() {
            if (!source)
				source = GetComponentInParent<Entity>();
	    	if (source && source.GetComponent<Entity>()) {
				if (source.GetComponent<Entity>().isServer && !source.GetComponent<Entity>().isClient) {
					int childs = transform.childCount;
					for (int i = childs - 1; i >= 0; i--) {
						GameObject.Destroy(transform.GetChild(i).gameObject);
					}
					Destroy(this);
				}
	    	}
	    }

	    // -------------------------------------------------------------------------------

	}

}

// =======================================================================================