======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Select any Entity in your game, expand it and attach this script to any OverlayPosition
that you see. 

This compromises:

* For Monsters:
	- NameOverlayPosition

* For NPCs:
	- NameOverlayPosition
	- QuestIndicatorPosition

* For Players:
	- NameOverlayPosition
	- GuildOverlayPosition

The best practice is to add this script to all of your Monster, NPC and Player prefabs as
this will update all your objects in the scene as well.

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

This script will increase your game servers performance. As several objects (especially
Name Overlays) are not required server-side, it is a good idea to destroy them.

FreeDestroyOnServer will destroy the NameOverlay, QuestIndicator and GuildOverlay
when the object is instanciated. It will only do so on a Server, not on a Client! As these
kinds of objects use up memory and processing power - especially when present in massive
amounts - it is better to destroy them.

Even when you run your server in headless (no-graphics) mode, those objects will be
created and just "sit around" soaking up RAM and CPU.

Note: In the newest uMMORPG version, those objects are de-activated server side, saving
memory and CPU. But this script can still be useful at times!

======================================================================