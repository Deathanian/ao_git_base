﻿// =======================================================================================
// SIMPLE ATTRIBUTES - PLAYER
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

#if _SimpleAttributes

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	[Header("SimpleAttributes")]
	
	// -----------------------------------------------------------------------------------
    // Attribute1
    // -----------------------------------------------------------------------------------
    public int _attribute1 = 0;
    public int attribute1 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute1Bonus).Sum();
#endif

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute1Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus1 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute1).Sum();
            buffBonus = buffBonus + buffBonus1;
#endif

            return UMO3d_AttributeGet("attribute1") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute1 = value;
    		UMO3d_AttributeSet("attribute1", _attribute1);
    	}
    }	
	
	// -----------------------------------------------------------------------------------
    // Attribute2
    // -----------------------------------------------------------------------------------
    public int _attribute2 = 0;
    public int attribute2 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute2Bonus).Sum();
#endif

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute2Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus2 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute2).Sum();
            buffBonus = buffBonus + buffBonus2;
#endif

			return UMO3d_AttributeGet("attribute2") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute2 = value;
    		UMO3d_AttributeSet("attribute2", _attribute2);
    	}
    }		
	
	// -----------------------------------------------------------------------------------
    // Attribute3
    // -----------------------------------------------------------------------------------
    public int _attribute3 = 0;
    public int attribute3 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute3Bonus).Sum();
#endif		

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute3Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus3 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute3).Sum();
            buffBonus = buffBonus + buffBonus3;
#endif
			// return base + equip + buffs
			return UMO3d_AttributeGet("attribute3") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute3 = value;
    		UMO3d_AttributeSet("attribute3", _attribute3);
    	}
    }
    
	// -----------------------------------------------------------------------------------
    // Attribute4
    // -----------------------------------------------------------------------------------
    public int _attribute4 = 0;
    public int attribute4 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute4Bonus).Sum();
#endif		

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute4Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus4 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute4).Sum();
            buffBonus = buffBonus + buffBonus4;
#endif
			// return base + equip + buffs
			return UMO3d_AttributeGet("attribute4") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute4 = value;
    		UMO3d_AttributeSet("attribute4", _attribute4);
    	}
    }		

	// -----------------------------------------------------------------------------------
    // Attribute5
    // -----------------------------------------------------------------------------------
    public int _attribute5 = 0;
    public int attribute5 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute5Bonus).Sum();
#endif		

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute5Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus5 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute5).Sum();
            buffBonus = buffBonus + buffBonus5;
#endif
			// return base + equip + buffs
			return UMO3d_AttributeGet("attribute5") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute5 = value;
    		UMO3d_AttributeSet("attribute5", _attribute5);
    	}
    }		

	// -----------------------------------------------------------------------------------
    // Attribute6
    // -----------------------------------------------------------------------------------
    public int _attribute6 = 0;
    public int attribute6 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute6Bonus).Sum();
#endif		

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute6Bonus).Sum();
#endif

            int buffBonus = 0;
#if _SimpleSpellStats
            int buffBonus6 = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffsAttribute6).Sum();
            buffBonus = buffBonus + buffBonus6;
#endif
			// return base + equip + buffs
			return UMO3d_AttributeGet("attribute6") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute6 = value;
    		UMO3d_AttributeSet("attribute6", _attribute6);
    	}
    }
	
	// -----------------------------------------------------------------------------------
    // Attribute7
    // -----------------------------------------------------------------------------------
    [HideInInspector]public int _attribute7 = 0;
    [HideInInspector]public int attribute7 = 0;
    
    // -----------------------------------------------------------------------------------
    // Attribute8
    // -----------------------------------------------------------------------------------
    [HideInInspector]public int _attribute8 = 0;
    [HideInInspector]public int attribute8 = 0;

	// -----------------------------------------------------------------------------------
	// Attribute9
	// -----------------------------------------------------------------------------------
#if _SimpleStamina
	public int _attribute9 = 0;
    public int attribute9 {
    	get {

            int equipmentBonus = 0;
#if _FreeItemStats
            equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipAttribute9Bonus).Sum();
#endif

			int setBonus = 0;
#if _SimpleEquipmentSets
			setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute9Bonus).Sum();
#endif

            int buffBonus = 0;

            // return base + equip + buffs
            return UMO3d_AttributeGet("attribute9") + equipmentBonus + buffBonus + setBonus;
    	}
    	
    	set { 
    		_attribute9 = value;
    		UMO3d_AttributeSet("attribute9", _attribute9);
    	}
    }
#endif

	// =======================================================================================
	// =======================================================================================

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute1() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute1;																	// NuCore
    	}
    }

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute2() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute2;																	// NuCore
    	}
    }

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute3() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute3;																	// NuCore
    	}
    }

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute4() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute4;																	// NuCore
    	}
    }

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute5() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute5;																	// NuCore
    	}
    }

	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute6() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute6;																	// NuCore
    	}
    }

	//--7
	//--8

#if _SimpleStamina
	[Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void CmdIncreaseAttribute9() {
        // validate
        if (health > 0 && AttributesSpendable() > 0) {										// NuCore
        	attributePoints--;
        	++ attribute9;																	// NuCore
    	}
    }
#endif

    // =======================================================================================
}

#endif

// =======================================================================================