// =======================================================================================
// DEFINES
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class Define_UMO3d_SimpleAttributes
{
	static string definestring;

    const string define = "_SimpleAttributes";
    
	const string nuCore = "_NuCore";
	const bool requiredNuCore = true;
	const int versionNuCore = 113;
	const string nuTools = "_NuTools";
	const bool requiredNuTools = false;
	const int versionNuTools = 100;
	
	static Define_UMO3d_SimpleAttributes() {AddLibrayDefineIfNeeded();}

    static void AddLibrayDefineIfNeeded() {
		BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
		definestring = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

		#pragma warning disable
		if (requiredNuCore && !inDefines(nuCore))
			Debug.LogWarning("WARNING! This AddOn requires <b>NuCore v"+versionNuCore+"</b> - free download: http://www.indie-mmo.com/butler - If installed already, please restart Unity.");

		if (requiredNuTools && !inDefines(nuTools))
			Debug.LogWarning("WARNING! This AddOn requires <b>NuTools v"+versionNuTools+"</b> - free download: http://www.indie-mmo.com/product/nutools/ - If installed already, please restart Unity.");
		#pragma warning restore
		
		if (inDefines (define))
			return;

		PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, (definestring + ";" + define));
		Debug.LogWarning("<b>" + define + "</b> added to <i>Scripting Define Symbols</i> for selected build target (" + EditorUserBuildSettings.activeBuildTarget.ToString() + ").");
    }

	static bool inDefines(string define) {
		string[] defines = definestring.Split (';');
		foreach (string def in defines) {
			if (def == define)
				return true;
		}
		return false;
	}
}

#endif

// =======================================================================================
// VERSION
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {
	public partial class Version {
		public const int UMO3d_SimpleAttributes_Int = 106;
		public const string UMO3d_SimpleAttributes_Str = "v1.06";
	}
}

// =======================================================================================