﻿// =======================================================================================
// SIMPLE ATTRIBUTES - UI CHARACTER INFO
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.UI;

#if _SimpleAttributes

// =======================================================================================
// UI CHARACTER INFO
// =======================================================================================
public partial class UICharacterInfo : MonoBehaviour {
    
    public Text attribute1Name;
    public Text attribute2Name;
    public Text attribute3Name;
    public Text attribute4Name;
    public Text attribute5Name;
    public Text attribute6Name;
#if _SimpleStamina
	public Text attribute9Name;
#endif

    public Text attribute1Text;
    public Text attribute2Text;
    public Text attribute3Text;
    public Text attribute4Text;
    public Text attribute5Text;
    public Text attribute6Text;
#if _SimpleStamina
	public Text attribute9Text;
#endif

    public Button attribute1Button;
    public Button attribute2Button;
    public Button attribute3Button;
    public Button attribute4Button;
    public Button attribute5Button;
    public Button attribute6Button;
#if _SimpleStamina
	 public Button attribute9Button;
#endif

	// -----------------------------------------------------------------------------------
	// Update_UMO3d_UICharacterInfo
	// -----------------------------------------------------------------------------------
    void Update_UMO3d_UICharacterInfo() {
        var player = Utils.ClientLocalPlayer();
        if (!player) return;
        
        // only refresh the panel while it's active
        if (panel.activeSelf) {
#pragma warning disable
            if (Constants.UMO3d_ATRIB_ACTIVE1) {
				attribute1Name.text = Constants.UMO3d_ATTRIB_ATR1;
				attribute1Text.text = player.attribute1.ToString();
				attribute1Button.interactable = player.AttributesSpendable() > 0;
				attribute1Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute1();
				});
            } else {
            	attribute1Name.enabled = false;
            	attribute1Text.enabled = false;
            	attribute1Button.gameObject.SetActive(false);
            }
			if (Constants.UMO3d_ATRIB_ACTIVE2) {
				attribute2Name.text = Constants.UMO3d_ATTRIB_ATR2;
				attribute2Text.text = player.attribute2.ToString();
				attribute2Button.interactable = player.AttributesSpendable() > 0;
				attribute2Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute2();
				});
			} else {
            	attribute2Name.enabled = false;
            	attribute2Text.enabled = false;
            	attribute2Button.gameObject.SetActive(false);
            }
			if (Constants.UMO3d_ATRIB_ACTIVE3) {
				attribute3Name.text = Constants.UMO3d_ATTRIB_ATR3;
				attribute3Text.text = player.attribute3.ToString();
				attribute3Button.interactable = player.AttributesSpendable() > 0;
				attribute3Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute3();
				});
			} else {
            	attribute3Name.enabled = false;
            	attribute3Text.enabled = false;
            	attribute3Button.gameObject.SetActive(false);
            }
			if (Constants.UMO3d_ATRIB_ACTIVE4) {
				attribute4Name.text = Constants.UMO3d_ATTRIB_ATR4;
				attribute4Text.text = player.attribute4.ToString();
				attribute4Button.interactable = player.AttributesSpendable() > 0;
				attribute4Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute4();
				});
			} else {
            	attribute4Name.enabled = false;
            	attribute4Text.enabled = false;
            	attribute4Button.gameObject.SetActive(false);
            }
			if (Constants.UMO3d_ATRIB_ACTIVE5) {
				attribute5Name.text = Constants.UMO3d_ATTRIB_ATR5;
				attribute5Text.text = player.attribute5.ToString();
				attribute5Button.interactable = player.AttributesSpendable() > 0;
				attribute5Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute5();
				});
			} else {
            	attribute5Name.enabled = false;
            	attribute5Text.enabled = false;
            	attribute5Button.gameObject.SetActive(false);
            }
			if (Constants.UMO3d_ATRIB_ACTIVE6) {
				attribute6Name.text = Constants.UMO3d_ATTRIB_ATR6;
				attribute6Text.text = player.attribute6.ToString();
				attribute6Button.interactable = player.AttributesSpendable() > 0;
				attribute6Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute6();
				});
			} else {
            	attribute6Name.enabled = false;
            	attribute6Text.enabled = false;
            	attribute6Button.gameObject.SetActive(false);
            }
            
#if _SimpleStamina
			if (Constants.UMO3d_ATRIB_ACTIVE9) {
				attribute9Name.text = Constants.UMO3d_ATTRIB_ATR9;
				attribute9Text.text = player.attribute9.ToString();
				attribute9Button.interactable = player.AttributesSpendable() > 0;
				attribute9Button.onClick.SetListener(() => {
					player.CmdIncreaseAttribute9();
				});
			} else {
            	attribute9Name.enabled = false;
            	attribute9Text.enabled = false;
            	attribute9Button.gameObject.SetActive(false);
            }
#endif
            
#pragma warning restore
        }       
    }
}

#endif

// =======================================================================================
