﻿======================================================================
INDIE-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Indie-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.indie-mmo.com
* NuCore Download (required for most AddOns): http://www.indie-mmo.com/butler/index.php
* Support via eMail: support@indie-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires the NuCore!

First, Install the most recent version of NuCore.

2. Next, install this AddOn.

3. Edit the SimpleAttributesConstants file in this AddOns prefab directory.

This allows you to change the names and the effect of each attribute.

* Set the effect of an attribute to 0 to disable that effect.

4. Deactivate the old CharacterInfo UI panel
You dont have to delete it, just deactivating it is enough.

5. Drag-n-Drop the new CharacterInfo UI panel into your canvas.

6. Select the "Shortcuts" panel in your canvas and assign the new CharacterInfo UI panel
to the Character Info Panel slot.

Thats it, no further modifications required!

Note: In the inspector window, each attribute is simply labelled "Attribute1" etc.
It is not possible to change the displayed name because we cannot have "variable variables"
in the inspector. Just hover over the "Attribute1" name to see the real name of that attribute.
You can ajdust these names to your liking in the SimpleAttributesConstants file.

Attention: If you get an error in combination with the FreeItemStats AddOn, simply
re-install the FreeItemStats AddOn after you installed the SimpleAttributes and all
errors should be gone.

3. DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn adds 6 new upgradeable Attributes with effects to your game!

* Adds 6 new Attributes to your game (in addition to Strength/Intelligence)
* Spend attribute points on level up like for Strength/Intelligence
* Each attribute has a specific side-effect
* Adjust the side-effect power by editing a simple script
	- Bonus to HealthMax
	- Bonus to ManaMax
	- Bonus to Damage
	- Bonus to Defense
	- Bonus to BlockChance
	- Bonus to CritChange
	- Bonus to Block Effectivity
	- Bonus to Crit Effectivity

* This AddOn adds a synergy effect to various other AddOns like:
	- SimpleUsageLimits
	- FreeItemStats
	- SimpleDamageFloor

Please note that the number of Attributes is preset and each attribute is tied to one
single effect. All these settings are fixed and not changeable!

======================================================================