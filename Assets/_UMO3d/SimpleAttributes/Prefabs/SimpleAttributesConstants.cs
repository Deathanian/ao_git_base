﻿// =======================================================================================
// SIMPLE ATTRIBUTES - CONSTANTS
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Constants {
		
		// -----------------------------------------------------------------------------------
		// Simple Attributes
		//
		// UMO3d_ATTRIB_ATR...	= The name of the attribute as displayed in the GUI and Tooltips
		//
		// UMO3d_ATTRIB_ACTIVE_ATR... = Is the attribute available in your game?
		///
		// SCALE = true/false
		//			Set to true, the bonus value relies on the base value and scales with it
		//			Set to false, the bonus value will simply be added to the base value
		//
		// BONUS = float value
		//			Scale True: 	0.01f = each attribute point adds 1% of base value
		//			Scale False:	0.01f * attributepoints is added to the base value
		// -----------------------------------------------------------------------------------	
		
		// ---------- Attribute 1 (Increases Damage)
		public const string UMO3d_ATTRIB_ATR1 			= "Dam Increase";
		public const bool	UMO3d_ATRIB_ACTIVE1			= true;
		public const bool	UMO3d_SCALE_DAMAGE			= true;
		public const float 	UMO3d_BONUS_DAMAGE			= 1.00f;
		
		// ---------- Attribute 2 (Increases Defense)
		public const string UMO3d_ATTRIB_ATR2 			= "Def Increase";
		public const bool	UMO3d_ATRIB_ACTIVE2			= true;
		public const bool	UMO3d_SCALE_DEFENSE			= false;
		public const float 	UMO3d_BONUS_DEFENSE			= 1.00f;
		
		// ---------- Attribute 3 (Increases Block Chance)
		public const string UMO3d_ATTRIB_ATR3		 	= "Block Chance";
		public const bool	UMO3d_ATRIB_ACTIVE3			= true;
		public const bool	UMO3d_SCALE_BLOCKCHANCE		= true;
		public const float 	UMO3d_BONUS_BLOCKCHANCE		= 0.01f;
		
		// ---------- Attribute 4 (Increases Crit Chance)
		public const string UMO3d_ATTRIB_ATR4 			= "Crit Chance";
		public const bool	UMO3d_ATRIB_ACTIVE4			= true;
		public const bool	UMO3d_SCALE_CRITCHANCE		= true;
		public const float 	UMO3d_BONUS_CRITCHANCE		= 0.01f;
		
		// ---------- Attribute 5 (Increases Block Effect Strength)
		public const string UMO3d_ATTRIB_ATR5 			= "Block Effect";
		public const bool	UMO3d_ATRIB_ACTIVE5			= true;
		public const bool	UMO3d_SCALE_BLOCKEFFECT		= true;
		public const float 	UMO3d_BONUS_BLOCKEFFECT		= 0.01f;
		
		// ---------- Attribute 6 (Increases Crit Effect Strength)
		public const string UMO3d_ATTRIB_ATR6 			= "Crit Effect";
		public const bool	UMO3d_ATRIB_ACTIVE6			= true;
		public const bool	UMO3d_SCALE_CRITEFFECT		= true;
		public const float 	UMO3d_BONUS_CRITEFFECT		= 0.01f;
		
		// ---------- Attribute 7 (Increases Speed)
		
		// ---------- Attribute 8 (Increases Cast Range)


#if _SimpleStamina
		// ---------- Attribute 9 (Increases Stamina)
		public const string UMO3d_ATTRIB_ATR9 			= "Stamina Effect";
		public const bool	UMO3d_ATRIB_ACTIVE9			= true;
		public const bool	UMO3d_SCALE_STAMINA			= true;
		public const float 	UMO3d_BONUS_STAMINA			= 1.00f;
#endif

	}

}

// ===================================================================================