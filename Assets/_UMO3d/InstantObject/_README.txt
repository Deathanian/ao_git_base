﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Locate the Prefabs folder within this AddOns folder. 

1. Move the "Instant Sphere" item from the prefabs folder into your projects Items
folder in: Resources -> Items. It must be in that folder in order to be functional.

2. Locate the "NetworkManager" in your scene, select it and open the "Spawn Info" section.
Add a new spawnable object to the list, search for "Sphere Object". That sphere will be
our test object. You can replace it later with your own objects.

3. Locate a Skeleton prefab (or any common enemy in your game) OR a NPC and either add
the "Instant Sphere" item as loot drop or as shop item.

In order to test the system, play your game and acquire a "Instant Sphere" item. Now
click that item in order to place it in the scene. Items are always placed in front of the
player, in facing direction.

Note: The "Sphere Object" has a timer attached to it, that will destroy each Sphere after
60 seconds. Remove that script if you want the object last as long as the server is up.

Creating items:

1. Create a new item in your Resources -> Items folder.

Leave the "Category" empty, or choose a custom one (not "Potion") otherwise item usage
might not trigger correctly.

You can set the item to "Usage Destroy" or add a mana cost to it by adjusting "Usage Mana".

2. Check the "Instantiate Object" checkbox.

3. Select a Model Prefab for your object.

Thats it, no script modifications!

3. DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This little script allows your players to place objects in the scene by using an item.

* Useful to place obstacles, decorations, furniture, plants and even traps.
* Objects are temporary and are removed when the server restarts.
* Objects can be any type of GameObject.
* The placed object will be spawned in facing direction, in front of the player.

The true power of this system is unleashed if you combine it with proper GameObjects. For
example you can combine it with my "SimpleDamageFloor" script in order to create player
spawnable traps or totems!

======================================================================