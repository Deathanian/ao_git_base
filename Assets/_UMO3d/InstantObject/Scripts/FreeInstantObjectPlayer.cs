﻿// =======================================================================================
// FREE INSTANT OBJECT - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	// -----------------------------------------------------------------------------------
	// OnUseInventoryItem_CHFreeInstantObject
	// -----------------------------------------------------------------------------------
	public void OnUseInventoryItem_CHFreeInstantObject(int index) {
		var item = inventory[index];
		if (item.valid && item.InstantiateObject && ItemTemplate.dict[item.name].modelPrefab != null && mana >= Mathf.Abs(item.usageMana) ) {

			mana += item.usageMana;
                
        	// decrease amount or destroy
            if (item.usageDestroy) {
            	--item.amount;
            	if (item.amount == 0) item.valid = false;
            	inventory[index] = item; // put new values in there
            }
		
			var SimpleObject = (GameObject)Instantiate(ItemTemplate.dict[item.name].modelPrefab, transform.position + transform.forward, Quaternion.identity);
			NetworkServer.Spawn(SimpleObject);
		}
	}
	
}

// =======================================================================================