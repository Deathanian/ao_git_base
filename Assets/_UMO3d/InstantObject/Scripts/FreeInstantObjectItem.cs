
using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public partial struct Item {
    
    public bool InstantiateObject {
        get { return template.InstantiateObject; }
    }

}