﻿// =======================================================================================
// VERSION
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace CriticalHit {

	public static partial class Version {
		
		public const int CH_VERSION_FREE_INSTANTOBJECT = 104;

	}

}

// ===================================================================================