﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires the NuCore: http://www.unity-mmo.com/butler

First, Install the most recent version of NuCore.

Next, import this AddOn.

Thats it, no further modifications required!

In order to change the elemental resistances and some internal settings, you have to edit
the file "SimpleResistancesConstants.cs" in the "Prefabs" folder. There is no Inspector
for this data, as it rarely changes.

To edit the resistances themselves, just edit your Monsters, Player, Items, Skills and
so on.

If you get a series of warnings in the log, simply ignore them - thats a C# issue and
nothing of importance.

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This script allows you to add elements alongside elemental resistance and weakness
to your game.

* 10 pre-defined elements available (plus "Neutral")
* Names changeable in the GUI of your game
* Define each element in values from -100% to +100% (where +100% is completely immune).
* Set resistance (+) or weakness (-) for:
	- Players
	- Monsters
	- Items (like Armor when equipped)
	- Skills (like Buffs when applied)
	- all classes based on Entity (like Pets/Mounts from other AddOns)

You can set elemental resistance and weakness for up to 10 predefined elements in the
inspector window of Players, Monsters, Items and Skills. Its also possible to set these
elements for Entities and all classes that are based on entities as well (like Pets or
Mounts from other AddOns). This AddOn is fixed to 10 elements, but the names can be
defined as you like in the GUI (so its possible to change "Fire" to "Laser").

Resistance will lower damage from the specific element, while Weakness will increase
damage dealt by that element.

If you set a skill element, it overrides the element of the equipped weapon, which
overrides the element of the Entity itself. Therefore, an Entities inherent element is
only used when neither a skill nor a weapon is present.

Armor and Buff Skills on the other hand, modfy the elemental resistance by -100% up to +100%.
All armor parts and active buffs are cummulative up to 100%.

======================================================================