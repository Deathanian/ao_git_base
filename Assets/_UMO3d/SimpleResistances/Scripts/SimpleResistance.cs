﻿// =======================================================================================
// SIMPLE RESISTANCE - CLASS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

// ---------------------------------------------------------------------------------------
// SimpleResistance
// ---------------------------------------------------------------------------------------
namespace UMO3d {
 
	[System.Serializable]
	public struct SimpleResistance {
		public string name;
		public int value;
 
		public SimpleResistance(string _name, int _value) {
			name = _name;
			value = _value;
		}
	}
 
	public class SyncListSimpleResistance : SyncListStruct<SimpleResistance> { }

}

// =======================================================================================