﻿// =======================================================================================
// SIMPLE RESISTANCES - SKILL
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// SKILL
// =======================================================================================
public partial struct Skill {
	
	public Constants.Elements element {
		get { return template.element; }
	}

	// ---------- Level dependant Stats
	
	public float buffResFire {
		get { return template.levels[level-1].buffResFire; }
	}
	
	public float buffResWater {
		get { return template.levels[level-1].buffResWater; }
	}

	public float buffResEarth {
		get { return template.levels[level-1].buffResEarth; }
	}

	public float buffResAir {
		get { return template.levels[level-1].buffResAir; }
	}

	public float buffResMagic {
		get { return template.levels[level-1].buffResMagic; }
	}

	public float buffResLight {
		get { return template.levels[level-1].buffResLight; }
	}

	public float buffResDark {
		get { return template.levels[level-1].buffResDark; }
	}

	public float buffResSlashing {
		get { return template.levels[level-1].buffResSlashing; }
	}

	public float buffResCrushing {
		get { return template.levels[level-1].buffResCrushing; }
	}

	public float buffResPiercing {
		get { return template.levels[level-1].buffResPiercing; }
	}
	
	// ---------- ToolTip
	
	public void ToolTip_UMO3d_SimpleResistancesSkill(StringBuilder tip) {
        if (category.StartsWith("Attack")) {
       		tip.Append("\n");
        	tip.Append("Element: " + element.ToString() + "\n");
		}
        if (category.StartsWith("Buff")) {
        	tip.Append("\n");
        	if (buffResFire != 0) 		tip.Append( ((Constants.Elements)1).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResFire*10).ToString() 		+ "%\n");
        	if (buffResWater != 0) 		tip.Append( ((Constants.Elements)2).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResWater*10).ToString() 		+ "%\n");
        	if (buffResEarth != 0) 		tip.Append( ((Constants.Elements)3).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResEarth*10).ToString() 		+ "%\n");
        	if (buffResAir != 0) 		tip.Append( ((Constants.Elements)4).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResAir*10).ToString() 		+ "%\n");
        	if (buffResMagic != 0) 		tip.Append( ((Constants.Elements)5).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResMagic*10).ToString() 		+ "%\n");
        	if (buffResLight != 0) 		tip.Append( ((Constants.Elements)6).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResLight*10).ToString() 		+ "%\n");
        	if (buffResDark != 0) 		tip.Append( ((Constants.Elements)7).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResDark*10).ToString() 		+ "%\n");
        	if (buffResSlashing != 0)	tip.Append( ((Constants.Elements)8).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResSlashing*10).ToString() 	+ "%\n");
        	if (buffResCrushing != 0)	tip.Append( ((Constants.Elements)9).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(buffResCrushing*10).ToString() 	+ "%\n");
        	if (buffResPiercing != 0)	tip.Append( ((Constants.Elements)10).ToString() + " Resistance: " 	+ Mathf.RoundToInt(buffResPiercing*10).ToString() 	+ "%\n");
        }
    }
    
}

// =======================================================================================