﻿// =======================================================================================
// SIMPLE RESISTANCES - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// ENTITY
// =======================================================================================
public partial class Entity {
	
	public SyncListSimpleResistance UMO3d_Resistances = new SyncListSimpleResistance();
	
	[Header("SimpleResistances - Default Resistances")]
	[Range(-1,1)] public float defaultResFire;
	[Range(-1,1)] public float defaultResWater;
	[Range(-1,1)] public float defaultResEarth;
	[Range(-1,1)] public float defaultResAir;
	[Range(-1,1)] public float defaultResMagic;
	[Range(-1,1)] public float defaultResLight;
	[Range(-1,1)] public float defaultResDark;
	[Range(-1,1)] public float defaultResSlashing;
	[Range(-1,1)] public float defaultResCrushing;
	[Range(-1,1)] public float defaultResPiercing;
	
	[Header("SimpleResistances - Attack Element")]
	public Constants.Elements element;

	// -----------------------------------------------------------------------------------
	// DealDamageAtEaUMO3d_UMO3d_SimpleResistances
	// -----------------------------------------------------------------------------------
	private void DealDamageAtEaUMO3d_UMO3d_SimpleResistances(Entity entity, int damageDealt, Skill skill) {
		var elementalModifier = UMO3d_GetElementalModifier(entity, UMO3d_GetAttackElement(skill));
		elementalModifier = Mathf.Clamp(elementalModifier, Constants.UMO3d_RESISTANCE_MIN, Constants.UMO3d_RESISTANCE_MAX);
		damageModifier = Convert.ToInt32(damageDealt * elementalModifier);
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_GetAttackElement
	// -----------------------------------------------------------------------------------
	private float UMO3d_GetElementalModifier(Entity entity, Constants.Elements element) {	
	
		switch ((int)element) {
			
			case 1:
				return entity.resistFire * -1;
			case 2:
				return entity.resistWater * -1;
			case 3:
				return entity.resistEarth * -1;
			case 4:
				return entity.resistAir * -1;
			case 5:
				return entity.resistMagic * -1;
			case 6:
				return entity.resistLight * -1;
			case 7:
				return entity.resistDark * -1;
			case 8:
				return entity.resistSlashing * -1;
			case 9:
				return entity.resistCrushing * -1;
			case 10:
				return entity.resistPiercing * -1;
			default:
				return 0;
		}
		

	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_GetAttackElement
	// -----------------------------------------------------------------------------------
	public virtual Constants.Elements UMO3d_GetAttackElement(Skill skill) {
	
		// ---------- check casted skill for element
		if ((int)skill.element != 0) {
			return skill.element;
		}
		
		// ---------- check entities base element
		return element;
	
	}
	
	// ===================================================================================
	// ===================================================================================
	
	// -----------------------------------------------------------------------------------
	// resistFire
	// -----------------------------------------------------------------------------------
    public virtual float resistFire {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResFire).Sum();
            return defaultResFire + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistWater
	// -----------------------------------------------------------------------------------
    public virtual float resistWater {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResWater).Sum();
            return defaultResWater + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistEarth
	// -----------------------------------------------------------------------------------
    public virtual float resistEarth {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResEarth).Sum();
            return defaultResEarth + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistAir
	// -----------------------------------------------------------------------------------
    public virtual float resistAir {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResAir).Sum();
            return defaultResAir + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistMagic
	// -----------------------------------------------------------------------------------
    public virtual float resistMagic {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResMagic).Sum();
            return defaultResMagic + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistLight
	// -----------------------------------------------------------------------------------
    public virtual float resistLight {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResLight).Sum();
            return defaultResLight + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistDark
	// -----------------------------------------------------------------------------------
    public virtual float resistDark {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResDark).Sum();
            return defaultResDark + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistSlashing
	// -----------------------------------------------------------------------------------
    public virtual float resistSlashing {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResSlashing).Sum();
            return defaultResSlashing + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistCrushing
	// -----------------------------------------------------------------------------------
    public virtual float resistCrushing {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResCrushing).Sum();
            return defaultResCrushing + buffBonus;
        }
    }

	// -----------------------------------------------------------------------------------
	// resistPiercing
	// -----------------------------------------------------------------------------------
    public virtual float resistPiercing {
        get {
            float buffBonus = (from skill in skills
                               where skill.BuffTimeRemaining() > 0
                               select skill.buffResPiercing).Sum();
            return defaultResPiercing + buffBonus;
        }
    }
	
}

// =======================================================================================