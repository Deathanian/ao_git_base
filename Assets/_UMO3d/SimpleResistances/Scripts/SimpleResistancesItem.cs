﻿// =======================================================================================
// SIMPLE RESISTANCES - ITEM
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// =======================================================================================
// ITEM
// =======================================================================================
public partial struct Item {

	public Constants.Elements element {
		get { return template.element; }
	}
	
	public float equipResFire {
		get { return template.equipResFire; }
	}

	public float equipResWater {
		get { return template.equipResWater; }
	}

	public float equipResEarth {
		get { return template.equipResEarth; }
	}

	public float equipResAir {
		get { return template.equipResAir; }
	}

	public float equipResMagic {
		get { return template.equipResMagic; }
	}

	public float equipResLight {
		get { return template.equipResLight; }
	}

	public float equipResDark {
		get { return template.equipResDark; }
	}

	public float equipResSlashing {
		get { return template.equipResSlashing; }
	}

	public float equipResCrushing {
		get { return template.equipResCrushing; }
	}

	public float equipResPiercing {
		get { return template.equipResPiercing; }
	}

	// ---------- ToolTip
	
	public void ToolTip_UMO3d_SimpleResistancesItem(StringBuilder tip) {
       	if (category.StartsWith("EquipmentWeapon")) {
       		tip.Append("\n");
        	tip.Append("Element: " + element.ToString() + "\n");
		}
        if (category.StartsWith("Equipment")) {
        	tip.Append("\n");
        	if (equipResFire != 0) 		tip.Append( ((Constants.Elements)1).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResFire*100).ToString() 		+ "%\n");
        	if (equipResWater != 0) 	tip.Append( ((Constants.Elements)2).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResWater*100).ToString() 		+ "%\n");
        	if (equipResEarth != 0) 	tip.Append( ((Constants.Elements)3).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResEarth*100).ToString() 		+ "%\n");
        	if (equipResAir != 0)		tip.Append( ((Constants.Elements)4).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResAir*100).ToString() 			+ "%\n");
        	if (equipResMagic != 0) 	tip.Append( ((Constants.Elements)5).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResMagic*100).ToString() 		+ "%\n");
        	if (equipResLight != 0) 	tip.Append( ((Constants.Elements)6).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResLight*100).ToString() 		+ "%\n");
        	if (equipResDark != 0) 		tip.Append( ((Constants.Elements)7).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResDark*100).ToString() 		+ "%\n");
        	if (equipResSlashing != 0) 	tip.Append( ((Constants.Elements)8).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResSlashing*100).ToString() 	+ "%\n");
        	if (equipResCrushing != 0) 	tip.Append( ((Constants.Elements)9).ToString() 	+ " Resistance: " 	+ Mathf.RoundToInt(equipResCrushing*100).ToString() 	+ "%\n");
        	if (equipResPiercing != 0) 	tip.Append( ((Constants.Elements)10).ToString() + " Resistance: " 	+ Mathf.RoundToInt(equipResPiercing*100).ToString() 	+ "%\n");
        }
    } 

	
}

// =======================================================================================