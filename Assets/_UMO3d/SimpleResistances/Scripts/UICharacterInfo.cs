﻿// =======================================================================================
// SIMPLE ATTRIBUTES - UI CHARACTER INFO
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.UI;

// =======================================================================================
// UI CHARACTER INFO
// =======================================================================================
public partial class UICharacterInfo : MonoBehaviour {
    
    public Text TextRes1;
    public Text TextRes2;
    public Text TextRes3;
    public Text TextRes4;
    public Text TextRes5;
   	public Text TextRes6;
   	public Text TextRes7;
   	public Text TextRes8;
   	public Text TextRes9;
   	public Text TextRes10;
    
   	public Text TextResValue1;
    public Text TextResValue2;
    public Text TextResValue3;
    public Text TextResValue4;
    public Text TextResValue5;
   	public Text TextResValue6;
   	public Text TextResValue7;
   	public Text TextResValue8;
   	public Text TextResValue9;
   	public Text TextResValue10;
    
	// -----------------------------------------------------------------------------------
	// Update_UMO3d_UICharacterInfo_SimpleResistances
	// -----------------------------------------------------------------------------------
    void Update_UMO3d_UICharacterInfo_SimpleResistances() {
        var player = Utils.ClientLocalPlayer();
        if (!player) return;
        
        // only refresh the panel while it's active
        if (panel.activeSelf) {


			TextRes1.text		= ((Constants.Elements)1).ToString();
			TextResValue1.text	= Mathf.RoundToInt(player.resistFire*100).ToString() + "%";

			TextRes2.text		= ((Constants.Elements)2).ToString();
			TextResValue2.text	= Mathf.RoundToInt(player.resistWater*100).ToString() + "%";

			TextRes3.text		= ((Constants.Elements)3).ToString();
			TextResValue3.text	= Mathf.RoundToInt(player.resistEarth*100).ToString() + "%";

			TextRes4.text		= ((Constants.Elements)4).ToString();
			TextResValue4.text	= Mathf.RoundToInt(player.resistAir*100).ToString() + "%";

			TextRes5.text		= ((Constants.Elements)5).ToString();
			TextResValue5.text	= Mathf.RoundToInt(player.resistMagic*100).ToString() + "%";

			TextRes6.text		= ((Constants.Elements)6).ToString();
			TextResValue6.text	= Mathf.RoundToInt(player.resistLight*100).ToString() + "%";

			TextRes7.text		= ((Constants.Elements)7).ToString();
			TextResValue7.text	= Mathf.RoundToInt(player.resistDark*100).ToString() + "%";

			TextRes8.text		= ((Constants.Elements)8).ToString();
			TextResValue8.text	= Mathf.RoundToInt(player.resistSlashing*100).ToString() + "%";

			TextRes9.text		= ((Constants.Elements)9).ToString();
			TextResValue9.text	= Mathf.RoundToInt(player.resistCrushing*100).ToString() + "%";

			TextRes10.text		= ((Constants.Elements)10).ToString();
			TextResValue10.text	= Mathf.RoundToInt(player.resistPiercing*100).ToString() + "%";


        }       
    }
}
