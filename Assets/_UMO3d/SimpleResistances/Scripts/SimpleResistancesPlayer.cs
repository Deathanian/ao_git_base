﻿// =======================================================================================
// SIMPLE RESISTANCES - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {

	// -----------------------------------------------------------------------------------
	// UMO3d_GetAttackElement
	// -----------------------------------------------------------------------------------
	public override Constants.Elements UMO3d_GetAttackElement(Skill skill) {
		
		// ---------- check base element first
		var element = base.UMO3d_GetAttackElement(skill);
		
		// ---------- check equipped weapon for element
		for (int i = 0; i < equipment.Count; ++i) {
            if (equipment[i].valid &&
            	equipment[i].category.StartsWith("EquipmentWeapon") &&
                (int)equipment[i].element != 0) {
                	if ((int)equipment[i].element != 0) return equipment[i].element;
    		}
    	}

		return element;
	
	}
	
	// ===================================================================================
	// ===================================================================================

	// -----------------------------------------------------------------------------------
	// resistFire
	// -----------------------------------------------------------------------------------
    public override float resistFire {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResFire).Sum();
                                    
        	float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistFire();
#endif

            return base.resistFire + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistWater
	// -----------------------------------------------------------------------------------
    public override float resistWater {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResWater).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistWater();
#endif

            return base.resistWater + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistEarth
	// -----------------------------------------------------------------------------------
    public override float resistEarth {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResEarth).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistEarth();
#endif

            return base.resistEarth + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistAir
	// -----------------------------------------------------------------------------------
    public override float resistAir {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResAir).Sum();
                                    
            float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistAir();
#endif

            return base.resistAir + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistMagic
	// -----------------------------------------------------------------------------------
    public override float resistMagic {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResMagic).Sum();
                                    
			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistMagic();
#endif

            return base.resistMagic + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistLight
	// -----------------------------------------------------------------------------------
    public override float resistLight {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResLight).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistLight();
#endif

            return base.resistLight + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistDark
	// -----------------------------------------------------------------------------------
    public override float resistDark {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResDark).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistDark();
#endif

            return base.resistDark + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistSlashing
	// -----------------------------------------------------------------------------------
    public override float resistSlashing {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResSlashing).Sum();
                                    
			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistSlashing();
#endif

            return base.resistSlashing + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistCrushing
	// -----------------------------------------------------------------------------------
    public override float resistCrushing {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResCrushing).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistCrushing();
#endif

            return base.resistCrushing + equipmentBonus + setBonus;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// resistPiercing
	// -----------------------------------------------------------------------------------
    public override float resistPiercing {
        get {
            float equipmentBonus = (from item in equipment
                                    where item.valid
                                    select item.equipResPiercing).Sum();

			float setBonus = 0;
#if _SimpleEquipmentSets
        	setBonus = UMO3d_getSetBonusResistPiercing();
#endif

            return base.resistPiercing + equipmentBonus + setBonus;
        }
    }    
    
}

// =======================================================================================