﻿// =======================================================================================
// SIMPLE RESISTANCES - ITEM TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// ITEM TEMPLATE
// =======================================================================================
public partial class ItemTemplate : ScriptableObject {
   	
   	[Header("UMO3d SimpleResistances - Item Element")]
	public Constants.Elements element;

   	[Header("UMO3d SimpleResistances - Equipment Modifiers")]
   	[Range(-1,1)] public float equipResFire;
   	[Range(-1,1)] public float equipResWater;
   	[Range(-1,1)] public float equipResEarth;
   	[Range(-1,1)] public float equipResAir;
   	[Range(-1,1)] public float equipResMagic;
   	[Range(-1,1)] public float equipResLight;
   	[Range(-1,1)] public float equipResDark;
   	[Range(-1,1)] public float equipResSlashing;
   	[Range(-1,1)] public float equipResCrushing;
   	[Range(-1,1)] public float equipResPiercing;
   	
}

// =======================================================================================