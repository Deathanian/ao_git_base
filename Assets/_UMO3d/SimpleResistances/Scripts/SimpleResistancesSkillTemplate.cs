﻿// =======================================================================================
// SIMPLE RESISTANCES - SKILL TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// SKILL TEMPLATE
// =======================================================================================
public partial class SkillTemplate : ScriptableObject {
   
	[Header("SimpleResistances - Skill Element")]
	public Constants.Elements element;

    public partial struct SkillLevel {
        // Pragmatic but not erroneous, Nullify the Warning
#pragma warning disable
        [Header("SimpleResistances - Buff Modifiers")]
   		[Range(-1,1)] public float buffResFire;
 	  	[Range(-1,1)] public float buffResWater;
	   	[Range(-1,1)] public float buffResEarth;
	   	[Range(-1,1)] public float buffResAir;
 	  	[Range(-1,1)] public float buffResMagic;
 	  	[Range(-1,1)] public float buffResLight;
 	  	[Range(-1,1)] public float buffResDark;
  	 	[Range(-1,1)] public float buffResSlashing;
  	 	[Range(-1,1)] public float buffResCrushing;
   		[Range(-1,1)] public float buffResPiercing;
#pragma warning restore
    }
   
}

// =======================================================================================