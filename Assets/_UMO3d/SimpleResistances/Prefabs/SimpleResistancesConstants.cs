﻿// =======================================================================================
// SIMPLE RESISTANCE - CONSTANTS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Constants {
		
	  	// -----------------------------------------------------------------------------------
		// Constants
		// -----------------------------------------------------------------------------------
		public const float UMO3d_RESISTANCE_MIN	=	-100.0f;
		public const float UMO3d_RESISTANCE_MAX	=	100.0f;
		
  		// -----------------------------------------------------------------------------------
		// Elements Enumerations
		// -----------------------------------------------------------------------------------
		public enum Elements 			{ 
		
			None,
		
			Fire, 
			Water, 
			Earth, 
			Air, 
			Magic, 
			Light, 
			Dark, 
			Slashing, 
			Crushing, 
			Piercing
		
		}
		
		// -----------------------------------------------------------------------------------
		// Important Information:
		//
		// * In order to change the names of elements appearing in your game, you have to edit
		//   the entries above.
		//
		// * Do not change the order of elements once you set their names. If you switch Air
		//   with Dark for example it will mess up your whole game.
		//
		// * Do not delete any of the entries and do not add new ones. This system is limited
		//   to 10 elements (plus "None"). That number should be enough for most games.
		//
		// * If you mess up the statement above, here is a copy of the original. just remove
		//   the /* at the beginning and the */ at the end:
		
		/*
		public enum Elements 			{ 
		
			None,
		
			Fire, 
			Water, 
			Earth, 
			Air, 
			Magic, 
			Light, 
			Dark, 
			Slashing, 
			Crushing, 
			Piercing
		
		}
		*/

		// -----------------------------------------------------------------------------------

	}

}	
// ===================================================================================