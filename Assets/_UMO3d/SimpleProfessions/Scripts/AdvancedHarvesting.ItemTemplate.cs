﻿// =======================================================================================
// ADVANCED HarvestingS - ITEM TEMPLATE
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

#if _AdvancedHarvesting

	// =======================================================================================
	// ITEM TEMPLATE
	// =======================================================================================
	public partial class ItemTemplate {
	
		[Header("UMO3d - AdvancedHarvesting")]
		public AdvancedHarvestingProfessionTemplate learnProfession;
		public int gainProfessionExp;
	
	}

#endif

// =======================================================================================