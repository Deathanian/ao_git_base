//#define SPAWN_MANAGER

using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ResourceNode : Entity
#if SPAWN_MANAGER
    , ISpawnableEntity
#endif
{
    #region unity fields
    [Header("Collectible")]
    public float harvestLength = 2;

    public string collectibleBy;

    public int minimumProfessionLevel = 1;

    public int professionExperienceReward = 2;

    public ItemTemplate collectedItem;

    public RangeInt amount;

    public bool autoRespawn;

    public RangeFloat respawnTime;

    #endregion

    #region vars

    [SyncVar] bool spawned = true;
#if SPAWN_MANAGER
    [SyncVar] bool collected = false;
#endif
    public virtual bool Harvestable
    {
        get
        {
            return spawned;
        }
    }

#if SPAWN_MANAGER

    [SyncVar] bool isLinkedToregion = false;

    public virtual bool isDead
    {
        get
        {
            return collected && isLinkedToregion;
        }
    }

#endif

    #endregion

    #region functions

    public virtual bool CanBeCollected(Player p = null)
    {
        if (p == null)
            return Harvestable;
        if (p.professions.Count == 0 || !p.HasProfession(collectibleBy))
            return false;
        return Harvestable && p.GetProfession(collectibleBy).Level >= minimumProfessionLevel;
    }
    
    [Server]
    public virtual void GetCollectedBy(Player p)
    {
        int a = Random.Range(amount.min, amount.max);
        if (p.InventoryCanAddAmount(collectedItem, a))
        {
            p.InventoryAddAmount(collectedItem, a);
            Unspawn();
        }
        else
        {
            p.SendPopup("Your inventory is full!");
        }

        p.target = null;

        var prof = p.GetProfession(collectibleBy);
        int oldLevel = prof.Level;
        prof.experience += professionExperienceReward;
        p.SetProfession(collectibleBy, prof);

        if (oldLevel < prof.Level)
            p.SendPopup("You profession of " + prof.templateName + " just passed\n level " + prof.Level);
    }

    [Server]
    public virtual void Unspawn()
    {
        if (autoRespawn)
        {
            GetComponentInChildren<Renderer>().enabled = false;
            collider.enabled = false;

            StartCoroutine("RespawnCoroutine");
        }
#if SPAWN_MANAGER
        else if (isLinkedToregion)
        {
            GetComponentInChildren<Renderer>().enabled = false;
            collider.enabled = false;
            collected = true;
        }
#endif
        else
            NetworkServer.Destroy(gameObject);

        if (!isClient)
            RpcUnspawn();
    }

    [ClientRpc]
    protected virtual void RpcUnspawn()
    {
        if (autoRespawn)
        {
            GetComponentInChildren<Renderer>().enabled = false;
            collider.enabled = false;
        }
#if SPAWN_MANAGER
        else if (isLinkedToregion)
        {
            GetComponentInChildren<Renderer>().enabled = false;
            collider.enabled = false;
            collected = true;
        }
#endif
        else
            NetworkServer.Destroy(gameObject);
    }

    public virtual IEnumerator RespawnCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(respawnTime.min, respawnTime.max));
        Respawn();
    }

    [Server]
    public virtual void Respawn()
    {
        GetComponentInChildren<Renderer>().enabled = true;
        collider.enabled = true;
        if (!isClient)
            RpcRespawn();
    }

    [ClientRpc]
    protected virtual void RpcRespawn()
    {
        GetComponentInChildren<Renderer>().enabled = true;
        collider.enabled = true;
    }

#if SPAWN_MANAGER

    public virtual void LinkToRegion()
    {
        isLinkedToregion = true;
    }

    public virtual void UnlinkToRegion()
    {
        NetworkServer.Destroy(gameObject);
    }

    public virtual void SetRespawn(bool b)
    {
        autoRespawn = b;
    }

#endif

#endregion

#region entity stuff

    public override int manaMax { get { return 0; } }

    public override int damage { get { return 0; } }

    public override int defense { get { return 0; } }

    public override float blockChance { get { return 0; } }

    public override float criticalChance { get { return 0; } }

    public override int healthMax { get { return 1; } }

    public override bool CanAttack(Entity entity) { return false; }

    public override bool HasCastWeapon() { return false; }

    protected override void UpdateClient() { }

    protected override string UpdateServer() { return state; }

#endregion
}
