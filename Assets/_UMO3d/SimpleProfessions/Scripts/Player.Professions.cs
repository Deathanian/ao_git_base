//#define UMMORPG_NEW_QUESTS
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public partial class Player
{
    [Header("Harvesting")]
    public SyncListProfession professions;

    public float harvestingRange = 2;

    bool isHarvesting = false;

    TimeCounter harvestCounter;

    ObjectFinder<UIPopup> popup = new ObjectFinder<UIPopup>();

    public void Start_Harvesting()
    {
        harvestCounter = new TimeCounter(0);
        NetworkServer.SpawnObjects();
    }

    [Command]
    public void CmdAddProfession(Profession p)
    {
        professions.Add(p);
    }

    public Profession GetProfession(string p)
    {
        if (professions.Count == 0 || !HasProfession(p))
            throw new Exception("no professions");
        return professions.First(pr => pr.templateName == p);
    }

    public void SetProfession(string p, Profession prof)
    {
        int id = professions.FindIndex(pr => pr.templateName == p);
        professions[id] = prof;
    }

    public bool HasProfession(ProfessionTemplate p)
    {
        return HasProfession(p.name);
    }
    public bool HasProfession(string p)
    {
        return professions.Any(prof => prof.templateName == p);
    }

    public void OnSelect_Harvesting(Entity e)
    {
        if(e is ResourceNode)
        {
            var col = (ResourceNode)e;

            if(!col.Harvestable)
            {
                target = null;
                Destroy(indicator);
                return;
            }

            if(Utils.ClosestDistance(e.collider, collider) <= harvestingRange)
            {
                if (!isHarvesting && col.CanBeCollected(this))
                {
                    agent.ResetPath();
                    StartCollecting(col);
                }
                else if (!isHarvesting)
                {
                    popup.val.Show("You need to be a " + col.collectibleBy + " of at least\n level " + col.minimumProfessionLevel + " to collect this");
                }
            }
            else
            {
                CmdNavigateTo(e.collider.ClosestPointOnBounds(transform.position), harvestingRange);
            }
        }
    }

    public void LateUpdate_Harvesting()
    {
        if(isHarvesting && (state != "IDLE" || !(target is ResourceNode) || harvestCounter.ready))
        {
            StopCollecting();
            if(target is ResourceNode && harvestCounter.ready)
            {
                CmdCollect(target.netId);
            }
        }
    }

    [Command]
    void CmdCollect(NetworkInstanceId c)
    {
#if UMMORPG_ADVANCED_QUESTS
        IncreaseAdvancedQuestsHarvestCounterFor(target.name);
#endif
        NetworkServer.FindLocalObject(c).GetComponent<ResourceNode>().GetCollectedBy(this);
    }

    private void StartCollecting(ResourceNode col)
    {
        if (isClient)
        {
            foreach (var anim in GetComponentsInChildren<Animator>())
            {
                harvestCounter = new TimeCounter(col.harvestLength);
                isHarvesting = true;
                if(anim.parameters.Any(p => p.name == "HARVESTING"))
                    anim.SetBool("HARVESTING", true);
            }
        }
    }
    private void StopCollecting()
    {
        if (isClient)
        {
            foreach (var anim in GetComponentsInChildren<Animator>())
            {
                isHarvesting = false;
                if (anim.parameters.Any(p => p.name == "HARVESTING"))
                    anim.SetBool("HARVESTING", false);
                Destroy(indicator);
            }
        }
    }

#if UMMORPG_ADVANCED_QUESTS

    [Server]
    public void IncreaseNewQuestHarvestCounterFor(string harvestName)
    {
        for (int i = 0; i < advancedQuests.Count; ++i)
        {
            // active quest and not completed yet?
	if (!advancedQuests[i].completed)
	if (advancedQuests[i].harvested.Any(ni => ni.name == harvestName))
                {
	var quest = advancedQuests[i];
                    quest.harvested.First(q => q.name == harvestName).value++;
	advancedQuests[i] = quest;
                    TryCompleteNewQuest(i);
                }
        }

    }

#endif

    public void SendPopup(string msg)
    {
        if (isClient)
            new ObjectFinder<UIPopup>().val.Show(msg);
        else
            RpcSendPopup(msg);
    }

    [ClientRpc]
    private void RpcSendPopup(string msg)
    {
        SendPopup(msg);
    }
}
