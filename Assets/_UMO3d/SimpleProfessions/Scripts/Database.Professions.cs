﻿using Mono.Data.Sqlite;
using System;

public partial class Database
{
    static void Initialize_Harvesting()
    {
        ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS character_professions (
                            character TEXT NOT NULL,
                            profession TEXT NOT NULL,
                            experience INTEGER)");
    }

    public static void CharacterLoad_Harvesting(Player player)
    {
        var table = ExecuteReader(
            "SELECT profession, experience FROM character_professions WHERE character=@character",
            new SqliteParameter("@character", player.name));

        foreach (var row in table)
        {
            var profession = new Profession((string)row[0]);
            profession.experience = Convert.ToInt32((long)row[1]);
            player.professions.Add(profession);
        }
    }

    public static void CharacterSave_Harvesting(Player player)
    {
        ExecuteNonQuery("DELETE FROM character_professions WHERE character=@character", new SqliteParameter("@character", player.name));
        foreach (var profession in player.professions)
            ExecuteNonQuery("INSERT INTO character_professions VALUES (@character, @profession, @experience)",
                            new SqliteParameter("@character", player.name),
                            new SqliteParameter("@profession", profession.templateName),
                            new SqliteParameter("@experience", profession.experience));
    }
}