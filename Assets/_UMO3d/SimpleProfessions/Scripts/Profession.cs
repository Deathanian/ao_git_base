﻿using System.Linq;
using UnityEngine.Networking;

public struct Profession
{
    #region vars

    public string templateName;

    public int experience;

    #endregion

    #region getter

    public int Level
    {
        get
        {
            int xp = this.experience;
            return Template.levels.Count(l => l <= xp);
        }
    }

    public ProfessionTemplate Template
    {
        get
        {
            return ProfessionTemplate.Professions[templateName];
        }
    }

    #endregion

    public Profession(string templateName)
    {
        this.templateName = templateName;
        experience = 0;
    }

    #region functions

    #endregion
}

public class SyncListProfession : SyncListStruct<Profession> { }