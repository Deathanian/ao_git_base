//#define SIMPLE_WEATHER
//#define UMMORPG_SEASONS_SUN

using System;
using UnityEngine;
using UnityEngine.Networking;

public class GrowableResourceNode : ResourceNode
{
    [Header("Growable data")]
    public RangeFloat growingLength;

#if SIMPLE_WEATHER
    public bool canGrowDuringSun = true;
    public bool canGrowDuringRain = true;

    ObjectFinder<WeatherManager> weather = new ObjectFinder<WeatherManager>();
#endif

#if UMMORPG_SEASONS_SUN
    public bool canGrowDuringDay = true;
    public bool canGrowDuringNight = true;

    ObjectFinder<SunController> sun = new ObjectFinder<SunController>();
#endif

    Counter growingCounter = new Counter(0, false);

    bool shouldBeGrowing = false;

    [SyncVar] float progress;
    [SyncVar] bool ready;

    public override void OnStartClient()
    {
        if (!ready)
            collider.enabled = false;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        StartGrowing();
    }

    [Server]
    void StartGrowing()
    {
        collider.enabled = false;
        shouldBeGrowing = true;
        growingCounter = new Counter((int)(growingLength.random / Time.fixedDeltaTime), false);

        RpcStartGrowing();
    }

    [ClientRpc]
    void RpcStartGrowing()
    {
        collider.enabled = false;
    }

    private void FixedUpdate()
    {
        if (isServer && ShouldBeGrowing())
        {
            ready = growingCounter.ready;
            progress = growingCounter.progress;

        }
        if (!ready)
        {
            transform.localScale = Vector3.one * progress;
        }
        else
        {
            shouldBeGrowing = false;
            collider.enabled = true;
            transform.localScale = Vector3.one;
        }
    }

    public virtual bool ShouldBeGrowing()
    {
        bool b = shouldBeGrowing;

#if SIMPLE_WEATHER
            b = b ? ((weather.val.GetWeather() == Weather.RAIN && canGrowDuringRain) ||
                (weather.val.GetWeather() == Weather.SUN && canGrowDuringSun)) : false;
#endif

#if UMMORPG_SEASONS_SUN
        b = b ? (sun.val.isDay && canGrowDuringDay) || (sun.val.isNight && canGrowDuringNight) : false;
#endif

        return b;
    }

    public override bool Harvestable
    {
        get
        {
            return base.Harvestable && ready;
        }
    }

    public override void Respawn()
    {
        base.Respawn();
        StartGrowing();
    }
}
