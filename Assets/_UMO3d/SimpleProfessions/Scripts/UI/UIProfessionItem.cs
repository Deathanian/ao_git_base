﻿using UnityEngine;
using UnityEngine.UI;

public class UIProfessionItem : MonoBehaviour
{
    [HideInInspector]
    public Text text;

    private string template;

    void Awake()
    {
        text = GetComponentInChildren<Text>();
        template = text.text;
    }

    public void ShowProfession(Profession p)
    {
        string t = template.Replace("{{name}}", p.templateName);
        t = t.Replace("{{description}}", p.Template.description);
        t = t.Replace("{{level}}", p.Level.ToString());

        text.text = t;
    }
}