﻿using UnityEngine;

public class UIProfession : MonoBehaviour
{
    public KeyCode hotKey = KeyCode.P;

    public GameObject panel;

    public Transform content;

    public GameObject professionItemPrefab;

    Counter refresh = new Counter(10);

    public void InvertShow()
    {
        RefreshDisplay();
        panel.SetActive(!panel.activeSelf);
    }

    private void RefreshDisplay()
    {
        Player p = Utils.ClientLocalPlayer();
        UIUtils.BalancePrefabs(professionItemPrefab, p.professions.Count, content);

        for (int i = 0; i < content.childCount; i++)
        {
            content.GetChild(i).GetComponent<UIProfessionItem>().ShowProfession(p.professions[i]);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(hotKey) && !UIUtils.AnyInputActive())
            InvertShow();
        if (panel.activeSelf && refresh.ready)
            RefreshDisplay();
    }
}