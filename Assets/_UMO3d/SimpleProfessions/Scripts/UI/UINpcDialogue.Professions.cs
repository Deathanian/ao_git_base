﻿using UnityEngine;
using UnityEngine.UI;

public partial class UINpcDialogue
{
    [Header("Professions")]
    public Button learnProfessionButton;

    public Text learnProfessionText;

    public string learnProfessionFormat = "Learn {{profession}}";

    ObjectFinder<UIPopup> popup = new ObjectFinder<UIPopup>();

    public void Update_Harvesting()
    {
        var player = Utils.ClientLocalPlayer();
        if (!player) return;

        if (panel.activeSelf &&
            player.target != null && player.target is Npc &&
            Utils.ClosestDistance(player.collider, player.target.collider) <= player.interactionRange)
        {
            var npc = (Npc)player.target;

            learnProfessionButton.gameObject.SetActive(npc.professionOffer != null && !player.HasProfession(npc.professionOffer));
            if (npc.professionOffer != null && !player.HasProfession(npc.professionOffer))
            {
                learnProfessionText.text = learnProfessionFormat.Replace("{{profession}}", npc.professionOffer.name);

                learnProfessionButton.onClick.SetListener(() => {
                    panel.SetActive(false);
                    player.CmdAddProfession(new Profession(npc.professionOffer.name));
                    Debug.Log("successfully added profession " + npc.professionOffer.name);
                    popup.val.Show("You successfully learned the\n profession of " + npc.professionOffer.name);
                });
            }
        }
    }
}