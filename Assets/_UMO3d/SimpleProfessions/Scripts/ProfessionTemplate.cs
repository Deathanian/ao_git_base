﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New Profession", menuName = "Create Profession")]
public class ProfessionTemplate : ScriptableObject
{
    public int[] levels;

    [TextArea]
    public string description;

    static Dictionary<string, ProfessionTemplate> _professions = null;
    public static Dictionary<string, ProfessionTemplate> Professions
    {
        get
        {
            // load if not loaded yet
            return _professions ?? (
                _professions = Resources.LoadAll<ProfessionTemplate>("").ToDictionary(profession => profession.name, profession => profession)
            );
        }
    }
}