﻿======================================================================
INDIE-MMO -UMMORPG & UNITY ASSETS
======================================================================
* Copyright by INDIE-MMO -no permission to share, re-sell or give away
* Authors: Fhiz & Stue
* This asset is an Add-On for uMMORPG3d, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.indie-mmo.com
* NuCore Download (required for most AddOns): http://www.indie-mmo.com/butler/index.php
* Support via eMail: support@indie-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Locate the Prefabs folder within this AddOns folder. 

1. Drag and Drop the "SimpleDailyRewardUI" prefab in your scenes canvas. It is important to
place it inside the canvas object in the hierarchy, the exact position is irrelevant (just
not inside another object).

2. Locate the SimpleDailyRewards folder inside your AddOns folder and click once on the
Simple Daily Rewards scriptable object. Edit all settings in the inspector.

3. Locate each one of your player prefabs and assign the new "SimpleDailyRewards" property,
to the Scriptable Object "SimpleDailyRewards" that you just edited.

Optional: You can create more than one "SimpleDailyRewards" ScriptableObject and assign
a unique one to each of your player prefabs. This allows you to have unique rewards based
on player classes.

If you accidentally delete your SimpleDailyRewards ScriptableObject, right click
while inside your SimpleDailyRewards folder, choose "Create" and select "New Simple Daily
Reward" to create a fresh Scriptable Object.

Thats it, no script modifications!

COMPLETE GUIDE (MORE INFO AND DETAILS)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
http://www.indie-mmo.com/simpledailyrewards-complete-guide/

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
SimpleDailyRewards adds a popular feature to your game: Reward your players when logging
into your game on a daily basis. Keep your players interested and coming back for more!

* Setup as many rewards as you want.
* Setup the minimum timeframe.
* Grant gold/coins/experience or an item as part of the reward.

Players will see the reward in a popup window when logging on, as well as the contents of
the next reward (if any). Rewards are automatically applied to the players
character/inventory.

======================================================================