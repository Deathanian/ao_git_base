﻿// =======================================================================================
// SIMPLE DAILY REWARDS - DATABASE
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using Mono.Data.Sqlite; // copied from Unity/Mono/lib/mono/2.0 to Plugins
using System;
using System.Collections;

#if _SimpleDailyRewards

// =======================================================================================
// DATABASE
// =======================================================================================
public partial class Database {
	
	// -----------------------------------------------------------------------------------
	// Initialize_UMO3d_SimpleDailyRewards
	// -----------------------------------------------------------------------------------
	static void Initialize_UMO3d_SimpleDailyRewards() {
		ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS UMO3d_character_lastonline (
				character TEXT NOT NULL,
				lastOnline TEXT NOT NULL)");
				
		ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS UMO3d_character_dailyrewards (
				character TEXT NOT NULL,
				counter INTEGER NOT NULL,
				resetTime FLOAT NOT NULL)");		
				
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterLoad_UMO3d_SimpleDailyRewards
	// -----------------------------------------------------------------------------------
	public static void CharacterLoad_UMO3d_SimpleDailyRewards(Player player) {
		var table = ExecuteReader("SELECT counter, resetTime FROM UMO3d_character_dailyrewards WHERE character=@name", new SqliteParameter("@name", player.name));
		if (table.Count == 1) {
            var row = table[0];
			player.dailyRewardCounter 	= Convert.ToInt32((long)row[0]);
			player.dailyRewardReset		= (double)row[1];
		}
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterSave_UMO3d_SimpleDailyRewards
	// -----------------------------------------------------------------------------------
	static void CharacterSave_UMO3d_SimpleDailyRewards(Player player) {
		
		DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		TimeSpan timeSinceEpoch = DateTime.UtcNow - UnixEpoch;

		ExecuteNonQuery("DELETE FROM UMO3d_character_lastonline WHERE character=@character", new SqliteParameter("@character", player.name));
		ExecuteNonQuery("INSERT INTO UMO3d_character_lastonline VALUES (@character, @lastOnline)",
				new SqliteParameter("@lastOnline", DateTime.UtcNow.ToString("s")),
				new SqliteParameter("@character", player.name));
				
		ExecuteNonQuery("DELETE FROM UMO3d_character_dailyrewards WHERE character=@character", new SqliteParameter("@character", player.name));
		ExecuteNonQuery("INSERT INTO UMO3d_character_dailyrewards VALUES (@character, @counter, @resetTime)",
				new SqliteParameter("@character", player.name),
				new SqliteParameter("@counter", player.dailyRewardCounter),
				new SqliteParameter("@resetTime", timeSinceEpoch.TotalHours));
					
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_SimpleDailyRewards_HoursPassed
	// -----------------------------------------------------------------------------------
	public static double UMO3d_SimpleDailyRewards_HoursPassed(Player player) {
		var row = (string)ExecuteScalar("SELECT lastOnline FROM UMO3d_character_lastonline WHERE character=@name", new SqliteParameter("@name", player.name));
		if (!Utils.IsNullOrWhiteSpace(row)) {
			DateTime time 			= DateTime.Parse(row);
			return (DateTime.UtcNow - time).TotalSeconds/3600;
		}
		return 0;
		
	}

}

#endif

// =======================================================================================