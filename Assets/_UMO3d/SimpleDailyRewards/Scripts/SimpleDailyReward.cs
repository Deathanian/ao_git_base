﻿// =======================================================================================
// SIMPLE DAILY REWARDS - CLASS
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

namespace UMO3d {
	
	// ===================================================================================
	// SIMPLE DAILY REWARD
	// ===================================================================================
	[System.Serializable]
	public class SimpleDailyReward {
		public long rewardGold;
		public long rewardCoins;
		public long rewardExperience;
		public ItemTemplate rewardItem;
		public int itemAmount;
	}

	// ===================================================================================

}

// =======================================================================================
