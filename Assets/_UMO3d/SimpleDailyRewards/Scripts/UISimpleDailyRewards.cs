﻿// =======================================================================================
// SIMPLE DAILY REWARDS - UI
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.UI;

#if _SimpleDailyRewards

namespace UMO3d {

	// ===================================================================================
	// SIMPLE DAILY REWARDS UI
	// ===================================================================================
	public partial class UISimpleDailyRewards : MonoBehaviour {
		public GameObject panel;
		public Transform content;
		public ScrollRect scrollRect;
		public GameObject textPrefab;
   	
   		private const string MSG_REWARD_THIS	= "You received a login reward:";
   		private const string MSG_REWARD_NEXT	= "Next login reward: ";
   		private const string MSG_REWARD_RESET	= "<Daily Login Reward Reset>";
   		private const string MSG_REWARD_GOLD	= " - Gold: ";
   		private const string MSG_REWARD_COINS	= " - Coins: ";
   		private const string MSG_REWARD_EXP		= " - Experience: ";
   		private const string MSG_REWARD_ITEM	= " - Item: ";
	
		// -----------------------------------------------------------------------------------
		// Show
		// -----------------------------------------------------------------------------------
		public void Show(int dailyRewardCounter) {
			var player = Utils.ClientLocalPlayer();
			if (!player) return;
   			
			for (int i = 0; i < content.childCount; ++i) {
				Destroy(content.GetChild(i).gameObject);
			}

			if (player.SimpleDailyRewards != null && dailyRewardCounter > -1) {
				
				// ---------- Current Reward
				var rGold	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardGold;
				var rCoins	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardCoins;
				var rExp	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardExperience;
				var rItem	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem ? player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem.name : "";
				var rAmount = player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem ? player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].itemAmount : 0;
				
				if (rGold > 0 || rCoins > 0 || rExp > 0 || rItem != "" || rAmount > 0) {
					AddMessage(MSG_REWARD_THIS, Color.white);
					if (rGold > 0) 		AddMessage(MSG_REWARD_GOLD + rGold.ToString(), Color.green);
					if (rCoins > 0) 	AddMessage(MSG_REWARD_COINS + rCoins.ToString(), Color.green);
					if (rExp > 0) 		AddMessage(MSG_REWARD_EXP + rExp.ToString(), Color.green);
					if (rItem != "") 	AddMessage(MSG_REWARD_ITEM + rItem + " [x" + rAmount.ToString() + "]", Color.green);
				}
				
				// ---------- Next Reward
				if (player.dailyRewardCounter+1 < player.SimpleDailyRewards.SimpleDailyRewards.Length) {
			
					rGold	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardGold;
					rCoins	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardCoins;
					rExp	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardExperience;
					rItem	= player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardItem ? player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardItem.name : "";
					rAmount = player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].rewardItem ? player.SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter+1].itemAmount : 0;
				
					if (rGold > 0 || rCoins > 0 || rExp > 0 || rItem != "" || rAmount > 0) {
						AddMessage(MSG_REWARD_NEXT, Color.white);
						if (rGold > 0) 		AddMessage(MSG_REWARD_GOLD + rGold.ToString(), Color.green);
						if (rCoins > 0) 	AddMessage(MSG_REWARD_COINS + rCoins.ToString(), Color.green);
						if (rExp > 0) 		AddMessage(MSG_REWARD_EXP + rExp.ToString(), Color.green);
						if (rItem != "") 	AddMessage(MSG_REWARD_ITEM + rItem + " [x" + rAmount.ToString() + "]", Color.green);
					}
				
				} else {
					AddMessage(MSG_REWARD_NEXT, Color.white);
					AddMessage(MSG_REWARD_RESET, Color.red);
				}	
			
				panel.SetActive(true);
			
			}
					
		}

		// -----------------------------------------------------------------------------------
		// AutoScroll
		// -----------------------------------------------------------------------------------
		void AutoScroll() {
			Canvas.ForceUpdateCanvases();
			scrollRect.verticalNormalizedPosition = 0;
		}
	
		// -----------------------------------------------------------------------------------
		// AddMessage
		// -----------------------------------------------------------------------------------
		public void AddMessage(string msg, Color color) {
			var go = Instantiate(textPrefab);
			go.transform.SetParent(content.transform, false);
			go.GetComponent<Text>().text = msg;
			go.GetComponent<Text>().color = color;
			AutoScroll();
		}
	}

}

#endif

// =======================================================================================
