﻿// =======================================================================================
// SIMPLE DAILY REWARDS - TEMPLATE
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UMO3d {
	
	[CreateAssetMenu(fileName="SimpleDailyRewards", menuName="New SimpleDailyRewards", order=999)]
	public class SimpleDailyRewardsTemplate : ScriptableObject {
	
		[Tooltip("One click deactivation")]
		public bool isActive = true;
		[Tooltip("Hours between login to advance reward counter")]
		public int LoginIntervalHours = 23;
		[Tooltip("Define your daily rewards by adding and editing entries")]
		public SimpleDailyReward[] SimpleDailyRewards;

	}

}

// =======================================================================================