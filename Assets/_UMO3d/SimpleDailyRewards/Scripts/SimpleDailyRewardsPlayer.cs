﻿// =======================================================================================
// SIMPLE DAILY REWARDS - PLAYER
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

#if _SimpleDailyRewards

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	[SerializeField] public SimpleDailyRewardsTemplate SimpleDailyRewards;
	[HideInInspector] public int dailyRewardCounter = -1;
	[HideInInspector] public double dailyRewardReset = 0;
	
	// -----------------------------------------------------------------------------------
	// Start_UMO3d_SimpleDailyRewards
	// -----------------------------------------------------------------------------------
	[ServerCallback]
	public void Start_UMO3d_SimpleDailyRewards() {

		if (SimpleDailyRewards && SimpleDailyRewards.isActive) {
		
			var rewardHoursPassed 	= Database.UMO3d_SimpleDailyRewards_HoursPassed(this);
			DateTime UnixEpoch 		= new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan timeSinceEpoch = DateTime.UtcNow - UnixEpoch;

			// ---------- Reset
			if ( (dailyRewardCounter == -1 && dailyRewardReset == 0) ||
				(timeSinceEpoch.TotalHours >= (dailyRewardReset + (Mathf.Max(1,SimpleDailyRewards.LoginIntervalHours) * Mathf.Max(1,SimpleDailyRewards.SimpleDailyRewards.Length))) ) ||
				(dailyRewardCounter > SimpleDailyRewards.SimpleDailyRewards.Length) ) {
					dailyRewardReset = timeSinceEpoch.TotalHours;
					dailyRewardCounter = -1;
			}

			// ---------- Reward
			if (dailyRewardCounter+1 < SimpleDailyRewards.SimpleDailyRewards.Length && rewardHoursPassed >= SimpleDailyRewards.LoginIntervalHours) {
				dailyRewardCounter++;
				experience += SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardExperience;
				gold += SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardGold;
				coins += SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardCoins;
				if (SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem && SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].itemAmount > 0) {
					InventoryAddAmount(SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem, SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].itemAmount);
				}
				
				if (0 < SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardExperience ||
					0 < SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardGold ||
					0 < SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardCoins ||
					SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].rewardItem ||
					0 < SimpleDailyRewards.SimpleDailyRewards[dailyRewardCounter].itemAmount) {
						UMO3d_DailyRewardsProcess();	
				}
			}

		} else {
			Debug.LogWarning("SimpleDailyRewards: Either inactive or ScriptableObject not found!");
		}
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_DailyRewardsProcess
	// -----------------------------------------------------------------------------------
	[ServerCallback]
	private void UMO3d_DailyRewardsProcess() {
		Target_UMO3d_ShowDailyRewardsUI(connectionToClient, dailyRewardCounter);
	}

	// -----------------------------------------------------------------------------------
	// Target_UMO3d_ShowDailyRewardsUI
	// -----------------------------------------------------------------------------------
	[TargetRpc(channel=Channels.DefaultUnreliable)] // only send to one client
    private void Target_UMO3d_ShowDailyRewardsUI(NetworkConnection target, int dailyRewardCounter) {
        FindObjectOfType<UISimpleDailyRewards>().Show(dailyRewardCounter);
    }

}

#endif

// =======================================================================================