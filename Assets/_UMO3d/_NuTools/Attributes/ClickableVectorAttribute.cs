﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class ClickableVectorAttribute : PropertyAttribute
{

}