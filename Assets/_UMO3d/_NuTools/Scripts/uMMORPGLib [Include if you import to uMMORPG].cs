﻿//FUNBYTEZ uMMORPG Script Define v1.00

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class uMMORPGLib
{
    const string define = "UMMORPG";

	static uMMORPGLib()
    { AddLibrayDefineIfNeeded(); }

    static void AddLibrayDefineIfNeeded()
    {
        // Get defines.
        BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
		string definestring = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

		// Append only if not defined already.
		string[] defines = definestring.Split (';');
		foreach (string def in defines) {
			if (def == define)
				return;
		}

		// Append.
		PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, (definestring + ";" + define));
        Debug.LogWarning("<b>" + define + "</b> added to <i>Scripting Define Symbols</i> for selected build target (" + EditorUserBuildSettings.activeBuildTarget.ToString() + ").");
    }
}

#endif