﻿[System.Serializable]
public class NamedBool
{
    public string name;

    public bool value;

    public NamedBool(string name, bool value)
    {
        this.name = name;
        this.value = value;
    }

    public NamedBool()
    {
        new NamedBool("", true);
    }
}