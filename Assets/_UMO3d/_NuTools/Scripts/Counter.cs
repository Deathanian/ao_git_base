﻿public struct Counter
{
    public int stepsBeforeReset;

    private int steps;

    bool autoReset;

    public Counter(int stepsBeforeReady = 10, bool autoReset = true)
    {
        stepsBeforeReset = stepsBeforeReady;
		steps = 0;
        this.autoReset = autoReset;
    }

    public int stepsLeft
    {
        get { return stepsBeforeReset - steps; }
    }

    public int stepsDone
    {
        get { return steps; }
    }

    public bool ready
    {
        get
        {
            steps++;
            if(steps >= stepsBeforeReset)
            {
                if (autoReset)
                    Reset();
                return true;
            }
            return false;
        }
    }

    public void Reset()
    {
        steps = 0;
    }

    public float progress
    {
        get
        {
            return (float)steps / stepsBeforeReset;
        }
    }
}