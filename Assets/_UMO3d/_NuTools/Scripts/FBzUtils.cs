﻿//FUNBYTEZ FBzUtils v1.01
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FBzUtils {
	// -----------------------------------------------------------------------------------
	// GetTextMeshWidth (should be moved to some kind of utils)
	// by Stuepfnick
	// Calculates the width of a TextMesh, with each characters width, considering Fontsize and Styling.
	// -----------------------------------------------------------------------------------
	public static bool fakeMsgsFinished;

	public static float GetTextMeshWidth(TextMesh mesh, string txt)
	{
		float width = 0;
		foreach (char symbol in mesh.text)
		{
			CharacterInfo info;
			if (mesh.font.GetCharacterInfo(symbol, out info, mesh.fontSize, mesh.fontStyle))
			{
				width += info.advance;
			}
		}
		return width * mesh.characterSize * 0.1f;
	}
}
