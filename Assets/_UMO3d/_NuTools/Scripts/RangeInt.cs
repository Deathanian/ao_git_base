﻿using UnityEngine;

[System.Serializable]
public struct RangeInt
{
	public RangeInt(int minValue, int maxValue) {
		min = minValue;
		max = maxValue;
	}

    public int min;

    public int max;

    public int random
    {
        get
        {
            return Random.Range(min, max);
        }
    }
}