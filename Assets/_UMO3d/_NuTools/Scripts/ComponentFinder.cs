﻿using UnityEngine;

public class ComponentFinder<T> where T : Component
{
    private MonoBehaviour b;

    private T _val;

    public T val
    {
        get
        {
            return _val ?? (_val = b.GetComponent<T>());
        }
    }

    public ComponentFinder(MonoBehaviour b)
    {
        this.b = b;
    }

    public static implicit operator T(ComponentFinder<T> obj)
    {
        return obj.val;
    }
}