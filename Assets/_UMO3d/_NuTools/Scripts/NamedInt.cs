﻿[System.Serializable]
public class NamedInt
{
    public string name;

    public int value;

    public NamedInt(string name = "", int value = 0)
    {
        this.name = name;
        this.value = value;
    }

    public NamedInt()
    {
        name = "";
        value = 0;
    }
}