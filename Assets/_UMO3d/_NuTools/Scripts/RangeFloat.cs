﻿using UnityEngine;

[System.Serializable]
public struct RangeFloat
{
	public RangeFloat(float minValue, float maxValue) {
		min = minValue;
		max = maxValue;
	}
	
    public float min;

    public float max;

    public float random
	{
        get
		{
            return Random.Range(min, max);
        }
    }
}