﻿using UnityEngine;

[System.Serializable]
public class NamedEulerAngle
{
    public string name;

    public Vector3 angles;
}