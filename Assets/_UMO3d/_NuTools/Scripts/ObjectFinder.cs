﻿using UnityEngine;

public class ObjectFinder<T> where T : Component
{
    private T _val;

    public T val
    {
        get
        {
            return _val ?? (_val = Object.FindObjectOfType<T>());
        }
    }

    public static implicit operator T(ObjectFinder<T> obj)
    {
        return obj.val;
    }
}