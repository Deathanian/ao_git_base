﻿// =======================================================================================
// SIMPLE EQUIPMENT SETS - ITEM
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

// =======================================================================================
// ITEM
// =======================================================================================
public partial struct Item {

	// -----------------------------------------------------------------------------------
	// UMO3d_updateIndividualSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_updateIndividualSetBonus(SyncListItem equipment) {
		var isValid = false;
		var counter = 0;
		
    	for (int j = 0; j < template.setItems.Length; ++j) {
    		if (template.setItems[j] != null ) {
    			for (int i = 0; i < equipment.Count; ++i) {
					if (equipment[i].valid) {
 						if (equipment[i].template.name == template.setItems[j].name) {
 							counter++;
 							break;
 						}
    				}
    			}
    		}
    	}
    	
    	if (counter >= template.setItems.Length) {
			isValid = true;
		}

		return isValid;
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_updatePartialSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_updatePartialSetBonus(SyncListItem equipment) {
		var isValid = false;
		var counter = 0;
		
		if (template.equipmentSet != null) {
		
			for (int j = 0; j < template.equipmentSet.setItems.Length; ++j) {
				if (template.equipmentSet.setItems[j] != null ) {
					for (int i = 0; i < equipment.Count; ++i) {
						if (equipment[i].valid) {
							if (equipment[i].template.name == template.equipmentSet.setItems[j].name) {
								counter++;
								//break;
							}
						}
					}
				}
    		}
    		
    		Debug.Log("partial check: "+counter + " of " +template.equipmentSet.partialSetItemsCount);

			if (counter >= template.equipmentSet.partialSetItemsCount) {
				isValid = true;
			}
		
		}
		
		return isValid;
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_updateCompleteSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_updateCompleteSetBonus(SyncListItem equipment) {
		var isValid = false;
		var counter = 0;
		
		if (template.equipmentSet != null) {
		
			for (int j = 0; j < template.equipmentSet.setItems.Length; ++j) {
				if (template.equipmentSet.setItems[j] != null ) {
					for (int i = 0; i < equipment.Count; ++i) {
						if (equipment[i].valid) {
							if (equipment[i].template.name == template.equipmentSet.setItems[j].name) {
								counter++;
								//break;
							}
						}
					}
				}
			}
			
			if (counter >= template.equipmentSet.setItems.Length) {
				isValid = true;
			}
		
		}
		
		return isValid;
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_hasIndividualSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_hasIndividualSetBonus() {
		
		var success = false;
		
		if (template.category.StartsWith("Equipment") && template.setItems.Length > 0) {
#if _NuCore
			success = template.setBlockEffectBonus 		!= 0 ? true : success;
			success = template.setCriticalEffectBonus 	!= 0 ? true : success;

			success = template.setHealthBonus 			!= 0 ? true : success;
			success = template.setManaBonus 			!= 0 ? true : success;
			success = template.setDamageBonus 			!= 0 ? true : success;
			success = template.setDefenseBonus 			!= 0 ? true : success;
			success = template.setBlockChanceBonus 		!= 0 ? true : success;
			success = template.setCriticalChanceBonus 	!= 0 ? true : success;
#endif
#if _FreeItemStats
			success = template.setHealthRecoveryBonus 	!= 0 ? true : success;
			success = template.setManaRecoveryBonus 	!= 0 ? true : success;
			success = template.setSpeedBonus 			!= 0 ? true : success;
			success = template.setStrengthBonus 		!= 0 ? true : success;
			success = template.setIntelligenceBonus 	!= 0 ? true : success;
#endif	
#if _SimpleAttributes
			success = template.setAttribute1Bonus 		!= 0 ? true : success;
			success = template.setAttribute2Bonus 		!= 0 ? true : success;
			success = template.setAttribute3Bonus	 	!= 0 ? true : success;
			success = template.setAttribute4Bonus		!= 0 ? true : success;
			success = template.setAttribute5Bonus		!= 0 ? true : success;
			success = template.setAttribute6Bonus		!= 0 ? true : success;
#if _SimpleStamina
			success = template.setAttribute9Bonus 		!= 0 ? true : success;
#endif
#endif

#if _SimpleBoosters
			success = template.setGoldBonus 			!= 0 ? true : success;
			success = template.setExpBonus 				!= 0 ? true : success;
			success = template.setCoinsBonus 			!= 0 ? true : success;
#endif
#if _SimpleResistances
			success = template.setResFire 				!= 0 ? true : success;
			success = template.setResWater 				!= 0 ? true : success;
			success = template.setResEarth 				!= 0 ? true : success;
			success = template.setResAir 				!= 0 ? true : success;
			success = template.setResMagic 				!= 0 ? true : success;
			success = template.setResLight 				!= 0 ? true : success;
			success = template.setResDark 				!= 0 ? true : success;
			success = template.setResSlashing 			!= 0 ? true : success;
			success = template.setResCrushing 			!= 0 ? true : success;
			success = template.setResPiercing 			!= 0 ? true : success;
#endif
		}

		return success;
		
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_hasPartialSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_hasPartialSetBonus() {
	
		var success = false;
		
		if (template.category.StartsWith("Equipment") && template.equipmentSet != null) {
		
#if _NuCore
			success = template.equipmentSet.setPartialBlockEffectBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setPartialCriticalEffectBonus 	!= 0 ? true : success;

			success = template.equipmentSet.setPartialHealthBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialManaBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialDamageBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialDefenseBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialBlockChanceBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setPartialCriticalChanceBonus 	!= 0 ? true : success;
#endif
#if _FreeItemStats
			success = template.equipmentSet.setPartialHealthRecoveryBonus 	!= 0 ? true : success;
			success = template.equipmentSet.setPartialManaRecoveryBonus 	!= 0 ? true : success;
			success = template.equipmentSet.setPartialSpeedBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialStrengthBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setPartialIntelligenceBonus 	!= 0 ? true : success;
#endif	
#if _SimpleAttributes
			success = template.equipmentSet.setPartialAttribute1Bonus 		!= 0 ? true : success;
			success = template.equipmentSet.setPartialAttribute2Bonus 		!= 0 ? true : success;
			success = template.equipmentSet.setPartialAttribute3Bonus	 	!= 0 ? true : success;
			success = template.equipmentSet.setPartialAttribute4Bonus		!= 0 ? true : success;
			success = template.equipmentSet.setPartialAttribute5Bonus		!= 0 ? true : success;
			success = template.equipmentSet.setPartialAttribute6Bonus		!= 0 ? true : success;
#if _SimpleStamina
			success = template.equipmentSet.setPartialAttribute9Bonus 		!= 0 ? true : success;
#endif
#endif

#if _SimpleBoosters
			success = template.equipmentSet.setPartialGoldBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialExpBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialCoinsBonus 			!= 0 ? true : success;
#endif
#if _SimpleResistances
			success = template.equipmentSet.setPartialResFire 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResWater 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResEarth 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResAir 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResMagic 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResLight 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResDark 				!= 0 ? true : success;
			success = template.equipmentSet.setPartialResSlashing 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialResCrushing 			!= 0 ? true : success;
			success = template.equipmentSet.setPartialResPiercing 			!= 0 ? true : success;
#endif		

		}
		
		return success;
	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_hasCompleteSetBonus
	// -----------------------------------------------------------------------------------
	public bool UMO3d_hasCompleteSetBonus() {
		var success = false;
		
		if (template.category.StartsWith("Equipment") && template.equipmentSet != null) {
		
#if _NuCore
			success = template.equipmentSet.setCompleteBlockEffectBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setCompleteCriticalEffectBonus 		!= 0 ? true : success;

			success = template.equipmentSet.setCompleteHealthBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteManaBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteDamageBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteDefenseBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteBlockChanceBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setCompleteCriticalChanceBonus 		!= 0 ? true : success;
#endif
#if _FreeItemStats
			success = template.equipmentSet.setCompleteHealthRecoveryBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setCompleteManaRecoveryBonus 		!= 0 ? true : success;
			success = template.equipmentSet.setCompleteSpeedBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteStrengthBonus 			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteIntelligenceBonus 		!= 0 ? true : success;
#endif	
#if _SimpleAttributes
			success = template.equipmentSet.setCompleteAttribute1Bonus 			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteAttribute2Bonus 			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteAttribute3Bonus	 		!= 0 ? true : success;
			success = template.equipmentSet.setCompleteAttribute4Bonus			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteAttribute5Bonus			!= 0 ? true : success;
			success = template.equipmentSet.setCompleteAttribute6Bonus			!= 0 ? true : success;
#if _SimpleStamina
			success = template.equipmentSet.setCompleteAttribute9Bonus 			!= 0 ? true : success;
#endif
#endif

#if _SimpleBoosters
			success = template.equipmentSet.setCompleteGoldBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteExpBonus 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteCoinsBonus 				!= 0 ? true : success;
#endif
#if _SimpleResistances
			success = template.equipmentSet.setCompleteResFire 					!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResWater 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResEarth 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResAir 					!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResMagic 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResLight 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResDark 					!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResSlashing 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResCrushing 				!= 0 ? true : success;
			success = template.equipmentSet.setCompleteResPiercing 				!= 0 ? true : success;
#endif		
		
		}
		
		return success;
	}

	// -----------------------------------------------------------------------------------
	// GETTERS
	// -----------------------------------------------------------------------------------
#if _NuCore
	public float 	setBlockEffectBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setBlockEffectBonus : 0; 	} }
	public float 	setCriticalEffectBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setCriticalEffectBonus : 0; } }
#endif
	public int 		setHealthBonus 				{ get { return UMO3d_SetBonusIndividualValid ? template.setHealthBonus : 0; } }
	public int 		setManaBonus 				{ get { return UMO3d_SetBonusIndividualValid ? template.setManaBonus : 0; } }
	public int 		setDamageBonus 				{ get { return UMO3d_SetBonusIndividualValid ? template.setDamageBonus : 0; } }
	public int 		setDefenseBonus 			{ get { return UMO3d_SetBonusIndividualValid ? template.setDefenseBonus : 0; } }
	public float 	setBlockChanceBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setBlockChanceBonus : 0; } }
	public float 	setCriticalChanceBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setCriticalChanceBonus : 0; } }
#if _FreeItemStats
   	public int 		setHealthRecoveryBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setHealthRecoveryBonus : 0; } }
    public int 		setManaRecoveryBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setManaRecoveryBonus : 0; } }
    public float 	setSpeedBonus 				{ get { return UMO3d_SetBonusIndividualValid ? template.setSpeedBonus : 0; } }
	public int 		setStrengthBonus 			{ get { return UMO3d_SetBonusIndividualValid ? template.setStrengthBonus : 0; } }
	public int 		setIntelligenceBonus 		{ get { return UMO3d_SetBonusIndividualValid ? template.setIntelligenceBonus : 0; } }
#endif
#if _SimpleAttributes
	public int setAttribute1Bonus 				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute1Bonus : 0; } }
	public int setAttribute2Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute2Bonus : 0; } }
	public int setAttribute3Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute3Bonus : 0; } }
	public int setAttribute4Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute4Bonus : 0; } }
	public int setAttribute5Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute5Bonus : 0; } }
	public int setAttribute6Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute6Bonus : 0; } }
	//--7
	//--8
#if _SimpleStamina
	public int setAttribute9Bonus  				{ get { return UMO3d_SetBonusIndividualValid ? template.setAttribute9Bonus : 0; } }
#endif
#endif
#if _SimpleBoosters
	public float setGoldBonus   				{ get { return UMO3d_SetBonusIndividualValid ? template.setGoldBonus : 0; } }
	public float setExpBonus   					{ get { return UMO3d_SetBonusIndividualValid ? template.setExpBonus : 0; } }
	public float setCoinsBonus   				{ get { return UMO3d_SetBonusIndividualValid ? template.setCoinsBonus : 0; } }
#endif
#if _SimpleResistances
	public float setResFire   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResFire : 0; } }
	public float setResWater   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResWater : 0; } }
	public float setResEarth   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResEarth : 0; } }
	public float setResAir   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResAir : 0; } }
	public float setResMagic   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResMagic : 0; } }
	public float setResLight   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResLight : 0; } }
	public float setResDark   					{ get { return UMO3d_SetBonusIndividualValid ? template.setResDark : 0; } }
	public float setResSlashing   				{ get { return UMO3d_SetBonusIndividualValid ? template.setResSlashing : 0; } }
	public float setResCrushing   				{ get { return UMO3d_SetBonusIndividualValid ? template.setResCrushing : 0; } }
	public float setResPiercing   				{ get { return UMO3d_SetBonusIndividualValid ? template.setResPiercing : 0; } }
#endif

#if _NuCore
	public float 	setPartialBlockEffectBonus 			{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialBlockEffectBonus : 0; 	} }
	public float 	setPartialCriticalEffectBonus 		{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialCriticalEffectBonus : 0; } }
#endif
	public int 		setPartialHealthBonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialHealthBonus : 0; } }
	public int 		setPartialManaBonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialManaBonus : 0; } }
	public int 		setPartialDamageBonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialDamageBonus : 0; } }
	public int 		setPartialDefenseBonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialDefenseBonus : 0; } }
	public float 	setPartialBlockChanceBonus 			{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialBlockChanceBonus : 0; } }
	public float 	setPartialCriticalChanceBonus 		{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialCriticalChanceBonus : 0; } }
#if _FreeItemStats
   	public int 		setPartialHealthRecoveryBonus 		{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialHealthRecoveryBonus : 0; } }
    public int 		setPartialManaRecoveryBonus 		{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialManaRecoveryBonus : 0; } }
    public float 	setPartialSpeedBonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialSpeedBonus : 0; } }
	public int 		setPartialStrengthBonus 			{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialStrengthBonus : 0; } }
	public int 		setPartialIntelligenceBonus 		{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialIntelligenceBonus : 0; } }
#endif
#if _SimpleAttributes
	public int setPartialAttribute1Bonus 				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute1Bonus : 0; } }
	public int setPartialAttribute2Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute2Bonus : 0; } }
	public int setPartialAttribute3Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute3Bonus : 0; } }
	public int setPartialAttribute4Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute4Bonus : 0; } }
	public int setPartialAttribute5Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute5Bonus : 0; } }
	public int setPartialAttribute6Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute6Bonus : 0; } }
	//--7
	//--8
#if _SimpleStamina
	public int setPartialAttribute9Bonus  				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialAttribute9Bonus : 0; } }
#endif
#endif

#if _SimpleBoosters
	public float setPartialGoldBonus   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialGoldBonus : 0; } }
	public float setPartialExpBonus   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialExpBonus : 0; } }
	public float setPartialCoinsBonus   				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialCoinsBonus : 0; } }
#endif
#if _SimpleResistances
	public float setPartialResFire   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResFire : 0; } }
	public float setPartialResWater   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResWater : 0; } }
	public float setPartialResEarth   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResEarth : 0; } }
	public float setPartialResAir   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResAir : 0; } }
	public float setPartialResMagic   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResMagic : 0; } }
	public float setPartialResLight   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResLight : 0; } }
	public float setPartialResDark   					{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResDark : 0; } }
	public float setPartialResSlashing   				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResSlashing : 0; } }
	public float setPartialResCrushing   				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResCrushing : 0; } }
	public float setPartialResPiercing   				{ get { return UMO3d_SetBonusPartialValid ? template.equipmentSet.setPartialResPiercing : 0; } }
#endif

#if _NuCore
	public float 	setCompleteBlockEffectBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteBlockEffectBonus : 0; 	} }
	public float 	setCompleteCriticalEffectBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteCriticalEffectBonus : 0; } }
#endif
	public int 		setCompleteHealthBonus 				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteHealthBonus : 0; } }
	public int 		setCompleteManaBonus 				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteManaBonus : 0; } }
	public int 		setCompleteDamageBonus 				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteDamageBonus : 0; } }
	public int 		setCompleteDefenseBonus 			{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteDefenseBonus : 0; } }
	public float 	setCompleteBlockChanceBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteBlockChanceBonus : 0; } }
	public float 	setCompleteCriticalChanceBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteCriticalChanceBonus : 0; } }
#if _FreeItemStats
   	public int 		setCompleteHealthRecoveryBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteHealthRecoveryBonus : 0; } }
    public int 		setCompleteManaRecoveryBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteManaRecoveryBonus : 0; } }
    public float 	setCompleteSpeedBonus 				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteSpeedBonus : 0; } }
	public int 		setCompleteStrengthBonus 			{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteStrengthBonus : 0; } }
	public int 		setCompleteIntelligenceBonus 		{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteIntelligenceBonus : 0; } }
#endif
#if _SimpleAttributes
	public int setCompleteAttribute1Bonus 				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute1Bonus : 0; } }
	public int setCompleteAttribute2Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute2Bonus : 0; } }
	public int setCompleteAttribute3Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute3Bonus : 0; } }
	public int setCompleteAttribute4Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute4Bonus : 0; } }
	public int setCompleteAttribute5Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute5Bonus : 0; } }
	public int setCompleteAttribute6Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute6Bonus : 0; } }
	//--7
	//--8
#if _SimpleStamina
	public int setCompleteAttribute9Bonus  				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteAttribute9Bonus : 0; } }
#endif
#endif

#if _SimpleBoosters
	public float setCompleteGoldBonus   				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteGoldBonus : 0; } }
	public float setCompleteExpBonus   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteExpBonus : 0; } }
	public float setCompleteCoinsBonus   				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteCoinsBonus : 0; } }
#endif
#if _SimpleResistances
	public float setCompleteResFire   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResFire : 0; } }
	public float setCompleteResWater   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResWater : 0; } }
	public float setCompleteResEarth   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResEarth : 0; } }
	public float setCompleteResAir   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResAir : 0; } }
	public float setCompleteResMagic   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResMagic : 0; } }
	public float setCompleteResLight   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResLight : 0; } }
	public float setCompleteResDark   					{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResDark : 0; } }
	public float setCompleteResSlashing   				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResSlashing : 0; } }
	public float setCompleteResCrushing   				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResCrushing : 0; } }
	public float setCompleteResPiercing   				{ get { return UMO3d_SetBonusCompleteValid ? template.equipmentSet.setCompleteResPiercing : 0; } }
#endif


	// -----------------------------------------------------------------------------------
	// TOOLTIP
	// -----------------------------------------------------------------------------------
	public void ToolTip_UMO3d_SimpleEquipmentSets(StringBuilder tip) {
	
		// --------------------------------------------------------------------------- Equipment Set Name
		if (template.equipmentSet != null ) {
        	tip.Append("\n");
        	tip.Append("<b>" + template.equipmentSet.name + "</b>\n");
        }
		
       	// --------------------------------------------------------------------------- Individual Set Bonus
        if (UMO3d_hasIndividualSetBonus()) {
        	tip.Append("\n");
        	tip.Append("<b>" + Constants.UMO3d_TOOLTIP_SET_INDIVIDUAL + "</b>\n");
        	tip.Append(template.setItems.Length + " / " + template.setItems.Length + " Item(s) equipped:\n");
        	for (int j = 0; j < template.setItems.Length; ++j) {
        		if (template.setItems[j] != null)		tip.Append("* " 									+ template.setItems[j].name + "\n");
        	}
        	
        	if (UMO3d_SetBonusIndividualValid) {
        		tip.Append("<color='green'>");
        	} else {
        		tip.Append("<color='red'>");
        	}
        	
#if _NuCore
        	if (template.setBlockEffectBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.setBlockEffectBonus * 100).ToString() 	+ "%\n");
        	if (template.setCriticalEffectBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.setCriticalEffectBonus * 100).ToString() 	+ "%\n");

        	if (template.setHealthBonus != 0)			tip.Append(Constants.UMO3d_STAT_HEALTH 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setHealthBonus.ToString() + "\n");
        	if (template.setManaBonus != 0)				tip.Append(Constants.UMO3d_STAT_MANA 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setManaBonus.ToString() + "\n");
        	if (template.setDamageBonus != 0)			tip.Append(Constants.UMO3d_STAT_DAMAGE 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setDamageBonus.ToString() + "\n");
        	if (template.setDefenseBonus != 0)			tip.Append(Constants.UMO3d_STAT_DEFENSE 			+ Constants.UMO3d_TOOLTIP_BONUS + template.setDefenseBonus.ToString() + "\n");
        	if (template.setBlockChanceBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.setBlockChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.setCriticalChanceBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.setCriticalChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.setHealthRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_HEALTH_REC 			+ Constants.UMO3d_TOOLTIP_BONUS + template.setHealthRecoveryBonus.ToString() + "\n");
        	if (template.setManaRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_MANA_REC 			+ Constants.UMO3d_TOOLTIP_BONUS + template.setManaRecoveryBonus.ToString() + "\n");
        	if (template.setSpeedBonus != 0) 			tip.Append(Constants.UMO3d_STAT_SPEED				+ Constants.UMO3d_TOOLTIP_BONUS + template.setSpeedBonus.ToString() + "\n");
        	if (template.setStrengthBonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_STR 				+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.setStrengthBonus.ToString() + "\n");
        	if (template.setIntelligenceBonus != 0) 	tip.Append(Constants.UMO3d_ATTRIB_INT 				+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.setIntelligenceBonus.ToString() + "\n");
#endif
#if _SimpleAttributes
        	if (template.setAttribute1Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR1 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute1Bonus.ToString() + "\n");
        	if (template.setAttribute2Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR2 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute2Bonus.ToString() + "\n");
        	if (template.setAttribute3Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR3 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute3Bonus.ToString() + "\n");
        	if (template.setAttribute4Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR4 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute4Bonus.ToString() + "\n");
        	if (template.setAttribute5Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR5 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute5Bonus.ToString() + "\n");
        	if (template.setAttribute6Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR6 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute6Bonus.ToString() + "\n");
			//--7
			//--8
#if _SimpleStamina
			if (template.setAttribute9Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR9 				+ Constants.UMO3d_TOOLTIP_BONUS + template.setAttribute9Bonus.ToString() + "\n");
#endif
#endif
#if _SimpleBoosters
			if (template.setGoldBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_GOLD 		+ Mathf.RoundToInt(template.setGoldBonus * 100).ToString() 		+ "%\n");
			if (template.setExpBonus != 0) 				tip.Append(Constants.UMO3d_TOOLTIP_BOOST_EXP 		+ Mathf.RoundToInt(template.setExpBonus * 100).ToString() 		+ "%\n");
			if (template.setCoinsBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_COINS 		+ Mathf.RoundToInt(template.setCoinsBonus * 100).ToString() 	+ "%\n");
#endif
#if _SimpleResistances
			if (template.setResFire != 0) 				tip.Append( ((Constants.Elements)1).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResFire * 100).ToString() 	+ "%\n");
			if (template.setResWater != 0) 				tip.Append( ((Constants.Elements)2).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResWater * 100).ToString() 	+ "%\n");
			if (template.setResEarth != 0) 				tip.Append( ((Constants.Elements)3).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResEarth * 100).ToString() 	+ "%\n");
			if (template.setResAir != 0) 				tip.Append( ((Constants.Elements)4).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResAir * 100).ToString() 	+ "%\n");
			if (template.setResMagic != 0) 				tip.Append( ((Constants.Elements)5).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResMagic * 100).ToString() 	+ "%\n");
			if (template.setResLight != 0) 				tip.Append( ((Constants.Elements)6).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResLight * 100).ToString() 	+ "%\n");
			if (template.setResDark != 0) 				tip.Append( ((Constants.Elements)7).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResDark * 100).ToString() 	+ "%\n");
			if (template.setResSlashing != 0) 			tip.Append( ((Constants.Elements)8).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResSlashing * 100).ToString() 	+ "%\n");
			if (template.setResCrushing != 0) 			tip.Append( ((Constants.Elements)9).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResCrushing * 100).ToString() 	+ "%\n");
			if (template.setResPiercing != 0) 			tip.Append( ((Constants.Elements)10).ToString() 	+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.setResPiercing * 100).ToString() 	+ "%\n");
#endif
			tip.Append("</color>");
        }
        
        // --------------------------------------------------------------------------- Partial Set Bonus
 if (UMO3d_hasPartialSetBonus()) {
        	tip.Append("\n");
        	tip.Append("<b>" + Constants.UMO3d_TOOLTIP_SET_PARTIAL + "</b>\n");
        	tip.Append(template.equipmentSet.partialSetItemsCount + " / " + template.equipmentSet.setItems.Length + " Item(s) equipped.\n");
        	        	
        	if (UMO3d_SetBonusPartialValid) {
        		tip.Append("<color='green'>");
        	} else {
        		tip.Append("<color='red'>");
        	}
        	
#if _NuCore
        	if (template.equipmentSet.setPartialBlockEffectBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setPartialBlockEffectBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setPartialCriticalEffectBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setPartialCriticalEffectBonus * 100).ToString() 	+ "%\n");

        	if (template.equipmentSet.setPartialHealthBonus != 0)			tip.Append(Constants.UMO3d_STAT_HEALTH 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialHealthBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialManaBonus != 0)				tip.Append(Constants.UMO3d_STAT_MANA 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialManaBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialDamageBonus != 0)			tip.Append(Constants.UMO3d_STAT_DAMAGE 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialDamageBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialDefenseBonus != 0)			tip.Append(Constants.UMO3d_STAT_DEFENSE 			+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialDefenseBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialBlockChanceBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setPartialBlockChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setPartialCriticalChanceBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setPartialCriticalChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setPartialHealthRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_HEALTH_REC 			+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialHealthRecoveryBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialManaRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_MANA_REC 		+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialManaRecoveryBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialSpeedBonus != 0) 			tip.Append(Constants.UMO3d_STAT_SPEED				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialSpeedBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialStrengthBonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_STR 			+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialStrengthBonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialIntelligenceBonus != 0) 	tip.Append(Constants.UMO3d_ATTRIB_INT 			+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialIntelligenceBonus.ToString() + "\n");
#endif
#if _SimpleAttributes
        	if (template.equipmentSet.setPartialAttribute1Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR1 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute1Bonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialAttribute2Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR2 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute2Bonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialAttribute3Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR3 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute3Bonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialAttribute4Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR4 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute4Bonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialAttribute5Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR5 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute5Bonus.ToString() + "\n");
        	if (template.equipmentSet.setPartialAttribute6Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR6 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute6Bonus.ToString() + "\n");
			//--7
			//--8
#if _SimpleStamina
			if (template.equipmentSet.setPartialAttribute9Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR9 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setPartialAttribute9Bonus.ToString() + "\n");
#endif
#endif
#if _SimpleBoosters
			if (template.equipmentSet.setPartialGoldBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_GOLD 	+ Mathf.RoundToInt(template.equipmentSet.setPartialGoldBonus * 100).ToString() 		+ "%\n");
			if (template.equipmentSet.setPartialExpBonus != 0) 				tip.Append(Constants.UMO3d_TOOLTIP_BOOST_EXP 		+ Mathf.RoundToInt(template.equipmentSet.setPartialExpBonus * 100).ToString() 		+ "%\n");
			if (template.equipmentSet.setPartialCoinsBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_COINS 		+ Mathf.RoundToInt(template.equipmentSet.setPartialCoinsBonus * 100).ToString() 	+ "%\n");
#endif
#if _SimpleResistances
			if (template.equipmentSet.setPartialResFire != 0) 				tip.Append( ((Constants.Elements)1).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResFire * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResWater != 0) 				tip.Append( ((Constants.Elements)2).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResWater * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResEarth != 0) 				tip.Append( ((Constants.Elements)3).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResEarth * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResAir != 0) 				tip.Append( ((Constants.Elements)4).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResAir * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResMagic != 0) 				tip.Append( ((Constants.Elements)5).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResMagic * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResLight != 0) 				tip.Append( ((Constants.Elements)6).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResLight * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResDark != 0) 				tip.Append( ((Constants.Elements)7).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResDark * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResSlashing != 0) 			tip.Append( ((Constants.Elements)8).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResSlashing * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResCrushing != 0) 			tip.Append( ((Constants.Elements)9).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResCrushing * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setPartialResPiercing != 0) 			tip.Append( ((Constants.Elements)10).ToString() 	+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setPartialResPiercing * 100).ToString() 	+ "%\n");
#endif
			tip.Append("</color>");
        }       
        
        // --------------------------------------------------------------------------- Complete Set Bonus
  if (UMO3d_hasCompleteSetBonus()) {
        	tip.Append("\n");
        	tip.Append("<b>" + Constants.UMO3d_TOOLTIP_SET_COMPLETE + "</b>\n");
        	tip.Append(template.equipmentSet.setItems.Length + " / " + template.equipmentSet.setItems.Length + " Item(s) equipped:.\n");
        	for (int j = 0; j < template.equipmentSet.setItems.Length; ++j) {
        		if (template.equipmentSet.setItems[j] != null)		tip.Append("* " 									+ template.equipmentSet.setItems[j].name + "\n");
        	}
        	
        	if (UMO3d_SetBonusCompleteValid) {
        		tip.Append("<color='green'>");
        	} else {
        		tip.Append("<color='red'>");
        	}
        	
#if _NuCore
        	if (template.equipmentSet.setCompleteBlockEffectBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setCompleteBlockEffectBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setCompleteCriticalEffectBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_EFFECT 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setCompleteCriticalEffectBonus * 100).ToString() 	+ "%\n");

        	if (template.equipmentSet.setCompleteHealthBonus != 0)			tip.Append(Constants.UMO3d_STAT_HEALTH 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteHealthBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteManaBonus != 0)			tip.Append(Constants.UMO3d_STAT_MANA 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteManaBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteDamageBonus != 0)			tip.Append(Constants.UMO3d_STAT_DAMAGE 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteDamageBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteDefenseBonus != 0)			tip.Append(Constants.UMO3d_STAT_DEFENSE 			+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteDefenseBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteBlockChanceBonus != 0)		tip.Append(Constants.UMO3d_STAT_BLOCK_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setCompleteBlockChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setCompleteCriticalChanceBonus != 0)	tip.Append(Constants.UMO3d_STAT_CRIT_CHANCE 		+ Constants.UMO3d_TOOLTIP_BONUS + Mathf.RoundToInt(template.equipmentSet.setCompleteCriticalChanceBonus * 100).ToString() 	+ "%\n");
        	if (template.equipmentSet.setCompleteHealthRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_HEALTH_REC 			+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteHealthRecoveryBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteManaRecoveryBonus != 0) 	tip.Append(Constants.UMO3d_STAT_MANA_REC 			+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteManaRecoveryBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteSpeedBonus != 0) 			tip.Append(Constants.UMO3d_STAT_SPEED				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteSpeedBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteStrengthBonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_STR 				+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteStrengthBonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteIntelligenceBonus != 0) 	tip.Append(Constants.UMO3d_ATTRIB_INT 				+ Constants.UMO3d_TOOLTIP_BONUS + Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteIntelligenceBonus.ToString() + "\n");
#endif
#if _SimpleAttributes
        	if (template.equipmentSet.setCompleteAttribute1Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR1 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute1Bonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteAttribute2Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR2 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute2Bonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteAttribute3Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR3 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute3Bonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteAttribute4Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR4 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute4Bonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteAttribute5Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR5 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute5Bonus.ToString() + "\n");
        	if (template.equipmentSet.setCompleteAttribute6Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR6 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute6Bonus.ToString() + "\n");
			//--7
			//--8
#if _SimpleStamina
			if (template.equipmentSet.setCompleteAttribute9Bonus != 0) 		tip.Append(Constants.UMO3d_ATTRIB_ATR9 				+ Constants.UMO3d_TOOLTIP_BONUS + template.equipmentSet.setCompleteAttribute9Bonus.ToString() + "\n");
#endif
#endif
#if _SimpleBoosters
			if (template.equipmentSet.setCompleteGoldBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_GOLD 		+ Mathf.RoundToInt(template.equipmentSet.setCompleteGoldBonus * 100).ToString() 		+ "%\n");
			if (template.equipmentSet.setCompleteExpBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_EXP 		+ Mathf.RoundToInt(template.equipmentSet.setCompleteExpBonus * 100).ToString() 		+ "%\n");
			if (template.equipmentSet.setCompleteCoinsBonus != 0) 			tip.Append(Constants.UMO3d_TOOLTIP_BOOST_COINS 		+ Mathf.RoundToInt(template.equipmentSet.setCompleteCoinsBonus * 100).ToString() 	+ "%\n");
#endif
#if _SimpleResistances
			if (template.equipmentSet.setCompleteResFire != 0) 				tip.Append( ((Constants.Elements)1).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResFire * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResWater != 0) 			tip.Append( ((Constants.Elements)2).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResWater * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResEarth != 0) 			tip.Append( ((Constants.Elements)3).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResEarth * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResAir != 0) 				tip.Append( ((Constants.Elements)4).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResAir * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResMagic != 0) 			tip.Append( ((Constants.Elements)5).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResMagic * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResLight != 0) 			tip.Append( ((Constants.Elements)6).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResLight * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResDark != 0) 				tip.Append( ((Constants.Elements)7).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResDark * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResSlashing != 0) 			tip.Append( ((Constants.Elements)8).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResSlashing * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResCrushing != 0) 			tip.Append( ((Constants.Elements)9).ToString() 		+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResCrushing * 100).ToString() 	+ "%\n");
			if (template.equipmentSet.setCompleteResPiercing != 0) 			tip.Append( ((Constants.Elements)10).ToString() 	+ Constants.UMO3d_TOOLTIP_RESISTANCE + Mathf.RoundToInt(template.equipmentSet.setCompleteResPiercing * 100).ToString() 	+ "%\n");
#endif
			tip.Append("</color>");
        }      
        
    }  

}

// =======================================================================================