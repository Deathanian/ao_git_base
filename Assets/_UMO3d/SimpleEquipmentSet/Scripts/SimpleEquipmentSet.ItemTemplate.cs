﻿// =======================================================================================
// SIMPLE EQUIPMENT SETS - ITEM TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;

// =======================================================================================
// ITEM TEMPLATE
// =======================================================================================
public partial class ItemTemplate : ScriptableObject {
   
	[Header("UMO3d SimpleEquipmentSet - Equipment Set Bonus")]
	
	[Tooltip("Put any SimpleEquipmentSet here to make this item part of a larger set.")]
	public SimpleEquipmentSetTemplate equipmentSet;
	
	[Header("UMO3d SimpleEquipmentSet - Individual Set Bonus")]
	
	[Tooltip("All items must be equipped in order for this individual set bonus to be effective.")]
	public ItemTemplate[] setItems;
	
#if _NuCore
	[Range(-Constants.UMO3d_MODIFIER_BLOCK_MAX,Constants.UMO3d_MODIFIER_BLOCK_MAX)]
	public float setBlockEffectBonus;
	[Range(-Constants.UMO3d_MODIFIER_CRIT_MAX,	Constants.UMO3d_MODIFIER_CRIT_MAX)]
	public float setCriticalEffectBonus;

	public int setHealthBonus;
	public int setManaBonus;
	public int setDamageBonus;
	public int setDefenseBonus;
	[Range(0, 1)] public float setBlockChanceBonus;
	[Range(0, 1)] public float setCriticalChanceBonus;
	
	public int setHealthRecoveryBonus;
    public int setManaRecoveryBonus;
    public float setSpeedBonus;
	
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_STR)]
	public int setStrengthBonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_INT)]
	public int setIntelligenceBonus;
#endif
	
#if _SimpleAttributes
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR1)]
	public int setAttribute1Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR2)]
	public int setAttribute2Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR3)]
	public int setAttribute3Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR4)]
	public int setAttribute4Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR5)]
	public int setAttribute5Bonus;
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR6)]
	public int setAttribute6Bonus;
	
	//--7
	//--8

#if _SimpleStamina
	[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR9)]
	public int setAttribute9Bonus;
#endif

#endif

	
#if _SimpleBoosters
	[Tooltip(Constants.UMO3d_MSG_BOOST_GOLD)]
	[Range(0, 1)] public float setGoldBonus;
	[Tooltip(Constants.UMO3d_MSG_BOOST_EXP)]
	[Range(0, 1)] public float setExpBonus;
	[Tooltip(Constants.UMO3d_MSG_BOOST_COINS)]
	[Range(0, 1)] public float setCoinsBonus;
#endif

#if _SimpleResistances
	[Range(-1,1)] public float setResFire;
   	[Range(-1,1)] public float setResWater;
   	[Range(-1,1)] public float setResEarth;
   	[Range(-1,1)] public float setResAir;
   	[Range(-1,1)] public float setResMagic;
   	[Range(-1,1)] public float setResLight;
   	[Range(-1,1)] public float setResDark;
   	[Range(-1,1)] public float setResSlashing;
   	[Range(-1,1)] public float setResCrushing;
   	[Range(-1,1)] public float setResPiercing;
#endif


	[HideInInspector] public float setPartialResWater;


 
}

// =======================================================================================