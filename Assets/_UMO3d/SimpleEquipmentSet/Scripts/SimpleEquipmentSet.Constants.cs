﻿// =======================================================================================
// SIMPLE EQUIPMENT SET - CONSTANTS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

namespace UMO3d {

	public static partial class Constants {
		
		public const string UMO3d_TOOLTIP_SET_INDIVIDUAL = "Individual Set Bonus";
		public const string UMO3d_TOOLTIP_SET_PARTIAL	 = "Partial Set Bonus";
		public const string UMO3d_TOOLTIP_SET_COMPLETE	 = "Complete Set Bonus";
		
	}

}

// ===================================================================================