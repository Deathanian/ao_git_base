﻿// =======================================================================================
// SIMPLE EQUIPMENT SETS - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	// -----------------------------------------------------------------------------------
	// SwapInventoryEquip_UMO3d_SimpleEquipmentSets
	// -----------------------------------------------------------------------------------
	public void SwapInventoryEquip_UMO3d_SimpleEquipmentSets(int inventoryIndex, int equipIndex) {
		// ---------- Unequip any Item
		UMO3d_RefreshEquipmentSets();
	}
	
	// -----------------------------------------------------------------------------------
	// RefreshLocation_UMO3d_SimpleEquipmentSets
	// -----------------------------------------------------------------------------------
	public void RefreshLocation_UMO3d_SimpleEquipmentSets(int equipIndex, Item item) {
		// ---------- Refresh all equipped Items
		UMO3d_RefreshEquipmentSets();
	}
	
	// -----------------------------------------------------------------------------------
	// RefreshLocation_UMO3d_SimpleEquipmentSets
	// -----------------------------------------------------------------------------------
	private void UMO3d_RefreshEquipmentSets() {
		for (int i = 0; i < equipment.Count; ++i) {
       		if (equipment[i].valid && equipment[i].category.StartsWith("Equipment")) {
             	var tmp_item = new Item(equipment[i].template);
               	tmp_item.UMO3d_SetBonusIndividualValid 	= tmp_item.UMO3d_updateIndividualSetBonus(equipment);
               	tmp_item.UMO3d_SetBonusPartialValid 	= tmp_item.UMO3d_updatePartialSetBonus(equipment);
               	tmp_item.UMO3d_SetBonusCompleteValid 	= tmp_item.UMO3d_updateCompleteSetBonus(equipment);
               	equipment[i] = tmp_item;
    		}
    	}
	}
	
	// ===================================================================================
	// ===================================================================================

#if _NuCore
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusBlockEffect
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusBlockEffect() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setBlockEffectBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialBlockEffectBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteBlockEffectBonus).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}		
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusCriticalEffect
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusCriticalEffect() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setBlockEffectBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialBlockEffectBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteBlockEffectBonus).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}			
#endif
	
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusHealth
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusHealth() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setHealthBonus).Sum();
                                  
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialHealthBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteHealthBonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusMana
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusMana() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setManaBonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialManaBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteManaBonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusDamage
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusDamage() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setDamageBonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialDamageBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteDamageBonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}		
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusDefense
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusDefense() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setDefenseBonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialDefenseBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteDefenseBonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}		

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusBlockChance
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusBlockChance() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setBlockChanceBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialBlockChanceBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteBlockChanceBonus).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}		

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusCriticalChance
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusCriticalChance() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setCriticalChanceBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialCriticalChanceBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteCriticalChanceBonus).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}		


	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusStrength
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusStrength() {
	
		var setBonus = 0;
#if _FreeItemStats		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setStrengthBonus).Sum();
        
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialStrengthBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteStrengthBonus).DefaultIfEmpty(0).Average());
                                  
#endif
		return setBonus;
	
	}		



	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusIntelligence
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusIntelligence() {
	
		var setBonus = 0;
#if _FreeItemStats		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setIntelligenceBonus).Sum();
        
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialIntelligenceBonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteIntelligenceBonus).DefaultIfEmpty(0).Average());
     
#endif          
		return setBonus;
	
	}



	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusGoldBonus
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusGoldBonus() {
	
		float setBonus = 0;
#if _SimpleBoosters		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setGoldBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialGoldBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteGoldBonus).DefaultIfEmpty(0).Average();
#endif                         
		return setBonus;
	
	}			
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusGoldBonus
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusExpBonus() {
	
		float setBonus = 0;
#if _SimpleBoosters	
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setExpBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialExpBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteExpBonus).DefaultIfEmpty(0).Average();
#endif                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusGoldBonus
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusCoinsBonus() {
	
		float setBonus = 0;
#if _SimpleBoosters
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setCoinsBonus).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialCoinsBonus).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteCoinsBonus).DefaultIfEmpty(0).Average();
#endif                              
		return setBonus;
	
	}


#if _SimpleAttributes
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute1
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute1() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute1Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute1Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute1Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute2
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute2() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute2Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute2Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute2Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute3
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute3() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute3Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute3Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute3Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute4
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute4() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute4Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute4Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute4Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute5
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute5() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute5Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute5Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute5Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
	
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute6
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute6() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute6Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute6Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute6Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
	
	// -- Attribute 7
	// -- Attribute 8

#if _SimpleStamina
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusAttribute9
	// -----------------------------------------------------------------------------------
	private int UMO3d_getSetBonusAttribute9() {
	
		var setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setAttribute9Bonus).Sum();
        setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setPartialAttribute9Bonus).DefaultIfEmpty(0).Average());
		setBonus += Convert.ToInt32((from item in equipment
                                  where item.valid
                                  select item.setCompleteAttribute9Bonus).DefaultIfEmpty(0).Average());
                                  
		return setBonus;
	
	}
#endif

#endif

#if _SimpleResistances
	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistFire
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistFire() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResFire).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResFire).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResFire).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistWater
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistWater() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResWater).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResWater).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResWater).DefaultIfEmpty(0).Average();
                      
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistEarth
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistEarth() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResEarth).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResEarth).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResEarth).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistAir
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistAir() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResAir).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResAir).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResAir).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistMagic
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistMagic() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResMagic).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResMagic).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResMagic).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistLight
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistLight() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResLight).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResLight).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResLight).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistDark
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistDark() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResDark).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResDark).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResDark).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistSlashing
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistSlashing() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResSlashing).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResSlashing).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResSlashing).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistCrushing
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistCrushing() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResCrushing).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResCrushing).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResCrushing).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

	// -----------------------------------------------------------------------------------
	// UMO3d_getSetBonusResistPiercing
	// -----------------------------------------------------------------------------------
	private float UMO3d_getSetBonusResistPiercing() {
	
		float setBonus = 0;
		
		setBonus = (from item in equipment
                                  where item.valid
                                  select item.setResPiercing).Sum();
        setBonus += (from item in equipment
                                  where item.valid
                                  select item.setPartialResPiercing).DefaultIfEmpty(0).Average();
		setBonus += (from item in equipment
                                  where item.valid
                                  select item.setCompleteResPiercing).DefaultIfEmpty(0).Average();
                                  
		return setBonus;
	
	}

#endif
	
}

// =======================================================================================