﻿// =======================================================================================
// SIMPLE EQUIPMENT SET - TEMPLATE
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UMO3d {

	// =======================================================================================
	// EQUIPMENT SET TEMPLATE
	// =======================================================================================
	[CreateAssetMenu(fileName="SimpleEquipmentSet", menuName="New SimpleEquipmentSet", order=999)]
	public class SimpleEquipmentSetTemplate : ScriptableObject {
	
		[Header("UMO3d SimpleEquipmentSet")]
		[Tooltip("A number of set items must be equipped for the partial bonus to be active. All set items must be equipped in order for the complete set bonus to be effective.")]
		public ItemTemplate[] setItems;
		
		[Header("UMO3d Partial Set Bonus"),Tooltip("This number of set items must be equipped for the partial bonus to be active.")]
		[Range(0, 99)] public int partialSetItemsCount;
		
#if _NuCore
		[Range(-Constants.UMO3d_MODIFIER_BLOCK_MAX,Constants.UMO3d_MODIFIER_BLOCK_MAX)]
		public float setPartialBlockEffectBonus;
		[Range(-Constants.UMO3d_MODIFIER_CRIT_MAX,	Constants.UMO3d_MODIFIER_CRIT_MAX)]
		public float setPartialCriticalEffectBonus;
#endif
	
		public int setPartialHealthBonus;
		public int setPartialManaBonus;
		public int setPartialDamageBonus;
		public int setPartialDefenseBonus;
		[Range(0, 1)] public float setPartialBlockChanceBonus;
		[Range(0, 1)] public float setPartialCriticalChanceBonus;
	
		public int setPartialHealthRecoveryBonus;
		public int setPartialManaRecoveryBonus;
		public float setPartialSpeedBonus;
	
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_STR)]
		public int setPartialStrengthBonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_INT)]
		public int setPartialIntelligenceBonus;
	
#if _SimpleAttributes
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR1)]
		public int setPartialAttribute1Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR2)]
		public int setPartialAttribute2Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR3)]
		public int setPartialAttribute3Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR4)]
		public int setPartialAttribute4Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR5)]
		public int setPartialAttribute5Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR6)]
		public int setPartialAttribute6Bonus;
	
		//--7
		//--8
#if _SimpleStamina
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR9)]
		public int setPartialAttribute9Bonus;
#endif
#endif

#if _SimpleBoosters
		[Tooltip(Constants.UMO3d_MSG_BOOST_GOLD)]
		[Range(0, 1)] public float setPartialGoldBonus;
		[Tooltip(Constants.UMO3d_MSG_BOOST_EXP)]
		[Range(0, 1)] public float setPartialExpBonus;
		[Tooltip(Constants.UMO3d_MSG_BOOST_COINS)]
		[Range(0, 1)] public float setPartialCoinsBonus;
#endif

#if _SimpleResistances
		[Range(-1,1)] public float setPartialResFire;
		[Range(-1,1)] public float setPartialResWater;
		[Range(-1,1)] public float setPartialResEarth;
		[Range(-1,1)] public float setPartialResAir;
		[Range(-1,1)] public float setPartialResMagic;
		[Range(-1,1)] public float setPartialResLight;
		[Range(-1,1)] public float setPartialResDark;
		[Range(-1,1)] public float setPartialResSlashing;
		[Range(-1,1)] public float setPartialResCrushing;
		[Range(-1,1)] public float setPartialResPiercing;
#endif
 
		[Header("UMO3d Complete Set Bonus"),Tooltip("All set items must be equipped in order for the complete set bonus to be effective.")]
#if _NuCore
		[Range(-Constants.UMO3d_MODIFIER_BLOCK_MAX,Constants.UMO3d_MODIFIER_BLOCK_MAX)]
		public float setCompleteBlockEffectBonus;
		[Range(-Constants.UMO3d_MODIFIER_CRIT_MAX,	Constants.UMO3d_MODIFIER_CRIT_MAX)]
		public float setCompleteCriticalEffectBonus;
#endif
	
		public int setCompleteHealthBonus;
		public int setCompleteManaBonus;
		public int setCompleteDamageBonus;
		public int setCompleteDefenseBonus;
		[Range(0, 1)] public float setCompleteBlockChanceBonus;
		[Range(0, 1)] public float setCompleteCriticalChanceBonus;
	
		public int setCompleteHealthRecoveryBonus;
		public int setCompleteManaRecoveryBonus;
		public float setCompleteSpeedBonus;
	
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_STR)]
		public int setCompleteStrengthBonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_INT)]
		public int setCompleteIntelligenceBonus;
	
#if _SimpleAttributes
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR1)]
		public int setCompleteAttribute1Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR2)]
		public int setCompleteAttribute2Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR3)]
		public int setCompleteAttribute3Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR4)]
		public int setCompleteAttribute4Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR5)]
		public int setCompleteAttribute5Bonus;
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR6)]
		public int setCompleteAttribute6Bonus;
	
		//--7
		//--8
	
#if _SimpleStamina
		[Tooltip("Bonus to " + Constants.UMO3d_ATTRIB_ATR9)]
		public int setCompleteAttribute9Bonus;
#endif
	
#endif

#if _SimpleBoosters
		[Tooltip(Constants.UMO3d_MSG_BOOST_GOLD)]
		[Range(0, 1)] public float setCompleteGoldBonus;
		[Tooltip(Constants.UMO3d_MSG_BOOST_EXP)]
		[Range(0, 1)] public float setCompleteExpBonus;
		[Tooltip(Constants.UMO3d_MSG_BOOST_COINS)]
		[Range(0, 1)] public float setCompleteCoinsBonus;
#endif

#if _SimpleResistances
		[Range(-1,1)] public float setCompleteResFire;
		[Range(-1,1)] public float setCompleteResWater;
		[Range(-1,1)] public float setCompleteResEarth;
		[Range(-1,1)] public float setCompleteResAir;
		[Range(-1,1)] public float setCompleteResMagic;
		[Range(-1,1)] public float setCompleteResLight;
		[Range(-1,1)] public float setCompleteResDark;
		[Range(-1,1)] public float setCompleteResSlashing;
		[Range(-1,1)] public float setCompleteResCrushing;
		[Range(-1,1)] public float setCompleteResPiercing;
#endif
 
 

	}

}

// =======================================================================================