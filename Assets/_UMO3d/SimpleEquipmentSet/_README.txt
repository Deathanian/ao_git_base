﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn requires the NuCore!

First, Install the most recent version of NuCore.

Next, import this AddOn.

Edit the items in your game as you wish, see the new section in the Inspector
"SimpleEquipmentSets". You can also create more complex sets by editing the
"SimpleEquipmentSet" scriptable object in this AddOns Prefabs folder.

Thats it, no further modifications required!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
This AddOn allows you to create Equipment Sets!

* Create Individual, Partial and Complete Set Bonus!
* A Set item requires one or more other Items for its set effect to be active
* All required items must be equipped on the character to be in effect
* Each Set Item can feature a different Individual Set effect
* Set effects stack
* Wearing multiple sets is possible but reduces the set effects to their Average
* Set effects can modify almost all game parameters like health etc.
* Create as many Set items as you like (expands with the number of slots in your game)
* Interlocks to my other AddOns in order to increase amount of available Set Effects.

======================================================================