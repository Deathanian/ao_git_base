﻿// =======================================================================================
// SIMPLE EXPLORATION - AREA
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

#if _SimpleExploration

namespace UMO3d {

	// ===================================================================================
	// SIMPLE EXPLORATION
	// ===================================================================================
	[RequireComponent(typeof(SphereCollider))]
	public class SimpleExploration : NetworkBehaviour {
	
		[Tooltip("One click deactivation")]
		public bool isActive	= true;
		[Tooltip("Always show the area name on enter (even if already explored)")]
		public bool noticeOnEnter		= true;
		[Tooltip("Experience granted on first enter")]
		public long ExpOnExplore = 0;
		[Tooltip("Skill experience granted on first enter")]
		public long SkillExpOnExplore = 0;
		[Tooltip("Gold granted on first enter")]
		public long GoldOnExplore = 0;
		[Tooltip("Coins granted on first enter")]
		public long CoinsOnExplore = 0;
		
		// -------------------------------------------------------------------------------
		// OnTriggerEnter
		// -------------------------------------------------------------------------------
		void OnTriggerEnter(Collider co) {
			var e = co.GetComponentInParent<Player>();
			if (e && isActive) {
				e.UMO3d_mySimpleExploration = this;
				e.UMO3d_ExploreSimpleArea();
			}
		}
		
		// -------------------------------------------------------------------------------
		// OnTriggerExit
		// -------------------------------------------------------------------------------
		void OnTriggerExit(Collider co) {
			var e = co.GetComponentInParent<Player>();
			if (e && isActive) e.UMO3d_mySimpleExploration = null;
		}
		
		// -------------------------------------------------------------------------------
	
	}

}

#endif

// =======================================================================================