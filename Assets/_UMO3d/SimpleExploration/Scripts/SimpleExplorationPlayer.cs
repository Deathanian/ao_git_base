﻿// =======================================================================================
// SIMPLE EXPLORATION - PLAYER
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;

#if _SimpleExploration

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	[HideInInspector] public List<String> UMO3d_exploredAreas = new List<String>();
	[HideInInspector] public SimpleExploration UMO3d_mySimpleExploration;
	
	private const string UMO3d_MSG_EXPLORED_HEADING	= "Area explored:";
	private const string UMO3d_MSG_ENTERED_HEADING	= "Area entered:";
	
	// -----------------------------------------------------------------------------------
	// UMO3d_ExploreSimpleArea
	// -----------------------------------------------------------------------------------
	[ServerCallback]
	public void UMO3d_ExploreSimpleArea() {
		if (UMO3d_mySimpleExploration) {
			// -- explore the area
			if (!UMO3d_exploredAreas.Contains(UMO3d_mySimpleExploration.name)) {
				UMO3d_exploredAreas.Add(UMO3d_mySimpleExploration.name);
				experience += UMO3d_mySimpleExploration.ExpOnExplore;
				skillExperience += UMO3d_mySimpleExploration.SkillExpOnExplore;
				gold += UMO3d_mySimpleExploration.GoldOnExplore;
				coins += UMO3d_mySimpleExploration.CoinsOnExplore;
				UMO3d_exploredAreas.Add(UMO3d_mySimpleExploration.name);
				var msg = UMO3d_MSG_EXPLORED_HEADING + UMO3d_mySimpleExploration.name;
				Target_UMO3d_ShowExplorationUI(connectionToClient, msg);
			// -- show notice if already explored
			} else if (UMO3d_mySimpleExploration.noticeOnEnter) {
				var msg = UMO3d_MSG_ENTERED_HEADING + UMO3d_mySimpleExploration.name;
				Target_UMO3d_ShowExplorationUI(connectionToClient, msg);
			}
		}
	}
	
	// -----------------------------------------------------------------------------------
	// Target_UMO3d_ShowExplorationUI
	// -----------------------------------------------------------------------------------
	[TargetRpc(channel=Channels.DefaultUnreliable)] // only send to one client
    public void Target_UMO3d_ShowExplorationUI(NetworkConnection target, string message) {
        FindObjectOfType<UISimpleExploration>().Show(message);
    }

}

#endif

// =======================================================================================