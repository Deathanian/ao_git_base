﻿// =======================================================================================
// SIMPLE EXPLORATION - DATABASE
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UnityEngine;
using Mono.Data.Sqlite; // copied from Unity/Mono/lib/mono/2.0 to Plugins
using System;
using System.Collections;

#if _SimpleExploration

// =======================================================================================
// DATABASE
// =======================================================================================
public partial class Database {
	
	// -----------------------------------------------------------------------------------
	// Initialize_UMO3d_SimpleExploration
	// -----------------------------------------------------------------------------------
	static void Initialize_UMO3d_SimpleExploration() {
		ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS UMO3d_character_exploration ( character TEXT NOT NULL, exploredArea TEXT NOT NULL)");
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterLoad_UMO3d_SimpleExploration
	// -----------------------------------------------------------------------------------
	static void CharacterLoad_UMO3d_SimpleExploration(Player player) {
		var table = ExecuteReader("SELECT exploredArea FROM UMO3d_character_exploration WHERE character=@name", new SqliteParameter("@name", player.name));
		foreach (var row in table) {
			player.UMO3d_exploredAreas.Add((string)row[0]);
		}
	}
	
	// -----------------------------------------------------------------------------------
	// CharacterSave_UMO3d_SimpleExploration
	// -----------------------------------------------------------------------------------
	static void CharacterSave_UMO3d_SimpleExploration(Player player) {
		ExecuteNonQuery("DELETE FROM UMO3d_character_exploration WHERE character=@character", new SqliteParameter("@character", player.name));
		for (int i = 0; i < player.UMO3d_exploredAreas.Count; ++i) {
			ExecuteNonQuery("INSERT INTO UMO3d_character_exploration VALUES (@character, @exploredArea)",
 				new SqliteParameter("@character", player.name),
 				new SqliteParameter("@exploredArea", player.UMO3d_exploredAreas[i]));
 		}
			
	}	
	
}

#endif

// =======================================================================================