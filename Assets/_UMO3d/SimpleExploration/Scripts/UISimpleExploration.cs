﻿// =======================================================================================
// SIMPLE EXPLORATION - UI
// by Indie-MMO (http://indie-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.UI;

namespace UMO3d {

	// ===================================================================================
	// SIMPLE EXPLORATION UI
	// ===================================================================================
	public class UISimpleExploration : MonoBehaviour {
	    [SerializeField] GameObject panel;
	    [SerializeField] Text textMessage;

	    public void Show(string msg) {
	        textMessage.text = msg;
	        panel.SetActive(true);
	        Invoke("Hide", 2.5f);
	    }
	    
	    public void Hide() {
	    	textMessage.text = "";
	    	panel.SetActive(false);
	    }
	    
	}

}

// =======================================================================================