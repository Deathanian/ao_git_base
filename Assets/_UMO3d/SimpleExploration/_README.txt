﻿======================================================================
INDIE-MMO -UMMORPG & UNITY ASSETS
======================================================================
* Copyright by INDIE-MMO -no permission to share, re-sell or give away
* Authors: Fhiz & Stue
* This asset is an Add-On for uMMORPG3d, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.indie-mmo.com
* NuCore Download (required for most AddOns): http://www.indie-mmo.com/butler/index.php
* Support via eMail: support@indie-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Locate the Prefabs folder within this AddOns folder. 

1. Drag and Drop the "SimpleExplorationUI" prefab in your scenes canvas. It is important to
place it inside the canvas object in the hierarchy, the exact position is irrelevant (just
not inside another object).

2. Drag and Drop the "SimpleExploration" prefab into your scene, adjust its size and
parameters in the inspector.

You can place as many SimpleExploration prefabs as you like.

Thats it, no script modifications!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

"SimpleExploration" allows you to add areas to your scenes that can be explored by your
players. This gives your players something to do satisfy their exploratory urge and
provide a way of gaining experience/skill experiences besides combat and quests.

* Each area can only be explored once (persistent/saved in database).
* Exploration shows a popup window with the name of area explored (auto hide after 2.5s).
* Grant your players experience and/or skill experience, gold, coins on exploration.
* Notify the player once or every time they enter the area (exploration happens only once).

Edit the constant in the top of the UISimpleExploration.cs file in order to change the
notification texts send to player when exploring an area.

======================================================================