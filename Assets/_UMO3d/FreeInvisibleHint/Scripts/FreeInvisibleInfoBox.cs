﻿// =======================================================================================
// FREE INVISIBLE INFO BOX - AREA
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace UMO3d {

	// ===================================================================================
	// SIMPLE TELEPORT
	// ===================================================================================
	[RequireComponent(typeof(BoxCollider))]
	public class FreeInvisibleInfoBox : MonoBehaviour {
	
		[Tooltip("One click deactivation")]
		public bool infoBoxActive	= true;
		[Tooltip("Text to display while in the area"), TextArea(1, 30)]
		public string textToDisplay	= "";

		// -------------------------------------------------------------------------------
		// OnTriggerEnter
		// -------------------------------------------------------------------------------
		void OnTriggerEnter(Collider co) {
			var e = co.GetComponentInParent<Player>();
			if (e && infoBoxActive && textToDisplay != "") {
				FindObjectOfType<UIFreeInvisibleInfoBox>().Show(textToDisplay);
			}
		}
		
		// -------------------------------------------------------------------------------
		// OnTriggerExit
		// -------------------------------------------------------------------------------
		void OnTriggerExit(Collider co) {
			var e = co.GetComponentInParent<Player>();
			if (e && infoBoxActive && textToDisplay != "") {
				FindObjectOfType<UIFreeInvisibleInfoBox>().Hide();
			}
		}

	}

}

// =======================================================================================