﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Locate the Prefabs folder within this AddOns folder. 

1. Drag and Drop the "FreeInvisibleInfoBoxUI" prefab in your scenes canvas. It is important to
place it inside the canvas object in the hierarchy, the exact position is irrelevant (just
not inside another object).

2. Drag and Drop the "FreeInvisibleInfoBox" prefab into your scene, adjust its size and parameters
and dont forget to assign a valid teleportation target (that can be any object within the
same scene that features a transform).

You can place as many FreeInvisibleInfoBox prefabs as you like.

Thats it, no script modifications!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FreeInvisibleInfoBox adds a trigger to your scene that shows a popup window with some
predefined text content. Useful to give your players additional information in certain areas.

It is only visible once a player stepped onto the trigger and dis-appears again once
the player leaves the trigger. The box accepts rich text so you can add some basic HTML
tags to it like <b>bold</b>

======================================================================