﻿======================================================================
UNITY-MMO - UMMORPG & UNITY ASSETS
======================================================================
* Copyright by Unity-MMO - no permission to share, re-sell or give away
* Authors: Stue & Fhiz (Laffy AddOns maintained with permission)
* This asset is an Add-On for uMMORPG, it is useless without.
* Tested under MacOS, Windows and Linux Dedicated Server.
* Get uMMORPG here: https://www.assetstore.unity3d.com/en/#!/content/51212

* Our AddOn/Asset Store: http://www.unity-mmo.com
* NuCore Download (required for most AddOns): http://www.unity-mmo.com/butler/index.php
* Support via eMail: support@unity-mmo.com
* Support via Discord: https://discord.gg/NGNqpnw
======================================================================

INSTALLATION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Locate the Prefabs folder within this AddOns folder. 

1. Drag and Drop the "SimpleLootcrateUI" prefab in your scenes canvas. It is important to
place it inside the canvas object in the hierarchy, the exact position is irrelevant (just
not inside another object).

2. Drag and Drop the "SimpleLootcrate" prefab into your scene, adjust its model and parameters.

Ignore the Entity parameters like State, Level, Health and especially Gold. Just ignore
them, I cannot do anything about them right now!

Only change the relevant parameters that are:

- Everything under the "Lock" section
- Everything under the "Loot" section
- Everything under the "Resapwn" section

3. If you assign a itemKey and/or a itemLockpick you will have to create those items
as well. There are two example items in the Addons prefabs folder. Drag and Drop them
into your Resources -> Items folder and assign them to a Monsters loot drop or to an
NPCs shop items (so that they can be acquired in your game).

Thats it, no script modifications!

DESCRIPTION
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Add lootcrates and chests to your game!

* Spawn and Respawn Lootcrates similar to monsters
* Populate Lootcrates with Gold, Coins and Items
* Set a optional probability that the Lootcrate is locked
* Set a key and/or a lockpick in order to unlock Lootcrates
* Set if key/lockpick will be destroyed on use or not
* Lockpick takes precedence over the key, if both are available

======================================================================