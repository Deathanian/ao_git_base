﻿// =======================================================================================
// SIMPLE LOOTCRATE - CLASS
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

namespace UMO3d {

	public partial class SimpleLootcrate : Entity {
		
		[Header("Lock")]
		[Range(0, 1)] public float lockChance;
		[SyncVar, HideInInspector] public bool locked;
		public ItemTemplate itemKey;
		public bool destroyKey;
		public ItemTemplate itemLockpick;
		public bool destroyLockpick;
		
		[Header("Loot")]
    	public int lootGoldMin = 0;
    	public int lootGoldMax = 10;
    	public int lootCoinsMin = 0;
    	public int lootCoinsMax = 5;
    	public ItemDropChance[] dropChances;
    	public long coins;
    	
    	[Header("Respawn")]
    	[Tooltip("How long the lootcrate stays empty in the scene when it has been completely plundered (in Seconds)")]
    	public float emptyTime = 5f;
    	float emptyTimeEnd;
    	[Tooltip("If the lootcrate is respawned once it got plundered. Otherwise its available only once.")]
    	public bool respawn = true;
    	[Tooltip("How long it takes for a empty lootcrate to respawn (in Seconds) with full loot again. Set higher than 0 !")]
    	public float respawnTime = 10f;
	    float respawnTimeEnd;
		
		// networkbehaviour ////////////////////////////////////////////////////////
		protected override void Awake() {
			base.Awake();
		}

		public override void OnStartServer() {
			base.OnStartServer();
		}

		void LateUpdate() {
		}

		// finite state machine events /////////////////////////////////////////////
		bool EventEmpty() {
			return !HasLoot();
		}
		
 		bool EventEmptyTimeElapsed() {
         return state == "EMPTY" && Time.time >= emptyTimeEnd;
     	}

    	bool EventRespawnTimeElapsed() {
        return state == "EMPTY" && respawn && Time.time >= respawnTimeEnd;
    	}

		// finite state machine - server ///////////////////////////////////////////
		[Server]
		string UpdateServer_IDLE() {
			// events sorted by priority (e.g. target doesn't matter if we died)
			if (EventEmpty()) {
				OnEmpty();
				return "EMPTY";
			}
			return "IDLE"; // nothing interesting happened
		}

		[Server]
		string UpdateServer_EMPTY() {
			// events sorted by priority (e.g. target doesn't matter if we died)
			if (EventRespawnTimeElapsed()) {
				OnRefill();
				Show();
				return "IDLE";
			}
			if (EventEmptyTimeElapsed()) {
				// we were lying around empty for long enough now.
				// hide while respawning, or disappear forever
				if (respawn) Hide();
				else NetworkServer.Destroy(gameObject);
				return "EMPTY";
			}
			
			if (EventEmpty()) {} // don't care, of course we are empty

			return "EMPTY"; // nothing interesting happened
		}

		[Server]
		protected override string UpdateServer() {
			if (state == "IDLE")    return UpdateServer_IDLE();
			if (state == "EMPTY")    return UpdateServer_EMPTY();
			Debug.LogError("invalid state:" + state);
			return "IDLE";
		}
		
		// finite state machine - client ///////////////////////////////////////////
    	[Client]
    	protected override void UpdateClient() { }

		// OnEmpty ///////////////////////////////////////////////////////////////////
		[Server]
		void OnEmpty() {
			// set death and respawn end times. we set both of them now to make sure
			// that everything works fine even if a monster isn't updated for a
			// while. so as soon as it's updated again, the death/respawn will
			// happen immediately if current time > end time.
			emptyTimeEnd = Time.time + emptyTime;
			respawnTimeEnd = emptyTimeEnd + respawnTime; // after empty time ended
		}
		
		// OnRefill ///////////////////////////////////////////////////////////////////
		[Server]
		void OnRefill() {
			
			// locked?
			if (Random.value <= lockChance) {
				locked = true; 
			} else {
				locked = false;
			}
			
			// generate gold & coins
			gold = Random.Range(lootGoldMin, lootGoldMax);
			coins = Random.Range(lootCoinsMin, lootCoinsMax);
			
			// generate items (note: can't use Linq because of SyncList)
			foreach (ItemDropChance itemChance in dropChances)
				if (Random.value <= itemChance.probability)
					inventory.Add(new Item(itemChance.template));
		}		

		// loot ////////////////////////////////////////////////////////////////////
    	// other scripts need to know if it still has valid loot (to show UI etc.)
    	public bool HasLoot() {
        	// any gold or valid items?
        	return gold > 0 || coins > 0 || inventory.Any(item => item.valid);
    	}

		// skills //////////////////////////////////////////////////////////////////
		public override bool HasCastWeapon() { return true; }
		public override bool CanAttack(Entity type) { return false; }
		public override int healthMax { get { return 1; } }
    	public override int manaMax { get { return 0; } }
    	public override int damage { get { return 0; } }
    	public override int defense { get { return 0; } }
    	public override float blockChance { get { return 0; } }
    	public override float criticalChance { get { return 0; } }

	}

}

// =======================================================================================