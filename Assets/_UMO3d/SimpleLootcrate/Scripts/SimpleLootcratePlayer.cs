﻿// =======================================================================================
// SIMPLE LOOTCRATE - PLAYER
// by Unity-MMO (http://unity-mmo.com)
// Copyright 2017+
// =======================================================================================

using UMO3d;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

// =======================================================================================
// PLAYER
// =======================================================================================
public partial class Player {
	
	public const string UMO3d_MSG_LOOTCRATE_LOCKED = "It's locked!";
	
	// -----------------------------------------------------------------------------------
	// OnSelect_UMO3d_SimpleLootcrate
	// -----------------------------------------------------------------------------------
	private void OnSelect_UMO3d_SimpleLootcrate(Entity entity) {
		if (entity != this && 
			entity is SimpleLootcrate &&
			Utils.ClosestDistance(collider, entity.collider) <= interactionRange) {
           	
           	var target = (SimpleLootcrate)entity;
           	var valid = false;
           	
           	// -- check lock
           	if (target.locked) {

           		if (target.itemLockpick && !target.destroyLockpick && InventoryCountAmount(target.itemLockpick.name) >= 1) {
					valid = true;
				} else if (target.itemLockpick && target.destroyLockpick && InventoryCountAmount(target.itemLockpick.name) >= 1) {
					InventoryRemoveAmount(target.itemLockpick.name, 1);
					valid = true;
				}
           	
           		if (!valid) {
					if (target.itemKey && !target.destroyKey && InventoryCountAmount(target.itemKey.name) >= 1) {
						valid = true;
					} else if (target.itemKey && target.destroyKey && InventoryCountAmount(target.itemKey.name) >= 1) {
						InventoryRemoveAmount(target.itemKey.name, 1);
						valid = true;
					}
				}
           		
           		// -- unlock
           		if (valid) {
           			((SimpleLootcrate)entity).locked = false;
           		}
           		
           	}
           	
           	// --- show loot window
           	if (!target.locked || valid) {
           		FindObjectOfType<UILootcrate>().Show();
           	} else {
           		FindObjectOfType<UIPopup>().Show(UMO3d_MSG_LOOTCRATE_LOCKED);
           	}
           	
		}
	}
	
	// -----------------------------------------------------------------------------------
	// Cmd_UMO3d_TakeLootcrateGold
	// -----------------------------------------------------------------------------------
    [Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void Cmd_UMO3d_TakeLootcrateGold() {
        // use collider point(s) to also work with big entities
        if ((state == "IDLE" || state == "MOVING" || state == "CASTING") &&
            target != null && target is SimpleLootcrate && target.gold > 0 &&
            Utils.ClosestDistance(collider, target.collider) <= interactionRange)
        {
            // take it
            gold += target.gold;
            target.gold = 0;
        }
    }
  
  	// -----------------------------------------------------------------------------------
	// Cmd_UMO3d_TakeLootcrateCoins
	// -----------------------------------------------------------------------------------
    [Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void Cmd_UMO3d_TakeLootcrateCoins() {
        // use collider point(s) to also work with big entities
        if ((state == "IDLE" || state == "MOVING" || state == "CASTING") &&
            target != null && target is SimpleLootcrate && ((SimpleLootcrate)target).coins > 0 &&
            Utils.ClosestDistance(collider, target.collider) <= interactionRange)
        {
            // take it
            coins += ((SimpleLootcrate)target).coins;
            ((SimpleLootcrate)target).coins = 0;
        }
    }
    
	// -----------------------------------------------------------------------------------
	// Cmd_UMO3d_TakeLootcrateItem
	// -----------------------------------------------------------------------------------
    [Command(channel=Channels.DefaultUnreliable)] // unimportant => unreliable
    public void Cmd_UMO3d_TakeLootcrateItem(int index) {
        // validate: dead monster and close enough and valid loot index?
        // use collider point(s) to also work with big entities
        if ((state == "IDLE" || state == "MOVING" || state == "CASTING") &&
            target != null && target is SimpleLootcrate && ((SimpleLootcrate)target).HasLoot() &&
            Utils.ClosestDistance(collider, target.collider) <= interactionRange &&
            0 <= index && index < target.inventory.Count &&
            target.inventory[index].valid)
        {
            var item = target.inventory[index];

            // try to add it to the inventory, clear lootcrate slot if it worked
            if (InventoryAddAmount(item.template, item.amount)) {
                item.valid = false;
                target.inventory[index] = item;
            }
        }
    }
    
}

// =======================================================================================